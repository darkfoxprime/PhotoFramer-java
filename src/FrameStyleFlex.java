import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.font.FontRenderContext;
import java.awt.font.TextAttribute;
import java.awt.font.TextLayout;
import java.awt.geom.AffineTransform;
import java.awt.geom.Path2D;
import java.awt.image.BufferedImage;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;

import org.springhaven.util.EXIFInfo;
import org.springhaven.util.MetaImage;
import org.springhaven.util.SpacePacker;
import org.springhaven.util.EXIFInfo.Rational;
import org.springhaven.util.SpacePacker.Anchor;
import org.springhaven.util.SpacePacker.Edge;
import org.springhaven.util.SpacePacker.Expand;
import org.springhaven.util.SpacePacker.Fill;

/**
 * <h1>The Flex Framing Style</h1>
 * 
 * <p>
 * The Flex framing style allows arbitrary text items to be placed, along with
 * separators, in a flexible user-controlled manner. The user may define and
 * place as many &ldquo;items&rdquo; in the photo as they wish, in whichever
 * borders they wish.
 * </p>
 * 
 * <p>
 * Each &ldquo;item&rdquo; has a number of item-specific properties that vary
 * based on the type of item. Common properties include
 * <i>&lt;itemName&gt;</i><b>Type</b>, <i>&lt;itemName&gt;</i><b>Color</b>,
 * <i>&lt;itemName&gt;</i><b>Width</b>, <i>&lt;itemName&gt;</i><b>Height</b>,
 * and <i>&lt;itemName&gt;</i><b>Position</b>. For example, an item could look
 * like the following.
 * </p>
 * <table align="center" border="1">
 * <tr>
 * <td>
 * CopyrightType=Text<br>
 * CopyrightPosition=SOUTH<br>
 * CopyrightHeight=20%<br>
 * CopyrightFont=Lucida Bold<br>
 * CopyrightFormat=Copyright \u00A9 %Y by %A<br>
 * </td>
 * </tr>
 * </table>
 * <p>
 * In this case, the item is named &ldquo;Copyright&rdquo; and has the
 * properties <b>Type</b>, <b>Position</b>, <b>Height</b>, <b>Font</b>, and
 * <b>Format</b> defined. (These and the rest of the properties used by
 * different item types will be described completely in the following sections.)
 * </p>
 * 
 * <h1><a name="ItemPlacement">Item Placement</a></h1>
 * 
 * <p>
 * The Flex framer lays out the items it is given using a method known as Edge
 * Packing. Each item has a Position which is one of the four edges of the
 * border into which the items are being drawn. The four edges are referred to
 * by compass direction, with <b>NORTH</b> at the top:
 * </p>
 * <table border="1" align="center" cellspacing="0">
 * <tr>
 * <td></td>
 * <td align="center" valign="middle"><b>NORTH</b></td>
 * <td></td>
 * </tr>
 * <tr>
 * <td align="center" valign="middle"><b>W<br>
 * e<br>
 * s<br>
 * t</b></td>
 * <td width="120" height="80" style="background:gray" align="center" valign="middle">
 * </td>
 * <td align="center" valign="middle"><b>E<br>
 * a<br>
 * s<br>
 * t</b></td>
 * </tr>
 * <tr>
 * <td></td>
 * <td align="center" valign="middle"><b>SOUTH</b></td>
 * <td></td>
 * </tr>
 * </table>
 * 
 * <p>
 * In the order specified by the <b>ItemOrder</b> property, each item is packed
 * against one of the edges of the space available. The item will span the
 * entire edge, but will extend towards the center of the space only as much as
 * it needs to. For example, after placing an item "A" on the WEST edge, the
 * space might look like this:
 * </p>
 * <table border="1" align="center" cellspacing="0">
 * <tr>
 * <td></td>
 * <td align="center" valign="middle" colspan="2"><b>NORTH</b></td>
 * <td></td>
 * </tr>
 * <tr>
 * <td align="center" valign="middle"><b>W<br>
 * e<br>
 * s<br>
 * t</b></td>
 * <td width="20" height="80" style="background:green" align="center" valign="middle">
 * A</td>
 * <td width="100" height="80" style="background:gray" align="center" valign="middle">
 * </td>
 * <td align="center" valign="middle"><b>E<br>
 * a<br>
 * s<br>
 * t</b></td>
 * </tr>
 * <tr>
 * <td></td>
 * <td align="center" valign="middle" colspan="2"><b>SOUTH</b></td>
 * <td></td>
 * </tr>
 * </table>
 * <p>
 * Successive items are then placed in the same manner, in the space remaining.
 * For example, after placing the additional items "B" (NORTH), "C" (NORTH), and
 * "D" (EAST), the layout would look like this:
 * </p>
 * <table border="1" align="center" cellspacing="0">
 * <tr>
 * <td></td>
 * <td align="center" valign="middle" colspan="3"><b>NORTH</b></td>
 * <td></td>
 * </tr>
 * <tr>
 * <td align="center" valign="middle" rowspan="3"><b>W<br>
 * e<br>
 * s<br>
 * t</b></td>
 * <td width="20" height="80" style="background:green" align="center" valign="middle" rowspan="3">
 * A</td>
 * <td width="100" height="20" style="background:blue" align="center" valign="middle" colspan="2">
 * B</td>
 * <td rowspan="3" align="center" valign="middle"><b>E<br>
 * a<br>
 * s<br>
 * t</b></td>
 * </tr>
 * <tr>
 * <td width="100" height="20" style="background:red" align="center" valign="middle" colspan="2">
 * C</td>
 * </tr>
 * <tr>
 * <td style="background:gray" width="80" height="40" align="center" valign="middle">
 * &nbsp;</td>
 * <td style="background:yellow" width="20" height="40" align="center" valign="middle">
 * D</td>
 * </tr>
 * <tr>
 * <td></td>
 * <td colspan="3" align="center" valign="middle"><b>SOUTH</b></td>
 * <td></td>
 * </tr>
 * </table>
 * <p>
 * Once all items have been placed, the remaining unused space is collapsed, and
 * the placed items all expand equally<small><sup>*</sup></small> to fit the
 * original space:
 * </p>
 * <table border="1" align="center" cellspacing="0">
 * <tr>
 * <td></td>
 * <td colspan="3" align="center" valign="middle"><b>NORTH</b></td>
 * <td></td>
 * </tr>
 * <tr>
 * <td rowspan="3" align="center" valign="middle"><b>W<br>
 * e<br>
 * s<br>
 * t</b></td>
 * <td width="60" height="80" style="background:green" rowspan="3" align="center" valign="middle">
 * A</td>
 * <td width="60" height="27" style="background:blue" align="center" valign="middle">
 * B</td>
 * <td rowspan="3" align="center" valign="middle"><b>E<br>
 * a<br>
 * s<br>
 * t</b></td>
 * </tr>
 * <tr>
 * <td width="60" height="26" style="background:red" align="center" valign="middle">
 * C</td>
 * </tr>
 * <tr>
 * <td width="60" height="27" style="background:yellow" align="center" valign="middle">
 * D</td>
 * </tr>
 * <tr>
 * <td></td>
 * <td colspan="2" align="center" valign="middle"><b>SOUTH</b></td>
 * <td></td>
 * </tr>
 * </table>
 * <p>
 * <small><sup>*</sup></small>Depending on the item type and the amount of
 * expansion required, some items will not expand as much as others, and some
 * items might not expand at all. For example, a horizontal separator line will
 * expand horizontally but not vertically.
 * </p>
 * <p>
 * Since the amount of space available to an item might be larger than the item
 * needs, each item also has an <b>Anchor</b> property, which tells how the item
 * is &ldquo;anchored&rdquo; within its space. The Anchor property can have a
 * value of <b>CENTER</b>, which means the item will be centered in the space
 * available to it; one of the four directions (<b>NORTH</b>, <b>SOUTH</b>,
 * <b>EAST</b>, <b>WEST</b>), in which case the item will be centered with one
 * edge touching that side of the item's allocated space; or one of the four
 * corners (<b>NORTHEAST</b>, <b>NORTHWEST</b>, <b>SOUTHEAST</b>,
 * <b>SOUTHWEST</b>), in which case the item will be drawn in that corner of its
 * space.
 * </p>
 * 
 * <h1><a name="TextFormatting">Text Formatting</a></h1>
 * 
 * <p>
 * The Flex framer includes a simple text formatting &ldquo;language&rdquo; to
 * allow incorporating information about the photo being framed in the text
 * strings being displayed.
 * </p>
 * 
 * <p>
 * Each format string consists of raw text to be displayed, interspersed with
 * formatting codes of the form <tt><b>%[width]&lt;code&gt;</b></tt>. The codes
 * are listed at the end of this section. While most codes are a single letter,
 * there are some extended codes that use two or more letters.
 * </p>
 * 
 * <p>
 * An example format code for a copyright string might be:
 * </p>
 * <blockquote><tt>Copyright \u00A9 %4Y by %A</tt></blockquote>
 * 
 * <p>
 * The formatting codes are:
 * </p>
 * <table border="1" align="center">
 * <tr>
 * <th>Code</th>
 * <th>Replaced by</th>
 * <th>Examples</th>
 * </tr>
 * <tr>
 * <th><nobr>%%</nobr></th>
 * <td>A literal % symbol.</td>
 * <td><b>%</b></td>
 * </tr>
 * <tr>
 * <th><nobr>%a</nobr></th>
 * <td>AM or PM. If width is 1, only the first letter is used.</td>
 * <td><b>PM</b>;&nbsp;<b>P</b></td>
 * <tr>
 * <th><nobr>%A</nobr></th>
 * <td>The artist's full name. The width is ignored.</td>
 * <td><b>Johnson&nbsp;Earls</b></td>
 * </tr>
 * <tr>
 * <th><nobr>%D</nobr></th>
 * <td>The date of the month the photo was taken. Unless the width is specified
 * as 1, a preceding 0 will be used if necessary to make a 2-digit date.</td>
 * <td><b>05</b>;&nbsp;<b>5</b></td>
 * </tr>
 * <tr>
 * <th><nobr>%h</nobr></th>
 * <td>The hour of the time the photo was taken, in 24-hour form (0 through 23).
 * Unless the width is specified as 1, a preceding 0 will be used if necessary
 * to make a 2-digit hour.</td>
 * <td><b>22</b>;&nbsp;<b>08</b>;&nbsp;<b>8</b></td>
 * </tr>
 * <tr>
 * <th><nobr>%i</nobr></th>
 * <td>The hour of the time the photo was taken, in 12-hour form (1 through 12).
 * If the width is specified as 2, a preceding 0 will be used if necessary to
 * make a 2-digit hour.</td>
 * <td><b>8</b>; <b>08</b></td>
 * </tr>
 * <tr>
 * <th><nobr>%m</nobr></th>
 * <td>The minute of the time the photo was taken (0 through 59). Unless the
 * width is 1, a preceding 0 will be used to make a 2-digit minute.</td>
 * <td><b>38</b></td>
 * </tr>
 * <tr>
 * <th><nobr>%M</nobr></th>
 * <td>The month the photo was taken. If the width is not specified or is 3, the
 * abbreviated text form of the month is used; if the width is 4 or greater, the
 * full text form is used; if the width is 1 or 2, the month number is used
 * (with preceding 0 if necessary when the width is 2).</td>
 * <td><b>Jul</b>;&nbsp;<b>July</b>;&nbsp;<b>07</b>;&nbsp;<b>7</b></tt>
 * </tr>
 * <tr>
 * <th><nobr>%s</nobr></th>
 * <td>The second of the time the photo was taken (0 through 61, in case of leap
 * seconds). Unless the width is 1, a preceding 0 will be used to make a 2-digit
 * second.</td>
 * <td><b>21</b></td>
 * </tr>
 * <tr>
 * <th><nobr>%T</nobr></th>
 * <td>The title of the photo, which is the last component of the filename,
 * without any .JPG or .JPEG extension, and with _ characters replaced by
 * spaces. The width is ignored.</td>
 * <td><b>Self&nbsp;Portrait</b></td>
 * </tr>
 * <tr>
 * <th><nobr>%XA</nobr></th>
 * <td>The aperture F-number from the photo's EXIF information. The width is
 * ignored.</td>
 * <td><b>5.6</b></td>
 * </tr>
 * <tr>
 * <th><nobr>%XC</nobr></th>
 * <td>The camera model. The width is ignored.</td>
 * <td><b>NIKON&nbsp;D60</b></td>
 * </tr>
 * <tr>
 * <th><nobr>%XF</nobr></th>
 * <td>The focal length (equivalent to 35mm film). The width is ignored.</td>
 * <td><b>300</b></td>
 * </tr>
 * <tr>
 * <th><nobr>%XI</nobr></th>
 * <td>The ISO rating from the photo's EXIF information. The width is ignored.</td>
 * <td><b>100</b>;&nbsp;<b>200</b></td>
 * </tr>
 * <tr>
 * <th><nobr>%XM</nobr></th>
 * <td>The camera's manufacturer. The width is ignored.</td>
 * <td><b>NIKON&nbsp;CORPORATION</b></td>
 * </tr>
 * <tr>
 * <th><nobr>%XS</nobr></th>
 * <td>The shutter speed from the photo's EXIF information, as a decimal number
 * or fraction of a second. If the width is 0 or not specified, a fraction is
 * used (or whole number plus fraction). If the width is greater than 0, then a
 * decimal number is used, with at most that many digits after the decimal
 * point.</td>
 * <td><b>1/60</b>;&nbsp;<b>1.33</b></td>
 * </tr>
 * <tr>
 * <th><nobr>%Y</nobr></th>
 * <td>The year the photo was taken. If the width is not specified, or is 3 or
 * greater, a 4-digit year will be used; otherwise, a 2-digit year will be used.
 * </td>
 * <td><b>2009</b>;&nbsp;<b>09</b></td>
 * </tr>
 * </table>
 * 
 * 
 * <h1>Options used by this framing style</h1>
 * 
 * <p>
 * The options that control this framing style are described in the following
 * table; the second table contains the descriptions of the Option Types used
 * below.
 * </p>
 * <table border="1">
 * <tr>
 * <th colspan="4">Border Options</th>
 * </tr>
 * <tr>
 * <th>Option&nbsp;Name</th>
 * <th>Option&nbsp;Type</th>
 * <th>Description</th>
 * <th>Default&nbsp;Value</th>
 * </tr>
 * <tr>
 * <th align="left">BorderBottom</th>
 * <td>Absolute&nbsp;or&nbsp;Relative&nbsp;Dimension</td>
 * <td>The height of the top border. If given as a percentage, it is relative to
 * the final height of the photo; otherwise, it is the height of the border in
 * pixels.</td>
 * <td>5%</td>
 * </tr>
 * <tr>
 * <th align="left">BorderColor</th>
 * <td>Color</td>
 * <td>The color of the border to be added to the outside of the picture.</td>
 * <td>White</td>
 * </tr>
 * <tr>
 * <th align="left">BorderLeft</th>
 * <td>Absolute&nbsp;or&nbsp;Relative&nbsp;Dimension</td>
 * <td>The width of the left border. If given as a percentage, it is relative to
 * the final width of the photo; otherwise, it is the width of the border in
 * pixels.</td>
 * <td>2%</td>
 * </tr>
 * <tr>
 * <th align="left">BorderRight</th>
 * <td>Absolute&nbsp;or&nbsp;Relative&nbsp;Dimension</td>
 * <td>The width of the right border. If given as a percentage, it is relative
 * to the final width of the photo; otherwise, it is the width of the border in
 * pixels.</td>
 * <td>2%</td>
 * </tr>
 * <tr>
 * <th align="left">BorderTop</th>
 * <td>Absolute&nbsp;or&nbsp;Relative&nbsp;Dimension</td>
 * <td>The height of the top border. If given as a percentage, it is relative to
 * the final height of the photo; otherwise, it is the height of the border in
 * pixels.</td>
 * <td>2%</td>
 * </tr>
 * <tr>
 * <th align="left">MaxHeight</th>
 * <td>Absolute&nbsp;Dimension</td>
 * <td>The maximum height of the final photo.</td>
 * <td>2048</td>
 * </tr>
 * <tr>
 * <th align="left">MaxWidth</th>
 * <td>Absolute&nbsp;Dimension</td>
 * <td>The maximum width of the final photo.</td>
 * <td>2048</td>
 * </tr>
 * <tr>
 * <th colspan="4">General Picture Options</th>
 * </tr>
 * <tr>
 * <th align="left">Artist</th>
 * <td>String</td>
 * <td>The artist's name.</td>
 * <td>The username of the user running the program.</td>
 * </tr>
 * <tr>
 * <th align="left">Title</th>
 * <td>String</td>
 * <td>The photo's title.</td>
 * <td>The photo's filename, without trailing &ldquo;.<i>xyz</i>&rdquo;
 * extension, and with all underscores (_) replaced by spaces.</td>
 * </tr>
 * <tr>
 * <th colspan="4">Item Placement Options</th>
 * </tr>
 * <tr>
 * <th>Option&nbsp;Name</th>
 * <th>Option&nbsp;Type</th>
 * <th>Description</th>
 * <th>Default&nbsp;Value</th>
 * </tr>
 * <tr>
 * <th>DefaultAnchor</th>
 * <td>Anchor</td>
 * <td>The location to use to anchor any item that does not specify its own
 * anchor. See <b><a href="#ItemPlacement">Item Placement</a></b> for more
 * information.</td>
 * <td>CENTER</td>
 * </tr>
 * <tr>
 * <th>DefaultColor</th>
 * <td>Color</td>
 * <td>The color to use for any item that does not specify its own color.</td>
 * <td>Black</td>
 * </tr>
 * <tr>
 * <th>DefaultEdge</th>
 * <td>Edge</td>
 * <td>The edge at which to place any item that does not specify its own edge.
 * See <b><a href="#ItemPlacement">Item Placement</a></b> for more information.</td>
 * <td>NORTH</td>
 * </tr>
 * <tr>
 * <th>DefaultExpand</th>
 * <td>Fill</td>
 * <td>How the item should be allowed to expand if more space is available than
 * it needs.</td>
 * <td>EastWest</td>
 * </tr>
 * <tr>
 * <th>DefaultFill</th>
 * <td>Fill</td>
 * <td>How the item should be fill its space.</td>
 * <td>None</td>
 * </tr>
 * <tr>
 * <th>DefaultFont</th>
 * <td>Font</td>
 * <td>For items of type <b>Text</b> only. The font to use for any Text item
 * that does not specify its own font.</td>
 * <td>SansSerif</td>
 * </tr>
 * <tr>
 * <th>DefaultHeight</th>
 * <td>Absolute or Relative Dimension</td>
 * <td>The height to use for any item that does not specify its own height. If a
 * Relative Dimension is used, it is relative to the height of the border
 * specified by <b>ItemBorder</b>.</td>
 * <td>25%</td>
 * </tr>
 * <tr>
 * <th>DefaultSeparator</th>
 * <td><b>Simple</b>, <b>Pointed</b>, or <b>Star</b></td>
 * <td>For items of type <b>Separator</b> only. The type of separator line to
 * use.</td>
 * <td>Simple</td>
 * </tr>
 * <tr>
 * <th>DefaultSpacing</th>
 * <td>Absolute or Relative Dimension</td>
 * <td>For items of type <b>Text</b> only. The default amount of space to add in
 * between each letter of the text being displayed for any item that does not
 * specify its own spacing. If a Relative Dimension is used, it is relative to
 * the width of the border specified by <b>ItemBorder</b>.</td>
 * <td>0</td>
 * </tr>
 * <tr>
 * <th>DefaultType</th>
 * <td><b>Text</b> or <b>Separator</b></td>
 * <td>The type of any item that does not specify its own type.</td>
 * <td>Text</td>
 * </tr>
 * <tr>
 * <th>DefaultWidth</th>
 * <td>Absolute or Relative Dimension</td>
 * <td>The width to use for any item that does not specify its own width. If a
 * Relative Dimension is used, it is relative to the width of the border
 * specified by <b>ItemBorder</b>.</td>
 * <td>75%</td>
 * </tr>
 * <tr>
 * <th>HorizontalGap</th>
 * <td>Absolute or Relative Dimension</td>
 * <td>The amount of space to leave in between horizontally-adjacent items. If a
 * Relative Dimension is used, it is relative to the width of the border
 * specified by ItemBorder.</td>
 * <td>0</td>
 * </tr>
 * <tr>
 * <th>ItemBorder</th>
 * <td>Edge</td>
 * <td>Which border the items will be placed into.</td>
 * <td>Bottom</td>
 * </tr>
 * <tr>
 * <th>ItemOrder</th>
 * <td>List of Strings</td>
 * <td>This specifies the list of items that will be drawn in the
 * <i>&lt;ItemBorder&gt;</i> border area. Each name in the list names one item
 * to be drawn; the parameters that determine how that item will be drawn come
 * from those listed below. If the same name appears more than once in the list,
 * then that item will appear multiple times in the result.</td>
 * <td>Title,&nbsp;Separator&nbsp;Artist&nbsp;Separator&nbsp;Date</td>
 * </tr>
 * <tr>
 * <th>VerticalGap</th>
 * <td>Absolute or Relative Dimension</td>
 * <td>The amount of space to leave in between vertically-adjacent items. If a
 * Relative Dimension is used, it is relative to the height of the border
 * specified by ItemBorder.</td>
 * <td>0</td>
 * </tr>
 * <tr>
 * <th><i>&lt;Name&gt;</i>Anchor</th>
 * <td>Anchor</td>
 * <td>The location at which to anchor the item within the space allocated for
 * it. See <b><a href="#ItemPlacement">Item Placement</a></b> for more
 * information.</td>
 * <td>The&nbsp;<b>DefaultAnchor</b>&nbsp;value.</td>
 * </tr>
 * <tr>
 * <th><i>&lt;Name&gt;</i>Color</th>
 * <td>Color</td>
 * <td>The color to use when drawing the item.</td>
 * <td>The&nbsp;<b>DefaultColor</b>&nbsp;value.</td>
 * </tr>
 * <tr>
 * <th><i>&lt;Name&gt;</i>Edge</th>
 * <td>Edge</td>
 * <td>The edge at which to place the item. See <b><a href="#ItemPlacement">Item
 * Placement</a></b> for more information.</td>
 * <td>The&nbsp;<b>DefaultEdge</b>&nbsp;value.</td>
 * </tr>
 * <tr>
 * <th><i>&lt;Name&gt;</i>Expand</th>
 * <td>Fill</td>
 * <td>How the item should be allowed to expand, if more space is available than
 * it needs.</td>
 * <td>The&nbsp;<b>DefaultExpand</b>&nbsp;value.</td>
 * </tr>
 * <tr>
 * <th><i>&lt;Name&gt;</i>Fill</th>
 * <td>Fill</td>
 * <td>How the item should fill its space.</td>
 * <td>The&nbsp;<b>DefaultFill</b>&nbsp;value.</td>
 * </tr>
 * <tr>
 * <th><i>&lt;Name&gt;</i>Font</th>
 * <td>Font</td>
 * <td>The font to use to display the item. Only used for <b>Text</b> items.</td>
 * <td>The&nbsp;<b>DefaultFont</b>&nbsp;value.</td>
 * </tr>
 * <tr>
 * <th><i>&lt;Name&gt;</i>Format</th>
 * <td>String</td>
 * <td>The format used to create the text to display. Only used for <b>Text</b>
 * items. See <b><a href="#TextFormatting">Text Formatting</a></b> for more
 * information.</td>
 * <td>NONE - except for specific fields:<br />
 * <nobr><b>ArtistFormat</b> = %A</nobr><br />
 * <nobr><b>CopyrightFormat</b> = Copyright \u00A9 %Y by %A</nobr><br />
 * <nobr><b>DateFormat</b> = %Y %4M %D</nobr><br * />
 * <nobr><b>TitleFormat</b> = %T</nobr></td>
 * </tr>
 * <tr>
 * <th><i>&lt;Name&gt;</i>Height</th>
 * <td>Absolute or Relative Dimension</td>
 * <td>The maximum height the item may occupy. If a Relative Dimension is used,
 * it is relative to the height of the border specified by <b>ItemBorder</b>.</td>
 * <td>The&nbsp;<b>DefaultHeight</b>&nbsp;value.</td>
 * </tr>
 * <tr>
 * <th><i>&lt;Name&gt;</i>Spacing</th>
 * <td>Absolute or Relative Dimension</td>
 * <td>The default amount of space to add in between each letter of the text
 * being displayed. If a Relative Dimension is used, it is relative to the width
 * of the border specified by <b>ItemBorder</b>. Only used for <b>Text</b>
 * items.</td>
 * <td>The&nbsp;<b>DefaultSpacing</b>&nbsp;value.</td>
 * </tr>
 * <tr>
 * <th><i>&lt;Name&gt;</i>Separator</th>
 * <td><b>Simple</b> or <b>Pointed</b></td>
 * <td>The type of separator line to use. Only used for <b>Separator</b> items.</td>
 * <td>The&nbsp;<b>DefaultSeparator</b>&nbsp;value.</td>
 * </tr>
 * <tr>
 * <th><i>&lt;Name&gt;</i>Type</th>
 * <td><b>Text</b> or <b>Separator</b></td>
 * <td>The type of item to be displayed.</td>
 * <td>The&nbsp;<b>DefaultType</b>&nbsp;value.</td>
 * </tr>
 * <tr>
 * <th><i>&lt;Name&gt;</i>Width</th>
 * <td>Absolute or Relative Dimension</td>
 * <td>The maximum width available for the item. If a Relative Dimension is
 * used, it is relative to the width of the border specified by
 * <b>ItemBorder</b>.</td>
 * <td>The&nbsp;<b>DefaultWidth</b>&nbsp;value.</td>
 * </tr>
 * </table>
 * <p>
 * The Option Types are:
 * </p>
 * <dl>
 * <dt>Anchor</dt>
 * <dd>
 * <p>
 * The location at which to &ldquo;anchor&rdquo; the contents of an item within
 * the space allocated to it. <b>CENTER</b> will center the item; <b>NORTH</b>,
 * <b>SOUTH</b>, <b>EAST</b>, or <b>WEST</b> will center the item along the
 * appropriate edge; and <b>NORTHEAST</b>, <b>NORTHWEST</b>, <b>SOUTHEAST</b>,
 * or <b>SOUTHWEST</b> will align the item in the appropriate corner.
 * </p>
 * </dd>
 * <dt>Color</dt>
 * <dd>
 * <p>
 * Either a known color name, or a numeric 24-bit RGB color value specified in
 * hexadecimal in the form #RRGGBB, where RR, GG, BB are the 8-bit hexadecimal
 * values for red, green, and blue, respectively.
 * </p>
 * <p>
 * Known color names are: black, blue, cyan, dark gray, gray, green, light gray,
 * magenta, orange, pink, red, white, yellow.
 * </p>
 * </dd>
 * <dt>Edge</dt>
 * <dd>
 * <p>
 * An edge of the space or picture. Can be one of: <b>NORTH</b> or <b>Top</b>,
 * <b>SOUTH</b> or <b>Bottom</b>, <b>EAST</b> or <b>Right</b>, or <b>WEST</b> or
 * <b>Left</b>.
 * </p>
 * </dd>
 * <dt>Fill</dt>
 * <dd>
 * <p>
 * The direction in which to expand or fill, which can be one of: <b>None</b>
 * (or <b>No</b>), <b>EastWest</b> (or <b>Horizontal</b> or <b>X</b>),
 * <b>NorthSouth</b> (or <b>Vertical</b> or <b>Y</b>), or <b>Both</b> (or
 * <b>Yes</b>).
 * </p>
 * </dd>
 * <dt>Absolute&nbsp;Dimension</dt>
 * <dd>
 * <p>
 * A dimension (either width or height) specified in pixels.
 * </p>
 * <p>
 * Example: <b>150</b>
 * </p>
 * </dd>
 * <dt>Relative&nbsp;Dimension</dt>
 * <dd>
 * <p>
 * A dimension (either width or height) specified as an integral percentage of
 * the picture's final (framed) width or height.
 * </p>
 * <p>
 * Example: <b>80%</b>
 * </p>
 * </dd>
 * <dt>Absolute&nbsp;or&nbsp;Relative&nbsp;Dimension</dt>
 * <dd>
 * <p>
 * Either an Absolute Dimension or a Relative Dimension. If the value ends with
 * a <b>%</b> sign, it's a Relative Dimension; otherwise, it's an Absolute
 * Dimension.
 * </p>
 * </dd>
 * </dl>
 * 
 * @author Copyright (c) 2009 by Johnson Earls. All rights reserved. See <A
 *         HREF="http://www.springhaven.org/~darkfox/FoxJava/LICENSE">
 *         http://www.springhaven.org/~darkfox/FoxJava/LICENSE </A> for details.
 * 
 */
public class FrameStyleFlex extends FrameStyleBorder {
	// ///////////////////////////////////////////////////////////////////// //

	/**
	 * Turn on debugging. Currently, this outputs debugging code to the console,
	 * as well as framing each parcel so you can see the size.
	 */
	boolean DEBUG;

	/**
	 * The title of the photo. While this <i>can</i> be passed in through the
	 * "Title" option, it's generally left undefined so the default value will
	 * be used. The default value is derived from the photo's filename, with the
	 * last trailing extension (.xxx) stripped off, and underscores (_) replaced
	 * by spaces in whatever remains.
	 */
	String photoTitle;

	/**
	 * The artist of the photo. If not passed in through the "Artist" option,
	 * this is the system user name for the user running the program.
	 */
	String photoArtist;

	/**
	 * The default {@link SpacePacker.Anchor} to use for items that don't
	 * specify their own.
	 */
	SpacePacker.Anchor defaultAnchor;

	/**
	 * The default {@link Color} to use for items that don't specify their own.
	 */
	Color defaultColor;

	/**
	 * The default {@link SpacePacker.Edge} to use for items that don't specify
	 * their own.
	 */
	SpacePacker.Edge defaultEdge;

	/**
	 * The default {@link SpacePacker.Expand} to use for items that don't
	 * specify their own.
	 */
	SpacePacker.Expand defaultExpand;

	/**
	 * The default {@link SpacePacker.Fill} to use for items that don't specify
	 * their own.
	 */
	SpacePacker.Fill defaultFill;

	/**
	 * The default height to use for items that don't specify their own.
	 */
	int defaultHeight;

	/**
	 * The default type (BorderText or BorderSeparator) to use for items that
	 * don't specify their own.
	 */
	Class<? extends BorderItem> defaultType;

	/**
	 * The default width to use for items that don't specify their own.
	 */
	int defaultWidth;

	/**
	 * The default {@link BorderSeparator.SeparatorStyle} to use for separators
	 * that don't specify their own.
	 */
	BorderSeparator.SeparatorStyle defaultSeparator;

	/**
	 * The default {@link Font} to use for text items that don't specify their
	 * own.
	 */
	Font defaultFont;
	/**
	 * The default inter-character spacing to use for text items that don't
	 * specify their own.
	 */
	int defaultSpacing;

	/**
	 * The amount of space to leave between items horizontally.
	 */
	int horizontalGap;

	/**
	 * The amount of space to leave between items vertically.
	 */
	int verticalGap;

	/**
	 * The border in which to render the items.
	 */
	Edge itemBorder;

	/**
	 * The border box to use when rendering the items.
	 */
	SpacePacker borderBox;

	/**
	 * The list of items being rendered.
	 */
	BorderItem itemList[];

	// ///////////////////////////////////////////////////////////////////// //

	/**
	 * Base class for items that will be drawn into the border.
	 * 
	 * @author Copyright (c) 2009 by Johnson Earls. All rights reserved. See <A
	 *         HREF="http://www.springhaven.org/~darkfox/FoxJava/LICENSE">
	 *         http://www.springhaven.org/~darkfox/FoxJava/LICENSE </A> for
	 *         details.
	 * 
	 */
	public abstract static class BorderItem {
		/**
		 * The position (edge) of the item.
		 */
		public final Edge position;

		/**
		 * The anchor location of the item.
		 */
		public final Anchor anchor;

		/**
		 * How the item should be allowed to expand.
		 */
		public final Expand expand;

		/**
		 * How the item should be fill its space.
		 */
		public final Fill fill;

		/**
		 * The {@link Graphics2D} graphics context that will be used to draw the
		 * item.
		 */
		Graphics2D graphics = null;

		/**
		 * Creates a new Border Item.
		 * 
		 * @param position
		 *            The edge in which to place the item.
		 * @param anchor
		 *            Where to anchor the item within its space.
		 * @param expand
		 *            How the item should be allowed to expand.
		 * @param fill
		 *            How the item should fill its space.
		 */
		public BorderItem(Edge position, Anchor anchor, Expand expand, Fill fill) {
			this.position = position;
			this.anchor = anchor;
			this.expand = expand;
			this.fill = fill;
		}

		@Override
		public String toString() {
			return "BorderItem[position=" + this.position + ",anchor="
					+ this.anchor + ",expand=" + this.expand + ",fill="
					+ this.fill + "]";
		}

		/**
		 * <p>
		 * Sets up the item for drawing. This must be called before any other
		 * operation on the object.
		 * </p>
		 * 
		 * <p>
		 * Overriding methods should chain-call this method first!
		 * </p>
		 * 
		 * @param graphics
		 *            The {@link Graphics2D} graphics context into which to draw
		 *            the item.
		 */
		public void setupItem(Graphics2D graphics) {
			this.graphics = graphics;
		}

		/**
		 * <p>
		 * Computes the item's minimum size.
		 * </p>
		 * 
		 * <p>
		 * Overriding methods should chain-call this method first, and
		 * incorporate the return from this method into their minimum size.
		 * </p>
		 * 
		 * @return the Dimension containing the smallest parcel the item will
		 *         fit into.
		 * @throws IllegalStateException
		 *             if setupItem has not yet been called for this item.
		 */
		public Dimension getMinimumSize() throws IllegalStateException {
			if (this.graphics == null) {
				throw new IllegalStateException(
						"Graphics context not yet initialized.");
			}
			return new Dimension(1, 1);
		}

		/**
		 * Returns how the item should be allowed to expand.
		 */
		public Expand getExpand() {
			return this.expand;
		}

		/**
		 * Returns how the item should fill its space.
		 */
		public Fill getFill() {
			return this.fill;
		}

		/**
		 * <p>
		 * Renders the item into the already-specified graphics context.
		 * </p>
		 * 
		 * @param location
		 *            The upper left corner of the space in which to draw the
		 *            item.
		 * @param size
		 *            The size of the space in which to draw the item.
		 */
		public abstract void renderItem(Point location, Dimension size);
	}

	/**
	 * A Text item to be drawn into the border.
	 * 
	 * @author Copyright (c) 2009 by Johnson Earls. All rights reserved. See <A
	 *         HREF="http://www.springhaven.org/~darkfox/FoxJava/LICENSE">
	 *         http://www.springhaven.org/~darkfox/FoxJava/LICENSE </A> for
	 *         details.
	 * 
	 */
	public static class BorderText extends BorderItem {
		/**
		 * The text to be drawn. This must be formatted before being created!
		 */
		public final String text;

		/**
		 * The {link Font} to use to draw the text.
		 */
		public final Font font;

		/**
		 * The {link Color} to use to draw the text.
		 */
		public final Color color;

		/**
		 * The amount of spacing, in pixels, to add in between letters of the
		 * font.
		 */
		public final int spacing;

		/**
		 * The maximum size to use for the text. The height of this dimension
		 * determines the height of the text; if the resulting width of the text
		 * (including the spacing) would be wider than the width of this
		 * dimension, the text (and spacing) are scaled horizontally (but not
		 * vertically) to fit.
		 */
		public final Dimension maxSize;

		/**
		 * The minimum size required to draw the text; computed based on the
		 * information passed into the constructor.
		 */
		Dimension minSize = null;

		/**
		 * The derived font to use to actually draw the text.
		 */
		Font derivedFont;

		/**
		 * Creates the BorderText item and sets up the initial parameters.
		 * 
		 * @param position
		 *            The edge in which to place the text.
		 * @param anchor
		 *            Where to anchor the text within its space.
		 * @param expand
		 *            How the text should be allowed to expand.
		 * @param fill
		 *            How the text should fill its space.
		 * @param text
		 *            The text to draw.
		 * @param font
		 *            The {@link Font} to use to draw the text.
		 * @param color
		 *            The {@link Color} to use to draw the text.
		 * @param spacing
		 *            The spacing, in pixels, to add between letters when
		 *            drawing the text.
		 * @param maxSize
		 *            The maximum space available to draw the text.
		 */
		public BorderText(Edge position, Anchor anchor, Expand expand,
				Fill fill, String text, Font font, Color color, int spacing,
				Dimension maxSize) {
			super(position, anchor, expand, fill);
			this.text = text;
			this.font = font;
			this.color = color;
			this.spacing = spacing;
			this.maxSize = new Dimension(maxSize);
		}

		/**
		 * Returns a string that identifies this BorderText item.
		 */
		@Override
		public String toString() {
			return "BorderText[position=" + this.position + ",anchor="
					+ this.anchor + ",expand=" + this.expand + ",fill="
					+ this.fill + ",text=\"" + this.text + "\",font="
					+ this.font + ",color=" + this.color + ",spacing="
					+ this.spacing + ",maxSize=" + this.maxSize + "]";
		}

		/**
		 * <p>
		 * Computes the minimum size and actual font to use to draw the text.
		 * </p>
		 * 
		 * @see FrameStyleFlex.BorderItem#setupItem(java.awt.Graphics2D)
		 */
		@Override
		public void setupItem(Graphics2D graphics) {
			// System.err.println("* BorderText.setupitem(\"" + this.text +
			// "\")");
			super.setupItem(graphics);
			// set up the initial textLayoutAttributes and the tracking value
			// for horizontal spacing.
			float trackingX = (float) this.spacing / maxSize.height;
			// System.err.println("*   trackingX = " + trackingX + " (" +
			// this.spacing + " / " + maxSize.height + ")");
			Map<TextAttribute, Object> textLayoutAttributes = new HashMap<TextAttribute, Object>();
			textLayoutAttributes.put(TextAttribute.TRACKING, new Float(
					trackingX));

			// figure out how to scale the font to fit.
			FontRenderContext frc = graphics.getFontRenderContext();
			Rectangle bounds;
			textLayoutAttributes.put(TextAttribute.SIZE, this.maxSize.height);
			this.derivedFont = this.font.deriveFont(textLayoutAttributes);
			bounds = new TextLayout(this.text, this.derivedFont, frc)
					.getBounds().getBounds();
			float scaleY = (float) maxSize.height / bounds.height;
			this.derivedFont = this.font.deriveFont(textLayoutAttributes)
					.deriveFont(
							AffineTransform.getScaleInstance(scaleY, scaleY));
			bounds = new TextLayout(this.text, this.derivedFont, frc)
					.getBounds().getBounds();
			float scaleX = (float) maxSize.width / bounds.width;
			if (scaleX < 1.0f) {
				this.derivedFont = this.font.deriveFont(textLayoutAttributes)
						.deriveFont(
								AffineTransform.getScaleInstance(scaleX
										* scaleY, scaleY));
			}
			bounds = new TextLayout(this.text, this.derivedFont, frc)
					.getBounds().getBounds();
			this.minSize = new Dimension(bounds.getSize());
		}

		/**
		 * <p>
		 * Returns the minimum size needed to draw the text.
		 * </p>
		 * 
		 * @see FrameStyleFlex.BorderItem#getMinimumSize()
		 */
		@Override
		public Dimension getMinimumSize() throws IllegalStateException {
			Dimension superMinSize = super.getMinimumSize();
			Dimension myMinSize = new Dimension(minSize);
			if (superMinSize.width > myMinSize.width) {
				myMinSize.setSize(superMinSize.width, myMinSize.height);
			}
			if (superMinSize.height > myMinSize.height) {
				myMinSize.setSize(myMinSize.width, superMinSize.height);
			}
			return myMinSize;
		}

		/**
		 * <p>
		 * Renders the item into the already-specified graphics context.
		 * </p>
		 * 
		 * @param location
		 *            The upper left corner of the space in which to draw the
		 *            item.
		 * @param size
		 *            The size of the space in which to draw the item.
		 */
		@Override
		public void renderItem(Point location, Dimension size) {
			AffineTransform xform = new AffineTransform(
					this.derivedFont.getTransform());
			xform.scale((float) size.width / this.minSize.width,
					(float) size.height / this.minSize.height);
			Font scaledFont = this.derivedFont.deriveFont(xform);
			this.graphics.setColor(this.color);
			TextLayout textLayout = new TextLayout(this.text, scaledFont,
					this.graphics.getFontRenderContext());
			Rectangle bounds = textLayout.getBounds().getBounds();
			textLayout.draw(this.graphics, location.x, location.y - bounds.y);
		}
	}

	public static class BorderSeparator extends BorderItem {

		/**
		 * The list of implemented separator styles:
		 * <dl>
		 * <dt>NONE</dt>
		 * <dd>No visible separator - just space</dd>
		 * <dt>SIMPLE</dt>
		 * <dd>A simple, solid rectangle</dd>
		 * <dt>Pointed</dt>
		 * <dt>A &lsquo;pointed&rsquo; line; with convex curving to points at
		 * the left and right sides.</dd>
		 * <dt>Star</dt>
		 * <dd>A 4-pointed star, with concave curves forming points at the
		 * midpoints of the edges.</dd>
		 * </dl>
		 * 
		 */
		public static enum SeparatorStyle {
			NONE, SIMPLE, POINTED, STAR
		};

		/**
		 * The style of this BorderSeparator.
		 */
		public final SeparatorStyle style;

		/**
		 * The color of this BorderSeparator.
		 */
		public final Color color;

		/**
		 * The maximum size this BorderSeparator can expand to.
		 */
		public final Dimension maxSize;

		/**
		 * The minimim size this BorderSeparator can shrink to.
		 */
		final Dimension minSize;

		/**
		 * Creates the BorderSeparator item and sets up the initial parameters.
		 * 
		 * @param position
		 *            The edge in which to place the separator.
		 * @param anchor
		 *            Where to anchor the separator within its space.
		 * @param expand
		 *            How the separator should be allowed to expand.
		 * @param fill
		 *            How the separator should fill its space.
		 * @param style
		 *            The style of separator to draw.
		 * @param color
		 *            The {@link Color} to use to draw the separator.
		 * @param maxSize
		 *            The maximum space available to draw the separator.
		 */
		public BorderSeparator(Edge position, Anchor anchor, Expand expand,
				Fill fill, SeparatorStyle style, Color color, Dimension maxSize) {
			super(position, anchor, expand, fill);
			this.style = style;
			this.color = color;
			this.maxSize = new Dimension(maxSize);
			this.minSize = new Dimension(maxSize);
		}

		/**
		 * Returns a string that identifies this BorderSeparator item.
		 */
		@Override
		public String toString() {
			return "BorderSeparator[position=" + this.position + ",anchor="
					+ this.anchor + ",expand=" + this.expand + ",fill="
					+ this.fill + ",style=" + this.style + ",color="
					+ this.color + ",maxSize=" + this.maxSize + "]";
		}

		/**
		 * <p>
		 * Computes the minimum size and actual font to use to draw the text.
		 * </p>
		 * 
		 * @see FrameStyleFlex.BorderItem#setupItem(java.awt.Graphics2D)
		 */
		@Override
		public void setupItem(Graphics2D graphics) {
			super.setupItem(graphics);
		}

		/**
		 * <p>
		 * Returns the minimum size needed to draw the text.
		 * </p>
		 * 
		 * @see FrameStyleFlex.BorderItem#getMinimumSize()
		 */
		@Override
		public Dimension getMinimumSize() throws IllegalStateException {
			Dimension superMinSize = super.getMinimumSize();
			Dimension myMinSize = new Dimension(minSize);
			if (superMinSize.width > myMinSize.width) {
				myMinSize.setSize(superMinSize.width, myMinSize.height);
			}
			if (superMinSize.height > myMinSize.height) {
				myMinSize.setSize(myMinSize.width, superMinSize.height);
			}
			return myMinSize;
		}

		/**
		 * <p>
		 * Renders the item into the already-specified graphics context.
		 * </p>
		 * 
		 * @param location
		 *            The upper left corner of the space in which to draw the
		 *            item.
		 * @param size
		 *            The size of the space in which to draw the item.
		 */
		@Override
		public void renderItem(Point location, Dimension size) {
			this.graphics.setColor(this.color);
			switch (this.style) {
			case NONE:
				break;
			case SIMPLE:
				this.graphics.fillRect(location.x, location.y, size.width,
						size.height);
				break;
			case POINTED: {
				double topY = location.y;
				double bottomY = (location.y + size.height);
				double midY = (topY + bottomY) * 0.5;
				double leftX = location.x;
				double rightX = (location.x + size.width - 1);
				double midleftX = leftX * 0.75 + rightX * 0.25;
				double midrightX = leftX * 0.25 + rightX * 0.75;
				Path2D.Double linePath = new Path2D.Double();
				linePath.moveTo(leftX, midY);
				linePath.curveTo(midleftX, topY - 1, midrightX, topY - 1,
						rightX, midY);
				linePath.curveTo(midrightX, bottomY + 1, midleftX, bottomY + 1,
						leftX, midY);
				this.graphics.draw(linePath);
				this.graphics.fill(linePath);
			}
				break;
			case STAR: {
				double topY = location.y;
				double bottomY = (location.y + size.height);
				double midTopY = topY * 0.75 + bottomY * 0.25;
				double midY = topY * 0.50 + bottomY * 0.50;
				double midBottomY = topY * 0.25 + bottomY * 0.75;
				double leftX = location.x;
				double rightX = (location.x + size.width - 1);
				double midLeftX = leftX * 0.75 + rightX * 0.25;
				double midX = leftX * 0.50 + rightX * 0.50;
				double midRightX = leftX * 0.25 + rightX * 0.75;
				Path2D.Double linePath = new Path2D.Double();
				linePath.moveTo(leftX, midY);
				linePath.curveTo(midLeftX, midY, midX, midTopY, midX, topY);
				linePath.curveTo(midX, midTopY, midRightX, midY, rightX, midY);
				linePath.curveTo(midRightX, midY, midX, midBottomY, midX,
						bottomY);
				linePath.curveTo(midX, midBottomY, midLeftX, midY, leftX, midY);
				this.graphics.draw(linePath);
				this.graphics.fill(linePath);
			}
				break;
			}
		}
	}

	/**
	 * The SpacePacker box that will hold the text and separators to be rendered
	 * in the border.
	 */
	SpacePacker flexBox = null;

	/**
	 * Returns a string that has a given number of a character in it (for
	 * example, 5 'a's).
	 * 
	 * @param count
	 *            How many of the character to include in the returned string.
	 * @param ch
	 *            The character to repeat.
	 * @return The string.
	 */
	static String fillCharacter(int count, char ch) {
		String filled = "";
		while (count > 0) {
			filled += ch;
			count -= 1;
		}
		return filled;
	}

	/**
	 * Converts a Flex text format string to a {@link SimpleDateFormat} object.
	 * Any format codes that do not refer to parts of the photo's time&date are
	 * converted to strings immediately, using the parameters already known to
	 * the framing class.
	 * 
	 * @param format
	 *            The Flex text format string.
	 * @param sourceImage
	 *            The image we're processing.
	 * @return A SimpleDateFormat object to format the photo's date/time
	 *         properly for this format.
	 */
	SimpleDateFormat formatCodeToSimpleDateFormat(String format,
			MetaImage sourceImage) {
		String originalFormat = format;
		String formatCode = "";
		boolean formatInString = false;
		for (int idx = format.indexOf('%'); idx > -1; idx = format.indexOf('%')) {
			int width = -1;
			if (idx > 0) {
				if (!formatInString) {
					formatInString = true;
					formatCode += "'";
				}
				formatCode += format.substring(0, idx).replaceAll("'", "''");
				format = format.substring(idx);
			}
			idx = 1;
			if (Character.isDigit(format.charAt(idx))) {
				width = 0;
				while (Character.isDigit(format.charAt(idx))) {
					width = width * 10
							+ "0123456789".indexOf(format.charAt(idx));
					idx += 1;
				}
			}
			char formatCodeChar = format.charAt(idx);
			idx += 1;
			switch (formatCodeChar) {
			case '%':
				if (width == 0) {
					width = 1;
				}
				if (!formatInString) {
					formatInString = true;
					formatCode += "'";
				}
				formatCode += fillCharacter(width, '%');
				break;
			case 'a':
				if (width == 0) {
					width = 2;
				}
				if (formatInString) {
					formatInString = false;
					formatCode += "'";
				}
				formatCode += fillCharacter(width, 'a');
				break;
			case 'A':
				if (!formatInString) {
					formatInString = true;
					formatCode += "'";
				}
				formatCode += photoArtist.replace("'", "''");
				break;
			case 'D':
				if (width < 1) {
					width = 2;
				}
				if (formatInString) {
					formatInString = false;
					formatCode += "'";
				}
				formatCode += fillCharacter(width, 'd');
				break;
			case 'h':
				if (width < 1) {
					width = 2;
				}
				if (formatInString) {
					formatInString = false;
					formatCode += "'";
				}
				formatCode += fillCharacter(width, 'H');
				break;
			case 'i':
				if (width < 1) {
					width = 2;
				}
				if (formatInString) {
					formatInString = false;
					formatCode += "'";
				}
				formatCode += fillCharacter(width, 'h');
				break;
			case 'm':
				if (width < 1) {
					width = 2;
				}
				if (formatInString) {
					formatInString = false;
					formatCode += "'";
				}
				formatCode += fillCharacter(width, 'm');
				break;
			case 'M':
				if (width < 1) {
					width = 3;
				}
				if (formatInString) {
					formatInString = false;
					formatCode += "'";
				}
				formatCode += fillCharacter(width, 'M');
				break;
			case 's':
				if (width < 1) {
					width = 2;
				}
				if (formatInString) {
					formatInString = false;
					formatCode += "'";
				}
				formatCode += fillCharacter(width, 's');
				break;
			case 'T':
				if (!formatInString) {
					formatInString = true;
					formatCode += "'";
				}
				formatCode += photoTitle.replace("'", "''");
				break;
			case 'Y':
				if ((width < 0) || (width > 2)) {
					width = 4;
				} else {
					width = 2;
				}
				if (formatInString) {
					formatInString = false;
					formatCode += "'";
				}
				formatCode += fillCharacter(width, 'y');
				break;
			case 'X':
				formatCodeChar = format.charAt(idx);
				idx += 1;
				int tagNumber = -1;
				switch (formatCodeChar) {
				case 'A':
					tagNumber = EXIFInfo.Tag.TagFNumber;
					break;
				case 'C':
					tagNumber = EXIFInfo.Tag.TagModel;
					break;
				case 'F':
					tagNumber = EXIFInfo.Tag.TagFocalLengthIn35mmFilm;
					break;
				case 'I':
					tagNumber = EXIFInfo.Tag.TagISOSpeedRatings;
					break;
				case 'M':
					tagNumber = EXIFInfo.Tag.TagMake;
					break;
				case 'S':
					tagNumber = EXIFInfo.Tag.TagShutterSpeedValue;
					break;
				default:
					if (!formatInString) {
						formatInString = true;
						formatCode += "'";
					}
					formatCode += "--unknown format code X" + formatCodeChar
							+ "--";
				}
				if (tagNumber >= 0) {
					EXIFInfo.Tag tag = EXIFInfo.getTagInfo(tagNumber);
					switch (tag.tagType) {
					case EXIFInfo.Tag.TagTypeRational:
						Rational rationalTag = (Rational) (sourceImage
								.getExifInfo().get(tagNumber)[0]);
						if (width > 0) {
							if (!formatInString) {
								formatInString = true;
								formatCode += "'";
							}
							formatCode += rationalTag.toDecimalString(width);
						} else {
							if (!formatInString) {
								formatInString = true;
								formatCode += "'";
							}
							formatCode += rationalTag.toFractionalString(true);
						}
						break;
					case EXIFInfo.Tag.TagTypeASCII:
					case EXIFInfo.Tag.TagTypeTypedString:
						String stringTag = (String) (sourceImage.getExifInfo()
								.get(tagNumber)[0]);
						if (!formatInString) {
							formatInString = true;
							formatCode += "'";
						}
						formatCode += (stringTag.replaceAll("'", "''"));
						break;
					case EXIFInfo.Tag.TagTypeByte:
					case EXIFInfo.Tag.TagTypeSByte:
						// because of the possibility of unsigned
						// values, EXIF
						// 8-bit "Byte" values are all stored in a java
						// Short
						Short byteTag = (Short) (sourceImage.getExifInfo().get(
								tagNumber)[0]);
						if (!formatInString) {
							formatInString = true;
							formatCode += "'";
						}
						formatCode += byteTag.toString();
						break;
					case EXIFInfo.Tag.TagTypeShort:
					case EXIFInfo.Tag.TagTypeSShort:
						// because of the possibility of unsigned
						// values, EXIF
						// 16-bit "Short" values are all stored in a
						// java
						// Integer
						Integer shortTag = (Integer) (sourceImage.getExifInfo()
								.get(tagNumber)[0]);
						if (!formatInString) {
							formatInString = true;
							formatCode += "'";
						}
						formatCode += shortTag.toString();
						break;
					case EXIFInfo.Tag.TagTypeLong:
					case EXIFInfo.Tag.TagTypeSLong:
						// because of the possibility of unsigned
						// values, EXIF
						// 32-bit "Long" values are all stored in a java
						// Long
						Long longTag = (Long) (sourceImage.getExifInfo().get(
								tagNumber)[0]);
						if (!formatInString) {
							formatInString = true;
							formatCode += "'";
						}
						formatCode += longTag.toString();
						break;
					default:
						if (!formatInString) {
							formatInString = true;
							formatCode += "'";
						}
						formatCode += "-- Unknown tag type for EXIF tag "
								+ tag.tagName.replaceAll("'", "''") + " --";
					}
				}
				break;
			default:
				if (!formatInString) {
					formatInString = true;
					formatCode += "'";
				}
				formatCode += "--unknown format code " + formatCodeChar + "--";
			}
			format = format.substring(idx);
		}
		if (format.length() > 0) {
			if (!formatInString) {
				formatInString = true;
				formatCode += "'";
			}
			formatCode += format.replaceAll("'", "''");
		}
		if (formatInString) {
			formatInString = false;
			formatCode += "'";
		}
		if (this.DEBUG) {
			System.err.println("formatCodeToSimpleDateFormat: \""
					+ originalFormat + "\" -> \"" + formatCode + "\"");
		}
		return new SimpleDateFormat(formatCode);
	}

	/**
	 * Convenience function which parses a property as an Anchor value. Throws
	 * IllegalArgumentException with the name of the property if the value is
	 * not parseable as an Anchor.
	 * 
	 * @param options
	 *            The properties to use to retrieve the property value.
	 * @param propertyName
	 *            The name of the property to parse.
	 * @param defaultValue
	 *            The default value to use if the property does not exist.
	 * @return The Anchor value parsed from the property.
	 * @throws IllegalArgumentException
	 *             if the property value is not parseable as an Anchor.
	 */
	public Anchor parsePropertyAsAnchor(Properties options,
			String propertyName, String defaultValue)
			throws IllegalArgumentException {
		Anchor retval = null;
		try {
			String anchorValue = options
					.getProperty(propertyName, defaultValue);
			retval = Anchor.valueOf(Anchor.class, anchorValue.toUpperCase());
		} catch (IllegalArgumentException e) {
			throw new IllegalArgumentException(propertyName);
		}
		return retval;
	}

	/**
	 * Convenience function which parses a property as an Anchor value. Throws
	 * IllegalArgumentException with the name of the property if the value is
	 * not parseable as an Anchor.
	 * 
	 * @param options
	 *            The properties to use to retrieve the property value.
	 * @param propertyName
	 *            The name of the property to parse.
	 * @param defaultValue
	 *            The default value to use if the property does not exist.
	 * @return The Anchor value parsed from the property.
	 * @throws IllegalArgumentException
	 *             if the property value is not parseable as an Anchor.
	 */
	public Anchor parsePropertyAsAnchor(Properties options,
			String propertyName, Anchor defaultValue)
			throws IllegalArgumentException {
		if (options.containsKey(propertyName)) {
			return this.parsePropertyAsAnchor(options, propertyName, "");
		} else {
			return defaultValue;
		}
	}

	/**
	 * Convenience function which parses a property as an Edge value. Throws
	 * IllegalArgumentException with the name of the property if the value is
	 * not parseable as an Edge.
	 * 
	 * @param options
	 *            The properties to use to retrieve the property value.
	 * @param propertyName
	 *            The name of the property to parse.
	 * @param defaultValue
	 *            The default value to use if the property does not exist.
	 * @return The Edge value parsed from the property.
	 * @throws IllegalArgumentException
	 *             if the property value is not parseable as an Edge.
	 */
	public Edge parsePropertyAsEdge(Properties options, String propertyName,
			String defaultValue) throws IllegalArgumentException {
		Edge retval = null;
		try {
			String edgeValue = options.getProperty(propertyName, defaultValue);
			retval = Edge.valueOf(Edge.class, edgeValue.toUpperCase());
		} catch (IllegalArgumentException e) {
			throw new IllegalArgumentException(propertyName);
		}
		return retval;
	}

	/**
	 * Convenience function which parses a property as an Edge value. Throws
	 * IllegalArgumentException with the name of the property if the value is
	 * not parseable as an Edge.
	 * 
	 * @param options
	 *            The properties to use to retrieve the property value.
	 * @param propertyName
	 *            The name of the property to parse.
	 * @param defaultValue
	 *            The default value to use if the property does not exist.
	 * @return The Edge value parsed from the property.
	 * @throws IllegalArgumentException
	 *             if the property value is not parseable as an Edge.
	 */
	public Edge parsePropertyAsEdge(Properties options, String propertyName,
			Edge defaultValue) throws IllegalArgumentException {
		if (options.containsKey(propertyName)) {
			return this.parsePropertyAsEdge(options, propertyName, "");
		} else {
			return defaultValue;
		}
	}

	/**
	 * Convenience function which parses a property as an Expand value. Throws
	 * IllegalArgumentException with the name of the property if the value is
	 * not parseable as an Expand.
	 * 
	 * @param options
	 *            The properties to use to retrieve the property value.
	 * @param propertyName
	 *            The name of the property to parse.
	 * @param defaultValue
	 *            The default value to use if the property does not exist.
	 * @return The Expand value parsed from the property.
	 * @throws IllegalArgumentException
	 *             if the property value is not parseable as an Expand.
	 */
	public Expand parsePropertyAsExpand(Properties options,
			String propertyName, String defaultValue)
			throws IllegalArgumentException {
		Expand retval = null;
		try {
			String expandValue = options
					.getProperty(propertyName, defaultValue);
			retval = Expand.valueOf(Expand.class, expandValue.toUpperCase());
		} catch (IllegalArgumentException e) {
			throw new IllegalArgumentException(propertyName);
		}
		return retval;
	}

	/**
	 * Convenience function which parses a property as an Expand value. Throws
	 * IllegalArgumentException with the name of the property if the value is
	 * not parseable as an Expand.
	 * 
	 * @param options
	 *            The properties to use to retrieve the property value.
	 * @param propertyName
	 *            The name of the property to parse.
	 * @param defaultValue
	 *            The default value to use if the property does not exist.
	 * @return The Expand value parsed from the property.
	 * @throws IllegalArgumentException
	 *             if the property value is not parseable as an Expand.
	 */
	public Expand parsePropertyAsExpand(Properties options,
			String propertyName, Expand defaultValue)
			throws IllegalArgumentException {
		if (options.containsKey(propertyName)) {
			return this.parsePropertyAsExpand(options, propertyName, "");
		} else {
			return defaultValue;
		}
	}

	/**
	 * Convenience function which parses a property as an Fill value. Throws
	 * IllegalArgumentException with the name of the property if the value is
	 * not parseable as an Fill.
	 * 
	 * @param options
	 *            The properties to use to retrieve the property value.
	 * @param propertyName
	 *            The name of the property to parse.
	 * @param defaultValue
	 *            The default value to use if the property does not exist.
	 * @return The Fill value parsed from the property.
	 * @throws IllegalArgumentException
	 *             if the property value is not parseable as an Fill.
	 */
	public Fill parsePropertyAsFill(Properties options, String propertyName,
			String defaultValue) throws IllegalArgumentException {
		Fill retval = null;
		try {
			String fillValue = options.getProperty(propertyName, defaultValue);
			retval = Fill.valueOf(Fill.class, fillValue.toUpperCase());
		} catch (IllegalArgumentException e) {
			throw new IllegalArgumentException(propertyName);
		}
		return retval;
	}

	/**
	 * Convenience function which parses a property as an Fill value. Throws
	 * IllegalArgumentException with the name of the property if the value is
	 * not parseable as an Fill.
	 * 
	 * @param options
	 *            The properties to use to retrieve the property value.
	 * @param propertyName
	 *            The name of the property to parse.
	 * @param defaultValue
	 *            The default value to use if the property does not exist.
	 * @return The Fill value parsed from the property.
	 * @throws IllegalArgumentException
	 *             if the property value is not parseable as an Fill.
	 */
	public Fill parsePropertyAsFill(Properties options, String propertyName,
			Fill defaultValue) throws IllegalArgumentException {
		if (options.containsKey(propertyName)) {
			return this.parsePropertyAsFill(options, propertyName, "");
		} else {
			return defaultValue;
		}
	}

	/**
	 * Convenience function which parses a property as a Separator Style value.
	 * Throws IllegalArgumentException with the name of the property if the
	 * value is not parseable as a Separator Style.
	 * 
	 * @param options
	 *            The properties to use to retrieve the property value.
	 * @param propertyName
	 *            The name of the property to parse.
	 * @param defaultValue
	 *            The default value to use if the property does not exist.
	 * @return The Separator Style value parsed from the property.
	 * @throws IllegalArgumentException
	 *             if the property value is not parseable as a Separator Style .
	 */
	public BorderSeparator.SeparatorStyle parsePropertyAsSeparatorStyle(
			Properties options, String propertyName, String defaultValue)
			throws IllegalArgumentException {
		BorderSeparator.SeparatorStyle retval = null;
		try {
			String edgeValue = options.getProperty(propertyName, defaultValue);
			retval = BorderSeparator.SeparatorStyle.valueOf(
					BorderSeparator.SeparatorStyle.class,
					edgeValue.toUpperCase());
		} catch (IllegalArgumentException e) {
			throw new IllegalArgumentException(propertyName);
		}
		return retval;
	}

	/**
	 * Convenience function which parses a property as a Separator Style value.
	 * Throws IllegalArgumentException with the name of the property if the
	 * value is not parseable as a Separator Style.
	 * 
	 * @param options
	 *            The properties to use to retrieve the property value.
	 * @param propertyName
	 *            The name of the property to parse.
	 * @param defaultValue
	 *            The default value to use if the property does not exist.
	 * @return The Separator Style value parsed from the property.
	 * @throws IllegalArgumentException
	 *             if the property value is not parseable as a Separator Style .
	 */
	public BorderSeparator.SeparatorStyle parsePropertyAsSeparatorStyle(
			Properties options, String propertyName,
			BorderSeparator.SeparatorStyle defaultValue)
			throws IllegalArgumentException {
		if (options.containsKey(propertyName)) {
			return this
					.parsePropertyAsSeparatorStyle(options, propertyName, "");
		} else {
			return defaultValue;
		}
	}

	/**
	 * Convenience function which parses a property as an Item Type value.
	 * Throws IllegalArgumentException with the name of the property if the
	 * value is not parseable as an Item Type.
	 * 
	 * @param options
	 *            The properties to use to retrieve the property value.
	 * @param propertyName
	 *            The name of the property to parse.
	 * @param defaultValue
	 *            The default value to use if the property does not exist.
	 * @return The Item Type value parsed from the property.
	 * @throws IllegalArgumentException
	 *             if the property value is not parseable as an Item Type.
	 */
	public Class<? extends BorderItem> parsePropertyAsItemType(
			Properties options, String propertyName, String defaultValue) {
		Class<? extends BorderItem> retval;
		String itemType = options.getProperty(propertyName, defaultValue)
				.toLowerCase();
		if (itemType.equals("text")) {
			retval = BorderText.class;
		} else if (itemType.equals("separator")) {
			retval = BorderSeparator.class;
		} else {
			throw new IllegalArgumentException(propertyName);
		}
		return retval;
	}

	/**
	 * <p>
	 * Convenience function which parses a property as an Item Type value.
	 * Throws IllegalArgumentException with the name of the property if the
	 * value is not parseable as an Item Type.
	 * </p>
	 * 
	 * <p>
	 * <i>WARNING!</i> This assumes the classes implementing the border types
	 * are named the same as the border type with a prefix of
	 * &ldquo;<b>Border</b>&rdquo;!
	 * </p>
	 * 
	 * @param options
	 *            The properties to use to retrieve the property value.
	 * @param propertyName
	 *            The name of the property to parse.
	 * @param defaultValue
	 *            The default value to use if the property does not exist.
	 * @return The Item Type value parsed from the property.
	 * @throws IllegalArgumentException
	 *             if the property value is not parseable as an Item Type.
	 */
	public Class<? extends BorderItem> parsePropertyAsItemType(
			Properties options, String propertyName,
			Class<? extends BorderItem> defaultValue) {
		if (options.containsKey(propertyName)) {
			return this.parsePropertyAsItemType(options, propertyName, "");
		} else {
			return defaultValue;
		}
	}

	/**
	 * Creates a BorderItem out of the properties for a given item name.
	 * 
	 * @param sourceImage
	 *            The image we're processing.
	 * @param options
	 *            The options from the frame.prop files found.
	 * @param itemName
	 *            The item name to use as a prefix for the item properties.
	 * @param resourceLoader
	 *            The ClassLoader used to load resources needed by the frame.
	 * @return The BorderItem representing this item, or <b>null</b> if the
	 *         item's type is unknown.
	 */
	public BorderItem parseBorderItem(MetaImage sourceImage,
			Properties options, String itemName, ClassLoader resourceLoader) {
		Class<? extends BorderItem> itemType = this.parsePropertyAsItemType(
				options, itemName + "Type", defaultType);

		BorderItem retval = null;

		if (itemType == BorderText.class) {
			Anchor itemAnchor = this.parsePropertyAsAnchor(options, itemName
					+ "Anchor", defaultAnchor);
			Color itemColor = this.parsePropertyAsColor(options, itemName
					+ "Color", defaultColor);
			Expand itemExpand = this.parsePropertyAsExpand(options, itemName
					+ "Expand", defaultExpand);
			Fill itemFill = this.parsePropertyAsFill(options,
					itemName + "Fill", defaultFill);
			Edge itemEdge = this.parsePropertyAsEdge(options,
					itemName + "Edge", defaultEdge);
			int itemHeight = this.parsePropertyAsIntegerOrPercentage(options,
					itemName + "Height", defaultHeight);
			int itemWidth = this.parsePropertyAsIntegerOrPercentage(options,
					itemName + "Width", defaultWidth);
			Dimension itemMaxSize = new Dimension(itemWidth, itemHeight);
			int itemSpacing = this.parsePropertyAsIntegerOrPercentage(options,
					itemName + "Spacing", defaultSpacing);
			String itemText = options.getProperty(itemName + "Format", "");
			itemText = this.formatCodeToSimpleDateFormat(itemText, sourceImage)
					.format(sourceImage.getDate());
			Font itemFont = this.parsePropertyAsFont(options,
					itemName + "Font", defaultFont, resourceLoader);
			retval = new BorderText(itemEdge, itemAnchor, itemExpand, itemFill,
					itemText, itemFont, itemColor, itemSpacing, itemMaxSize);
		} else if (itemType == BorderSeparator.class) {
			Anchor itemAnchor = this.parsePropertyAsAnchor(options, itemName
					+ "Anchor", defaultAnchor);
			Color itemColor = this.parsePropertyAsColor(options, itemName
					+ "Color", defaultColor);
			Expand itemExpand = this.parsePropertyAsExpand(options, itemName
					+ "Expand", defaultExpand);
			Fill itemFill = this.parsePropertyAsFill(options,
					itemName + "Fill", defaultFill);
			Edge itemEdge = this.parsePropertyAsEdge(options,
					itemName + "Edge", defaultEdge);
			int itemHeight = this.parsePropertyAsIntegerOrPercentage(options,
					itemName + "Height", defaultHeight);
			int itemWidth = this.parsePropertyAsIntegerOrPercentage(options,
					itemName + "Width", defaultWidth);
			Dimension itemMaxSize = new Dimension(itemWidth, itemHeight);
			BorderSeparator.SeparatorStyle itemStyle = this
					.parsePropertyAsSeparatorStyle(options, itemName
							+ "Separator", defaultSeparator);
			retval = new BorderSeparator(itemEdge, itemAnchor, itemExpand,
					itemFill, itemStyle, itemColor, itemMaxSize);
		}
		if (this.DEBUG) {
			if (retval != null) {
				System.err.println("Created " + retval);
			} else {
				System.err.println("ERROR!  Item " + itemName
						+ " did not create a border item!");
			}
		}
		return retval;
	}

	/**
	 * Finalizes a measurement based on a maximum size.
	 * 
	 * @param size
	 *            The maximum size for this measurement.
	 * @param measure
	 *            The measurement to finalize.
	 * @return The finalized measurement: If positive, it's returned unchanged.
	 *         If negative, it's treated as a [positive] percentage of the
	 *         maximum size and is returned as such.
	 */
	public int finalizeMeasure(int size, int measure) {
		int retval = measure;
		if (measure < 0) {
			retval = size * measure / -100;
		}
		return retval;
	}

	/**
	 * Finalizes a dimension based on the size of the box.
	 * 
	 * @param size
	 *            The size to which the dimension is relative.
	 * @param dimension
	 *            The dimension to finalize.
	 * @return The finalized dimension.
	 */
	public Dimension finalizeDimension(Dimension size, Dimension dimension) {
		Dimension retval = new Dimension(this.finalizeMeasure(size.width,
				dimension.width), this.finalizeMeasure(size.height,
				dimension.height));
		return retval;
	}

	/**
	 * Finalizes all lengths and sizes in a Border Item given the actual size of
	 * the box.
	 * 
	 * @param size
	 *            The size into which the border items will be placed.
	 * @param item
	 *            The border item to finalize.
	 * @return A new BorderItem with the finalized lengths and sizes.
	 */
	public BorderItem finalizeBorderItem(Dimension size, BorderItem item) {
		if (item instanceof BorderText) {
			BorderText textItem = (BorderText) item;
			item = new BorderText(textItem.position, textItem.anchor,
					textItem.expand, textItem.fill, textItem.text,
					textItem.font, textItem.color, this.finalizeMeasure(
							size.width, textItem.spacing),
					this.finalizeDimension(size, textItem.maxSize));
		} else if (item instanceof BorderSeparator) {
			BorderSeparator separatorItem = (BorderSeparator) item;
			item = new BorderSeparator(separatorItem.position,
					separatorItem.anchor, separatorItem.expand,
					separatorItem.fill, separatorItem.style,
					separatorItem.color, this.finalizeDimension(size,
							separatorItem.maxSize));
		}
		return item;
	}

	/**
	 * Parses the options and set up the parameters used for this photo frame.
	 * 
	 * @param options
	 *            The options from the frame.prop files found.
	 * @param resourceLoader
	 *            The ClassLoader used to load resources needed by the frame.
	 * @throws IllegalArgumentException
	 *             if one of the parameter values cannot be understood. The
	 *             parameter name will be in the exception message.
	 */
	@Override
	public void setupFrameParameters(MetaImage sourceImage, Properties options,
			ClassLoader resourceLoader) throws IllegalArgumentException {
		super.setupFrameParameters(sourceImage, options, resourceLoader);

		this.DEBUG = this.parsePropertyAsBoolean(options, "FlexDEBUG", false);
		if (this.DEBUG) {
			System.err.println("+++ Start of Flex Frame Debugging Output +++");
		}

		this.photoArtist = options.getProperty("Artist",
				System.getProperty("user.name"));
		this.photoTitle = options.getProperty("Title", sourceImage.getName());

		this.defaultAnchor = this.parsePropertyAsAnchor(options,
				"DefaultAnchor", "CENTER");
		this.defaultColor = this.parsePropertyAsColor(options, "DefaultColor",
				"BLACK");
		this.defaultEdge = this.parsePropertyAsEdge(options, "DefaultEdge",
				"NORTH");
		this.defaultExpand = this.parsePropertyAsExpand(options,
				"DefaultExpand", "HORIZONTAL");
		this.defaultFill = this.parsePropertyAsFill(options, "DefaultFill",
				"NONE");
		this.defaultHeight = this.parsePropertyAsIntegerOrPercentage(options,
				"DefaultHeight", "25%");
		this.defaultWidth = this.parsePropertyAsIntegerOrPercentage(options,
				"DefaultWidth", "75%");

		this.defaultType = this.parsePropertyAsItemType(options, "DefaultType",
				"text");

		this.defaultSeparator = this.parsePropertyAsSeparatorStyle(options,
				"DefaultSeparator", "Simple");
		this.defaultFont = this.parsePropertyAsFont(options, "DefaultFont",
				"SansSerif", resourceLoader);
		this.defaultSpacing = this.parsePropertyAsIntegerOrPercentage(options,
				"DefaultSpacing", "0");

		// TODO: No implementation of horizontalGap and verticalGap yet.
		this.horizontalGap = this.parsePropertyAsIntegerOrPercentage(options,
				"HorizontalGap", "0");
		this.verticalGap = this.parsePropertyAsIntegerOrPercentage(options,
				"VerticalGap", "0");

		this.itemBorder = this.parsePropertyAsEdge(options, "ItemBorder",
				"SOUTH");

		this.borderBox = new SpacePacker(); // size will be assigned during
		// addBorderToImage.

		// set up default items
		if (!options.containsKey("ArtistFormat")) {
			options.setProperty("ArtistFormat", "%A");
		}
		if (!options.containsKey("CopyrightFormat")) {
			options.setProperty("CopyrightFormat", "Copyright \u00A9 %Y by %A");
		}
		if (!options.containsKey("DateFormat")) {
			options.setProperty("DateFormat", "%1D %4M %4Y");
		}
		if (!options.containsKey("TitleFormat")) {
			options.setProperty("TitleFormat", "%T");
		}
		if (!options.containsKey("SeparatorType")) {
			options.setProperty("SeparatorType", "Separator");
		}
		if (!options.containsKey("SeparatorHeight")) {
			options.setProperty("SeparatorHeight", "5");
		}

		String ItemOrder[] = options.getProperty("ItemOrder",
				"Title, Separator, Artist, Separator, Date").split(", *");
		this.itemList = new BorderItem[ItemOrder.length];
		for (int i = 0; i < ItemOrder.length; i += 1) {
			BorderItem item = this.parseBorderItem(sourceImage, options,
					ItemOrder[i], resourceLoader);
			if (item == null) {
				throw new IllegalArgumentException(ItemOrder[i] + "Type");
			}
			this.itemList[i] = item;
		}
	}

	/**
	 * Resizes the image and adds the appropriate sized border to it, with photo
	 * name and copyright added to the bottom border.
	 * 
	 * @param sourceImage
	 *            The original image to border.
	 * @return The resized image with border.
	 */
	@Override
	public BufferedImage addBorderToImage(BufferedImage sourceImage) {
		BufferedImage borderedImage = super.addBorderToImage(sourceImage);

		Graphics2D imageGraphics = borderedImage.createGraphics();
		Rectangle flexBounds = new Rectangle();
		switch (itemBorder.value) {
		case SpacePacker.DIRECTION_NORTH:
			flexBounds = new Rectangle(this.RealBorderLeft, 0,
					this.RealImageWidth - this.RealBorderLeft
							- this.RealBorderRight, this.RealBorderTop);
			break;
		case SpacePacker.DIRECTION_SOUTH:
			flexBounds = new Rectangle(this.RealBorderLeft,
					this.RealImageHeight - this.RealBorderBottom,
					this.RealImageWidth - this.RealBorderLeft
							- this.RealBorderRight, this.RealBorderBottom);
			break;
		case SpacePacker.DIRECTION_EAST:
			flexBounds = new Rectangle(this.RealImageWidth
					- this.RealBorderRight, this.RealBorderTop,
					this.RealBorderRight, this.RealImageHeight
							- this.RealBorderTop - this.RealBorderBottom);
			break;
		case SpacePacker.DIRECTION_WEST:
			flexBounds = new Rectangle(0, this.RealBorderTop,
					this.RealBorderLeft, this.RealImageHeight
							- this.RealBorderTop - this.RealBorderBottom);
			break;
		}

		if (this.DEBUG) {
			System.err.println("border bounds = " + flexBounds);
		}
		borderBox.setSize(flexBounds.getSize());

		// recreate each item with the correct size.
		for (int i = 0; i < itemList.length; i += 1) {
			itemList[i] = this.finalizeBorderItem(flexBounds.getSize(),
					itemList[i]);
			itemList[i].setupItem(imageGraphics);
		}
		for (int i = 0; i < itemList.length; i += 1) {
			this.borderBox.add(
					borderBox.new Parcel(itemList[i], itemList[i]
							.getMinimumSize(), itemList[i].anchor, itemList[i]
							.getFill(), itemList[i].getExpand()),
					itemList[i].position);
		}

		this.borderBox.pack();

		Iterator<SpacePacker.Parcel> parcels = this.borderBox.iterator();

		while (parcels.hasNext()) {
			SpacePacker.Parcel parcel = parcels.next();
			Point contentLocation = parcel.getContentLocation();
			contentLocation.translate(flexBounds.x, flexBounds.y);
			Dimension contentSize = parcel.getContentSize();
			if (this.DEBUG) {
				Point parcelLocation = parcel.getParcelLocation();
				parcelLocation.translate(flexBounds.x, flexBounds.y);
				Dimension parcelSize = parcel.getParcelSize();
				imageGraphics.setColor(Color.LIGHT_GRAY);
				imageGraphics.draw(new Rectangle(parcelLocation, parcelSize));
				imageGraphics.setColor(Color.DARK_GRAY);
				imageGraphics.draw(new Rectangle(contentLocation, contentSize));
			}
			((BorderItem) parcel.getContents()).renderItem(contentLocation,
					contentSize);
		}

		return borderedImage;
	}

}
