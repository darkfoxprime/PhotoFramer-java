import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Properties;

import org.springhaven.util.AbstractFrameStyle;
import org.springhaven.util.MetaImage;

/**
 * A simple watermark overlay. Takes the provided watermark image and overlays
 * it through either transparency or pixel arithmetic onto the image.
 * 
 * <h1>Options used by this framing style</h1>
 * 
 * <p>
 * The options that control this framing style are described in the following
 * table; the second table contains the descriptions of the Option Types used
 * below.
 * </p>
 * <table border="1">
 * <tr>
 * <th>Option&nbsp;Name</th>
 * <th>Option&nbsp;Type</th>
 * <th>Description</th>
 * <th>Default&nbsp;Value</th>
 * </tr>
 * <tr>
 * <th align="left">Watermark</th>
 * <td>Image&nbspFile</td>
 * <td>
 * The watermark image to overlay.
 * <td><i>(No default)</i></td>
 * </tr>
 * <tr>
 * <th align="left">WatermarkHeight</th>
 * <td>Absolute&nbsp;or&nbsp;Relative&nbsp;Dimension</td>
 * <td>The height to which to scale the watermark.</td>
 * <td>100%</td>
 * </tr>
 * <tr>
 * <th align="left">WatermarkWidth</th>
 * <td>Absolute&nbsp;or&nbsp;Relative&nbsp;Dimension</td>
 * <td>The width to which to scale the watermark.</td>
 * <td>100%</td>
 * </tr>
 * <tr>
 * <th align="left">Mode</th>
 * <td>Overlay Mode</td>
 * <td>The method used to overlay the image. See description of Overlay Mode
 * below for details.</td>
 * <td>Overlay</td>
 * </tr>
 * <tr>
 * <th align="left">AnchorX</th>
 * <td>Absolute&nbsp;or&nbsp;Relative&nbsp;Dimension</td>
 * <td>The horizontal position within the watermark that anchors to the image.
 * Together with PositionX, this determines the horizontal placement of the
 * watermark.</td>
 * <td>50%</td>
 * </tr>
 * <tr>
 * <th align="left">AnchorY</th>
 * <td>Absolute&nbsp;or&nbsp;Relative&nbsp;Dimension</td>
 * <td>The vertical position within the watermark that anchors to the image.
 * Together with PositionY, this determines the vertical placement of the
 * watermark.</td>
 * <td>50%</td>
 * </tr>
 * <tr>
 * <th align="left">PositionX</th>
 * <td>Absolute&nbsp;or&nbsp;Relative&nbsp;Dimension</td>
 * <td>The horizontal position within the image to which the watermark is
 * anchored. Together with AnchorX, this determines the horizontal placement of
 * the watermark.</td>
 * <td>50%</td>
 * </tr>
 * <tr>
 * <th align="left">PositionY</th>
 * <td>Absolute&nbsp;or&nbsp;Relative&nbsp;Dimension</td>
 * <td>The vertical position within the image to which the watermark is
 * anchored. Together with AnchorY, this determines the vertical placement of
 * the watermark.</td>
 * <td>50%</td>
 * </tr>
 * </table>
 * <p>
 * The Option Types are:
 * </p>
 * <dl>
 * <dt>Image&nbsp;Path</dt>
 * <dd>
 * <p>
 * The path name to an image file.
 * </p>
 * </dd>
 * <dt>Overlay&nbsp;Mode</dt>
 * <dd>
 * <p>
 * The method used to overlay the watermark onto the image. Can be one of:
 * </p>
 * <dl>
 * <dt>Overlay</dt>
 * <dd>The watermark is directly overlayed onto the image with no modifications.
 * If the watermark has transparency, the transparency will allow the image to
 * show through the watermark.</dd>
 * <dt>Add</dt>
 * <dd>The watermark's pixels are "added" to the image's pixels. See the
 * description of pixel arithmetic, below.</dd>
 * <dt>Subtract</dt>
 * <dd>The watermark's pixels are "subtracted" from the image's pixels. See the
 * description of pixel arithmetic, below.</dd>
 * <dt>Multiply</dt>
 * <dd>The watermark's pixels are "multiplied" by the image's pixels. See the
 * description of pixel arithmetic, below.</dd>
 * </dl>
 * <p>
 * Pixel arithmetic is done by the following method:
 * </p>
 * <ol>
 * <li>Each red, green, and blue component of each pixel in both the image and
 * the watermark is converted to a decimal value between 0 and 1, inclusive.</li>
 * <li>The components are added, subtracted, or multiplied, based on the overlay
 * mode.</li>
 * <li>The resulting values are capped between 0.0 and 1.0, then converted back
 * into the same red, green, blue values that the image used originally.</li>
 * <li>Any transparency in the original image is preserved; transparency in the
 * watermark is ignored.</li>
 * </ol>
 * </dd>
 * <dt>Absolute&nbsp;Dimension</dt>
 * <dd>
 * <p>
 * A dimension (either width or height) specified in pixels.
 * </p>
 * <p>
 * Example: <b>150</b>
 * </p>
 * </dd>
 * <dt>Relative&nbsp;Dimension</dt>
 * <dd>
 * <p>
 * A dimension (either width or height) specified as an integral percentage of
 * the picture's final (framed) width or height.
 * </p>
 * <p>
 * Example: <b>80%</b>
 * </p>
 * </dd>
 * <dt>Absolute&nbsp;or&nbsp;Relative&nbsp;Dimension</dt>
 * <dd>
 * <p>
 * Either an Absolute Dimension or a Relative Dimension. If the value ends with
 * a <b>%</b> sign, it's a Relative Dimension; otherwise, it's an Absolute
 * Dimension.
 * </p>
 * </dd>
 * </dl>
 * 
 * 
 * @author Copyright (c) 2007-2011 by Johnson Earls. All rights reserved. See <A
 *         HREF="http://www.springhaven.org/~darkfox/FoxJava/LICENSE">
 *         http://www.springhaven.org/~darkfox/FoxJava/LICENSE </A> for details.
 * 
 */
public class FrameStyleWatermark extends AbstractFrameStyle {

	final String DefaultOverlayMode = "overlay";
	final String DefaultAnchorX = "50%";
	final String DefaultAnchorY = "50%";
	final String DefaultPositionX = "50%";
	final String DefaultPositionY = "50%";
	final String DefaultWatermarkWidth = "100%";
	final String DefaultWatermarkHeight = "100%";
	final String DefaultWatermarkRotation = "0";

	String WatermarkFilename = null;
	MetaImage WatermarkImage = null;
	String OverlayMode = "overlay";
	int WatermarkWidth = -100;
	int WatermarkHeight = -100;
	int WatermarkRotation = 0;
	int AnchorX = -50;
	int AnchorY = -50;
	int PositionX = -50;
	int PositionY = -50;

	/**
	 * Initializes the frame parameters from the options loaded from the
	 * frame.prop files and the defaults defined by this framing style.
	 * 
	 * @param sourceImage
	 *            The image we're processing.
	 * @param options
	 *            The options from the frame.prop files found.
	 * @param resourceLoader
	 *            The ClassLoader used to load resources needed by the frame.
	 * @throws IllegalArgumentException
	 *             if one of the parameter values cannot be understood. The
	 *             parameter name will be in the exception message.
	 */
	public void setupFrameParameters(MetaImage sourceImage, Properties options,
			ClassLoader resourceLoader) throws IllegalArgumentException {
		AnchorX = parsePropertyAsIntegerOrPercentage(options, "AnchorX",
				this.DefaultAnchorX);
		AnchorY = parsePropertyAsIntegerOrPercentage(options, "AnchorY",
				this.DefaultAnchorY);
		PositionX = parsePropertyAsIntegerOrPercentage(options, "PositionX",
				this.DefaultPositionX);
		PositionY = parsePropertyAsIntegerOrPercentage(options, "PositionY",
				this.DefaultPositionY);
		WatermarkWidth = parsePropertyAsIntegerOrPercentage(options,
				"AnchorWatermarkWidth", this.DefaultWatermarkWidth);
		WatermarkHeight = parsePropertyAsIntegerOrPercentage(options,
				"WatermarkHeight", this.DefaultWatermarkHeight);
		WatermarkRotation = parsePropertyAsInteger(options,
				"WatermarkRotation", this.DefaultWatermarkRotation);
		OverlayMode = options
				.getProperty("OverlayMode", this.DefaultOverlayMode).trim()
				.toLowerCase();
		// if (OverlayMode != "overlay" && OverlayMode != "add" && OverlayMode
		// != "subtract" && OverlayMode != "multiply") {
		if (OverlayMode != "overlay") {
			throw new IllegalArgumentException("OverlayMode");
		}
		WatermarkFilename = options.getProperty("Watermark");
		URL WatermarkURL = resourceLoader.getResource(WatermarkFilename);
		File WatermarkFile;
		try {
			WatermarkFile = new File(WatermarkURL.toURI());
		} catch (URISyntaxException e1) {
			throw new IllegalArgumentException("Watermark");
		}
		try {
			WatermarkImage = MetaImage.readImage(WatermarkFile);
		} catch (IOException e2) {
			throw new IllegalArgumentException("Watermark");
		} catch (Exception e3) {
			throw new IllegalArgumentException("Watermark: " + e3);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.springhaven.util.AbstractFrameStyle#processImage(org.springhaven.
	 * util.MetaImage, java.util.Properties, java.lang.ClassLoader)
	 */
	@Override
	public MetaImage processImage(MetaImage sourceImage, Properties options,
			ClassLoader resourceLoader) throws IllegalArgumentException {
		try {
			setupFrameParameters(sourceImage, options, resourceLoader);
		} catch (IllegalArgumentException e) {
			throw new IllegalArgumentException("Invalid value for option "
					+ e.getMessage());
		}
		MetaImage newImage = new MetaImage(sourceImage);
		newImage.setImage(addWatermarkToImage(newImage.getImage()));
		return newImage;
	}

	public BufferedImage addWatermarkToImage(BufferedImage sourceImage) {
		int realPosX = PositionX;
		int realPosY = PositionY;
		int realAnchorX = AnchorX;
		int realAnchorY = AnchorY;

		BufferedImage realWatermarkImage = WatermarkImage.getImage();
		int realWatermarkWidth = realWatermarkImage.getWidth();
		int realWatermarkHeight = realWatermarkImage.getHeight();
		int scaledWatermarkWidth = WatermarkWidth;
		int scaledWatermarkHeight = WatermarkHeight;
		int realSourceWidth = sourceImage.getWidth();
		int realSourceHeight = sourceImage.getHeight();

		if (scaledWatermarkWidth < 0) {
			scaledWatermarkWidth = scaledWatermarkWidth * realWatermarkWidth
					/ -100;
		}
		if (scaledWatermarkHeight < 0) {
			scaledWatermarkHeight = scaledWatermarkHeight * realWatermarkHeight
					/ -100;
		}

		if (realAnchorX < 0) {
			realAnchorX = realAnchorX * scaledWatermarkWidth / -100;
		}
		if (realAnchorY < 0) {
			realAnchorY = realAnchorY * scaledWatermarkHeight / -100;
		}
		if (realPosX < 0) {
			realPosX = realPosX * realSourceWidth / -100;
		}
		if (realPosY < 0) {
			realPosY = realPosY * realSourceHeight / -100;
		}
		realPosX -= realAnchorX;
		realPosY -= realAnchorY;

		BufferedImage newImage = new BufferedImage(realSourceWidth,
				realSourceHeight, BufferedImage.TYPE_INT_RGB);

		Graphics2D newGraphics = newImage.createGraphics();
		newGraphics.setColor(Color.WHITE);
		newGraphics.fillRect(0, 0, realSourceWidth, realSourceHeight);
		newGraphics.drawImage(sourceImage, 0, 0, realSourceWidth,
				realSourceHeight, null);

		newGraphics.translate(realPosX, realPosY);
		newGraphics.scale((float) scaledWatermarkWidth / realWatermarkWidth,
				(float) scaledWatermarkHeight / realWatermarkHeight);
		newGraphics.translate(0.5, 0.5);
		newGraphics.rotate(WatermarkRotation);
		newGraphics.translate(-0.5, -0.5);

		if (OverlayMode == "overlay") {
			newGraphics.drawImage(realWatermarkImage, new AffineTransform(),
					null);
		} else {
			throw new IllegalArgumentException("OverlayMode");
		}

		return newImage;
	}

}
