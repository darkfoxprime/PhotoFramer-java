import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.util.Properties;

import org.springhaven.util.AbstractFrameStyle;
import org.springhaven.util.MetaImage;

/**
 * A simple framer that just puts a colored border around the picture, and
 * ensure that the final result is no larger than a given maximum size.
 * 
 * <h1>Options used by this framing style</h1>
 * 
 * <p>
 * The options that control this framing style are described in the following
 * table; the second table contains the descriptions of the Option Types used
 * below.
 * </p>
 * <table border="1">
 * <tr>
 * <th>Option&nbsp;Name</th>
 * <th>Option&nbsp;Type</th>
 * <th>Description</th>
 * <th>Default&nbsp;Value</th>
 * </tr>
 * <tr>
 * <th align="left">AspectRatio</th>
 * <td>List of aspect ratios</td>
 * <td>
 * A comma-separated list of aspect ratios. The border sizes will be adjusted to
 * force the picture to match one of these aspect ratios; the aspect ratio
 * chosen will be the one that causes the smallest increase in the size of the
 * picture. <b><i>NOT YET IMPLEMENTED!</i></b></td>
 * <td>4x3, 16x9, 16x10</td>
 * </tr>
 * <tr>
 * <th align="left">BorderBottom</th>
 * <td>Absolute&nbsp;or&nbsp;Relative&nbsp;Dimension</td>
 * <td>The height of the top border. If given as a percentage, it is relative to
 * the final height of the photo; otherwise, it is the height of the border in
 * pixels.</td>
 * <td>5%</td>
 * </tr>
 * <tr>
 * <th align="left">BorderColor</th>
 * <td>Color</td>
 * <td>The color of the border to be added to the outside of the picture.</td>
 * <td>White</td>
 * </tr>
 * <tr>
 * <th align="left">BorderLeft</th>
 * <td>Absolute&nbsp;or&nbsp;Relative&nbsp;Dimension</td>
 * <td>The width of the left border. If given as a percentage, it is relative to
 * the final width of the photo; otherwise, it is the width of the border in
 * pixels.</td>
 * <td>2%</td>
 * </tr>
 * <tr>
 * <th align="left">BorderRight</th>
 * <td>Absolute&nbsp;or&nbsp;Relative&nbsp;Dimension</td>
 * <td>The width of the right border. If given as a percentage, it is relative
 * to the final width of the photo; otherwise, it is the width of the border in
 * pixels.</td>
 * <td>2%</td>
 * </tr>
 * <tr>
 * <th align="left">BorderTop</th>
 * <td>Absolute&nbsp;or&nbsp;Relative&nbsp;Dimension</td>
 * <td>The height of the top border. If given as a percentage, it is relative to
 * the final height of the photo; otherwise, it is the height of the border in
 * pixels.</td>
 * <td>2%</td>
 * </tr>
 * <tr>
 * <th align="left">MaxHeight</th>
 * <td>Absolute&nbsp;Dimension</td>
 * <td>The maximum height of the final photo.</td>
 * <td>2048</td>
 * </tr>
 * <tr>
 * <th align="left">MaxWidth</th>
 * <td>Absolute&nbsp;Dimension</td>
 * <td>The maximum width of the final photo.</td>
 * <td>2048</td>
 * </tr>
 * </table>
 * <p>
 * The Option Types are:
 * </p>
 * <dl>
 * <dt>Color</dt>
 * <dd>
 * <p>
 * Either a known color name, or a numeric 24-bit RGB color value specified in
 * hexadecimal in the form #RRGGBB, where RR, GG, BB are the 8-bit hexadecimal
 * values for red, green, and blue, respectively.
 * </p>
 * <p>
 * Known color names are: black, blue, cyan, dark gray, gray, green, light gray,
 * magenta, orange, pink, red, white, yellow.
 * </p>
 * </dd>
 * <dt>Absolute&nbsp;Dimension</dt>
 * <dd>
 * <p>
 * A dimension (either width or height) specified in pixels.
 * </p>
 * <p>
 * Example: <b>150</b>
 * </p>
 * </dd>
 * <dt>Relative&nbsp;Dimension</dt>
 * <dd>
 * <p>
 * A dimension (either width or height) specified as an integral percentage of
 * the picture's final (framed) width or height.
 * </p>
 * <p>
 * Example: <b>80%</b>
 * </p>
 * </dd>
 * <dt>Absolute&nbsp;or&nbsp;Relative&nbsp;Dimension</dt>
 * <dd>
 * <p>
 * Either an Absolute Dimension or a Relative Dimension. If the value ends with
 * a <b>%</b> sign, it's a Relative Dimension; otherwise, it's an Absolute
 * Dimension.
 * </p>
 * </dd>
 * </dl>
 * 
 * 
 * @author Copyright (c) 2012 by Johnson Earls. All rights reserved. See <A
 *         HREF="http://www.springhaven.org/~darkfox/FoxJava/LICENSE">
 *         http://www.springhaven.org/~darkfox/FoxJava/LICENSE </A> for details.
 * 
 */
public class FrameStyleBorder extends AbstractFrameStyle {
	public class AspectRatio {
		public final double width, height;

		public AspectRatio(double width, double height) {
			this.width = width;
			this.height = height;
		}
	}

	final String DefaultBorderLeft = "2%";
	final String DefaultBorderRight = "2%";
	final String DefaultBorderTop = "2%";
	final String DefaultBorderBottom = "5%";
	final String DefaultBorderColor = "White";
	final String DefaultMaxWidth = "2048";
	final String DefaultMaxHeight = "2048";
	final String DefaultAspectRatioList = "";

	int BorderLeft = 0;
	int BorderRight = 0;
	int BorderTop = 0;
	int BorderBottom = 0;
	Color BorderColor = null;
	int MaxWidth = 0;
	int MaxHeight = 0;

	AspectRatio AspectRatioList[] = null;

	int RealBorderLeft = 0;
	int RealBorderRight = 0;
	int RealBorderTop = 0;
	int RealBorderBottom = 0;
	int RealImageWidth = 0;
	int RealImageHeight = 0;
	AspectRatio RealAspectRatio = null;

	/**
	 * Initializes the frame parameters from the options loaded from the
	 * frame.prop files and the defaults defined by this framing style.
	 * 
	 * @param sourceImage
	 *            The image we're processing.
	 * @param options
	 *            The options from the frame.prop files found.
	 * @param resourceLoader
	 *            The ClassLoader used to load resources needed by the frame.
	 * @throws IllegalArgumentException
	 *             if one of the parameter values cannot be understood. The
	 *             parameter name will be in the exception message.
	 */
	public void setupFrameParameters(MetaImage sourceImage, Properties options,
			ClassLoader resourceLoader) throws IllegalArgumentException {
		BorderLeft = parsePropertyAsIntegerOrPercentage(options, "BorderLeft",
				this.DefaultBorderLeft);
		BorderRight = parsePropertyAsIntegerOrPercentage(options,
				"BorderRight", this.DefaultBorderRight);
		BorderTop = parsePropertyAsIntegerOrPercentage(options, "BorderTop",
				this.DefaultBorderTop);
		BorderBottom = parsePropertyAsIntegerOrPercentage(options,
				"BorderBottom", this.DefaultBorderBottom);
		BorderColor = parsePropertyAsColor(options, "BorderColor",
				this.DefaultBorderColor);
		MaxWidth = parsePropertyAsInteger(options, "MaxWidth",
				this.DefaultMaxWidth);
		MaxHeight = parsePropertyAsInteger(options, "MaxHeight",
				this.DefaultMaxHeight);

		{
			String aspectRatioStringValue = options.getProperty("AspectRatio",
					DefaultAspectRatioList).trim();
			String aspectRatioStringList[];
			if (aspectRatioStringValue.length() > 0) {
				aspectRatioStringList = aspectRatioStringValue.split(", *");
			} else {
				aspectRatioStringList = new String[0];
			}
			this.AspectRatioList = new AspectRatio[aspectRatioStringList.length];
			for (int i = 0; i < aspectRatioStringList.length; i += 1) {
				String aspectRatioStrings[];
				if (aspectRatioStringList[i].indexOf(':') > -1) {
					aspectRatioStrings = aspectRatioStringList[i].trim().split(
							":");
				} else if (aspectRatioStringList[i].indexOf('/') > -1) {
					aspectRatioStrings = aspectRatioStringList[i].trim().split(
							"/");
				} else {
					aspectRatioStrings = aspectRatioStringList[i].trim().split(
							"x");
				}
				if (aspectRatioStrings.length != 2) {
					throw new IllegalArgumentException("AspectRatio");
				}
				try {
					double width = Double.parseDouble(aspectRatioStrings[0]);
					double height = Double.parseDouble(aspectRatioStrings[1]);
					this.AspectRatioList[i] = new AspectRatio(width, height);
				} catch (NumberFormatException e) {
					throw new IllegalArgumentException("AspectRatio");
				}
			}
		}
	}

	/**
	 * Adds a simple border to the image.
	 * 
	 * @see AbstractFrameStyle#processImage(org.springhaven.util.MetaImage,
	 *      java.util.Properties, java.lang.ClassLoader)
	 */
	@Override
	public MetaImage processImage(MetaImage sourceImage, Properties options,
			ClassLoader resourceLoader) {
		try {
			setupFrameParameters(sourceImage, options, resourceLoader);
		} catch (IllegalArgumentException e) {
			throw new IllegalArgumentException("Invalid value for option "
					+ e.getMessage());
		}
		MetaImage newImage = new MetaImage(sourceImage);
		newImage.setImage(addBorderToImage(newImage.getImage()));
		return newImage;
	}

	/**
	 * Resizes the image and adds the appropriate sized border to it.
	 * 
	 * @param sourceImage
	 *            The original image to border.
	 * @return The resized image with border.
	 */
	public BufferedImage addBorderToImage(BufferedImage sourceImage) {
		// sourceWidth and sourceHeight are convenience holders for the original
		// image size.
		final int sourceWidth = sourceImage.getWidth();
		final int sourceHeight = sourceImage.getHeight();

		// Each border size is created as two values: preBorder* is for relative
		// border sizes, and is scaled based on the original image size.
		// postBorder* is for absolute border sizes, and is not scaled.
		int preBorderLeft = 0;
		int postBorderLeft = 0;
		int preBorderRight = 0;
		int postBorderRight = 0;
		int preBorderTop = 0;
		int postBorderTop = 0;
		int preBorderBottom = 0;
		int postBorderBottom = 0;

		// maxResizedWidth and maxResizedHeight will hold the maximum image size
		// available after adjusting for absolute border sizes.
		// borderedImageWidth and borderedImageHeight will hold the original
		// image size adjusted for relative border sizes.
		int maxResizedWidth = MaxWidth;
		int maxResizedHeight = MaxHeight;
		int borderedImageWidth = sourceWidth;
		int borderedImageHeight = sourceHeight;

		// figure out how to resize the image to fit the border parameters.

		// first, for each of BorderLeft, BorderRight, BorderTop, Border Bottom,
		// initialize the preBorder* and postBorder* values, depending on if the
		// given border is absolute or relative, and adjust the
		// maxResizedWidth/Height or borderedImageWidth/Height accordingly.
		if (BorderLeft >= 0) {
			postBorderLeft = BorderLeft;
			maxResizedWidth -= postBorderLeft;
		} else {
			preBorderLeft = sourceWidth * BorderLeft / -100;
			borderedImageWidth += preBorderLeft;
		}
		if (BorderRight >= 0) {
			postBorderRight = BorderRight;
			maxResizedWidth -= postBorderRight;
		} else {
			preBorderRight = sourceWidth * BorderRight / -100;
			borderedImageWidth += preBorderRight;
		}
		if (BorderTop >= 0) {
			postBorderTop = BorderTop;
			maxResizedHeight -= postBorderTop;
		} else {
			preBorderTop = sourceHeight * BorderTop / -100;
			borderedImageHeight += preBorderTop;
		}
		if (BorderBottom >= 0) {
			postBorderBottom = BorderBottom;
			maxResizedHeight -= postBorderBottom;
		} else {
			preBorderBottom = sourceHeight * BorderBottom / -100;
			borderedImageHeight += preBorderBottom;
		}

		// we now have: image size (with pre-scaled borders), post-scaled
		// borders,
		// a maximum size, and a set of aspect ratios.

		// expBorder* is the expansion of a border to fill out an aspect ratio.
		// these values will get split among the postBorder* values.
		int expBorderWidth = 0;
		int expBorderHeight = 0;

		// imageScale is the amount the image will be scaled.
		double imageScale = 0.0;

		int resizedBorderedImageWidth = 0;
		int resizedBorderedImageHeight = 0;

		RealAspectRatio = null;

		// look for the aspect ratio that will result in the smallest border
		// expansion.
		for (int arIndex = 0; arIndex < this.AspectRatioList.length; arIndex += 1) {
			AspectRatio ar = this.AspectRatioList[arIndex];
			// figure out the largest rectangle of this aspect ratio that will
			// fit within our max size
			int aspImageWidth = (int) (Math.ceil(MaxHeight * ar.width
					/ ar.height));
			int aspImageHeight = (int) (Math.ceil(MaxWidth * ar.height
					/ ar.width));
			if (aspImageWidth > MaxWidth) {
				aspImageWidth = MaxWidth;
			} else {
				aspImageHeight = MaxHeight;
			}
			// subtract the fixed borders from that rectangle
			aspImageWidth -= postBorderLeft + postBorderRight;
			aspImageHeight -= postBorderTop + postBorderBottom;
			// compute the horizontal and vertical scale values to bring the
			// image to those dimensions
			double imageScaleHoriz = ((double) aspImageWidth)
					/ borderedImageWidth;
			double imageScaleVert = ((double) aspImageHeight)
					/ borderedImageHeight;
			// figure out our actual image scale
			if (imageScaleHoriz >= 1.0) {
				imageScaleHoriz = 1.0;
			}
			if (imageScaleVert >= 1.0) {
				imageScaleVert = 1.0;
			}
			double thisImageScale = (imageScaleHoriz > imageScaleVert) ? imageScaleVert
					: imageScaleHoriz;
			// figure out the scaled image size
			resizedBorderedImageWidth = (int) Math.ceil(borderedImageWidth
					* thisImageScale);
			resizedBorderedImageHeight = (int) Math.ceil(borderedImageHeight
					* thisImageScale);
			// figure out the expansion borders:
			int thisExpBorderWidth = (int) Math
					.ceil((resizedBorderedImageHeight + postBorderTop + postBorderBottom)
							* ar.width / ar.height)
					- (resizedBorderedImageWidth + postBorderLeft + postBorderRight);
			int thisExpBorderHeight = (int) Math
					.ceil((resizedBorderedImageWidth + postBorderLeft + postBorderRight)
							* ar.height / ar.width)
					- (resizedBorderedImageHeight + postBorderTop + postBorderBottom);
			// whichever is negative gets set to 0
			if (thisExpBorderWidth < 0) {
				thisExpBorderWidth = 0;
			}
			if (thisExpBorderHeight < 0) {
				thisExpBorderHeight = 0;
			}
			if ((arIndex == 0)
					|| ((thisExpBorderWidth + thisExpBorderHeight) < (expBorderWidth + expBorderHeight))) {
				imageScale = thisImageScale;
				expBorderWidth = thisExpBorderWidth;
				expBorderHeight = thisExpBorderHeight;
				RealAspectRatio = ar;
			}
		}
		
		// fix imageScale in case we had no aspect ratios (doh!)
		if (imageScale == 0.0) {
			double imageScaleHoriz = ((double) maxResizedWidth)
					/ borderedImageWidth;
			double imageScaleVert = ((double) maxResizedHeight)
					/ borderedImageHeight;
			// figure out our actual image scale
			if (imageScaleHoriz >= 1.0) {
				imageScaleHoriz = 1.0;
			}
			if (imageScaleVert >= 1.0) {
				imageScaleVert = 1.0;
			}
			imageScale = (imageScaleHoriz > imageScaleVert) ? imageScaleVert
					: imageScaleHoriz;
		}

		// now we have our image scale and expansion borders. figure out where
		// to place the actual image.
		RealImageWidth = (int) Math.ceil(borderedImageWidth * imageScale)
				+ postBorderLeft + postBorderRight + expBorderWidth;
		RealImageHeight = (int) Math.ceil(borderedImageHeight * imageScale)
				+ postBorderTop + postBorderBottom + expBorderHeight;
		RealBorderLeft = postBorderLeft
				+ (int) Math.ceil(preBorderLeft * imageScale);
		RealBorderRight = postBorderRight
				+ (int) Math.ceil(preBorderRight * imageScale);
		RealBorderTop = postBorderLeft
				+ (int) Math.ceil(preBorderTop * imageScale);
		RealBorderBottom = postBorderRight
				+ (int) Math.ceil(preBorderBottom * imageScale);

		int expBorderRight = ((this.RealBorderLeft + this.RealBorderRight) == 0) ? (expBorderWidth / 2)
				: (expBorderWidth * this.RealBorderRight / (this.RealBorderLeft + this.RealBorderRight));
		int expBorderLeft = expBorderWidth - expBorderRight;
		this.RealBorderLeft += expBorderLeft;
		this.RealBorderRight += expBorderRight;
		int expBorderBottom = ((this.RealBorderTop + this.RealBorderBottom) == 0) ? (expBorderHeight / 2)
				: (expBorderHeight * this.RealBorderBottom / (this.RealBorderTop + this.RealBorderBottom));
		int expBorderTop = expBorderHeight - expBorderBottom;
		this.RealBorderTop += expBorderTop;
		this.RealBorderBottom += expBorderBottom;
		int resizedWidth = RealImageWidth - RealBorderLeft - RealBorderRight;
		int resizedHeight = RealImageHeight - RealBorderTop - RealBorderBottom;

		// Create the new image, fill it with the border color, and draw the
		// original image in at the correct location and size.
		BufferedImage newImage = new BufferedImage(RealImageWidth,
				RealImageHeight, BufferedImage.TYPE_INT_RGB);
		Graphics2D newGraphics = newImage.createGraphics();
		newGraphics.setColor(Color.white);
		newGraphics.drawImage(sourceImage, RealBorderLeft, RealBorderTop,
				resizedWidth, resizedHeight, null);
		newGraphics.setColor(BorderColor);
		newGraphics.fillRect(0, 0, RealBorderLeft, RealImageHeight);
		newGraphics.fillRect(0, 0, RealImageWidth, RealBorderTop);
		newGraphics.fillRect(0, RealImageHeight - RealBorderBottom,
				RealImageWidth, RealBorderBottom);
		newGraphics.fillRect(RealImageWidth - RealBorderRight, 0,
				RealBorderRight, RealImageHeight);
		newGraphics.dispose();

		// we're done, return the new image.
		return newImage;
	}
}
