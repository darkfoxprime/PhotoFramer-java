/**
 * 
 */
package org.springhaven.util;

import java.util.HashMap;
import java.util.Stack;

/**
 * A specialized HashMap that is initialized from an EXIF byte[] array.
 * 
 * @author Copyright (c) 2009 by Johnson Earls. All rights reserved. See <A
 *         HREF="http://www.springhaven.org/~darkfox/FoxJava/LICENSE">
 *         http://www.springhaven.org/~darkfox/FoxJava/LICENSE </A> for details.
 * 
 */
public class EXIFInfo extends HashMap<Integer, Object[]> {
	static final long serialVersionUID = 2;

	public class Tag {
		public static final int TagTypeUndefined = -3;
		public static final int TagTypeTypedString = -2;
		public static final int TagTypeASCII = -1;
		public static final int TagTypeByte = 1;
		public static final int TagTypeShort = 2;
		public static final int TagTypeLong = 4;
		public static final int TagTypeRational = 8;
		public static final int TagTypeSByte = 101;
		public static final int TagTypeSShort = 102;
		public static final int TagTypeSLong = 104;
		public static final int TagTypeSRational = 108;
		public static final int TagTypePointer = 204;

		public static final int TagNewSubfileType = 0x00FE;
		public static final int TagSubfileType = 0x00FF;
		public static final int TagImageWidth = 0x0100;
		public static final int TagImageHeight = 0x0101;
		public static final int TagBitsPerSample = 0x0102;
		public static final int TagCompression = 0x0103;
		public static final int TagPhotometricInterpretation = 0x0106;
		public static final int TagImageDescription = 0x010E;
		public static final int TagMake = 0x010F;
		public static final int TagModel = 0x0110;
		public static final int TagOrientation = 0x0112;
		public static final int TagSamplesPerPixel = 0x0115;
		public static final int TagXResolution = 0x011A;
		public static final int TagYResolution = 0x011B;
		public static final int TagPlanarConfiguration = 0x011C;
		public static final int TagResolutionUnit = 0x0128;
		public static final int TagSoftware = 0x0131;
		public static final int TagDateTime = 0x0132;
		public static final int TagArtist = 0x013B;
		public static final int TagWhitePoint = 0x013E;
		public static final int TagPrimaryChromaticities = 0x013F;
		public static final int TagYCbCrCoefficients = 0x0211;
		public static final int TagYCbCrSubSampling = 0x0212;
		public static final int TagYCbCrPositioning = 0x0213;
		public static final int TagReferenceBlackWhite = 0x0214;
		public static final int TagCopyright = 0x8298;
		public static final int TagExposureTime = 0x829A;
		public static final int TagFNumber = 0x829D;
		public static final int TagExifIFD = 0x8769;
		public static final int TagExposureProgram = 0x8822;
		public static final int TagGPSIFD = 0x8825;
		public static final int TagISOSpeedRatings = 0x8827;
		public static final int TagDateTimeOriginal = 0x9003;
		public static final int TagDateTimeDigitized = 0x9004;
		public static final int TagShutterSpeedValue = 0x9201;
		public static final int TagApertureValue = 0x9202;
		public static final int TagExposureBiasValue = 0x9204;
		public static final int TagMaxApertureValue = 0x9205;
		public static final int TagSubjectDistance = 0x9206;
		public static final int TagMeteringMode = 0x9207;
		public static final int TagLightSource = 0x9208;
		public static final int TagFlash = 0x9209;
		public static final int TagFocalLength = 0x920A;
		public static final int TagMakerNote = 0x927C;
		public static final int TagUserComment = 0x9286;
		public static final int TagSubsecTime = 0x9290;
		public static final int TagSubSecTimeOriginal = 0x9291;
		public static final int TagSubSecTimeDigitized = 0x9292;
		public static final int TagFlashpixVersion = 0xA000;
		public static final int TagInteroperabilityIFD = 0xA005;
		public static final int TagSensingMethod = 0xA217;
		public static final int TagFileSource = 0xA300;
		public static final int TagSceneType = 0xA301;
		public static final int TagCFAPattern = 0xA302;
		public static final int TagCustomRendered = 0xA401;
		public static final int TagExposureMode = 0xA402;
		public static final int TagWhiteBalance = 0xA403;
		public static final int TagDigitalZoomRatio = 0xA404;
		public static final int TagFocalLengthIn35mmFilm = 0xA405;
		public static final int TagSceneCaptureType = 0xA406;
		public static final int TagGainControl = 0xA407;
		public static final int TagContrast = 0xA408;
		public static final int TagSaturation = 0xA409;
		public static final int TagSharpness = 0xA40A;
		public static final int TagSubjectDistanceRange = 0xA40C;

		public final int tagMarker;
		public final String tagName;
		public final int tagType;

		public Tag(int marker, String name, int type) {
			tagMarker = marker;
			tagName = name;
			tagType = type;
		}

		@Override
		public String toString() {
			return "Tag(" + tagMarker + ", \""
					+ tagName.replaceAll("\"", "\\\"") + "\", " + tagType + ")";
		}
	}

	public static HashMap<Integer, Tag> EXIFTagInfo;
	{
		int tagMarkers[] = { Tag.TagImageWidth, Tag.TagImageHeight,
				Tag.TagBitsPerSample, Tag.TagCompression,
				Tag.TagPhotometricInterpretation, Tag.TagOrientation,
				Tag.TagSamplesPerPixel, Tag.TagPlanarConfiguration,
				Tag.TagYCbCrSubSampling, Tag.TagYCbCrPositioning,
				Tag.TagXResolution, Tag.TagYResolution, Tag.TagResolutionUnit,
				Tag.TagWhitePoint, Tag.TagPrimaryChromaticities,
				Tag.TagYCbCrCoefficients, Tag.TagReferenceBlackWhite,
				Tag.TagDateTime, Tag.TagImageDescription, Tag.TagMake,
				Tag.TagModel, Tag.TagSoftware, Tag.TagArtist, Tag.TagCopyright,
				Tag.TagExifIFD, Tag.TagGPSIFD, Tag.TagInteroperabilityIFD,
				Tag.TagExposureTime, Tag.TagFNumber, Tag.TagISOSpeedRatings,
				Tag.TagShutterSpeedValue, Tag.TagApertureValue,
				Tag.TagExposureBiasValue, Tag.TagMaxApertureValue,
				Tag.TagSubjectDistance, Tag.TagMeteringMode,
				Tag.TagLightSource, Tag.TagFlash, Tag.TagFocalLength,
				Tag.TagExposureMode, Tag.TagWhiteBalance,
				Tag.TagDigitalZoomRatio, Tag.TagFocalLengthIn35mmFilm,
				Tag.TagSceneCaptureType, Tag.TagGainControl, Tag.TagContrast,
				Tag.TagSaturation, Tag.TagSharpness, Tag.TagLightSource,
				Tag.TagUserComment, Tag.TagNewSubfileType, Tag.TagSubfileType,
				Tag.TagDateTimeOriginal, Tag.TagDateTimeDigitized,
				Tag.TagExposureProgram, Tag.TagMakerNote, Tag.TagSubsecTime,
				Tag.TagSubSecTimeOriginal, Tag.TagSubSecTimeDigitized,
				Tag.TagFlashpixVersion, Tag.TagSensingMethod,
				Tag.TagFileSource, Tag.TagSceneType, Tag.TagCFAPattern,
				Tag.TagCustomRendered, Tag.TagSubjectDistanceRange };
		String tagNames[] = { "ImageWidth", "ImageHeight", "BitsPerSample",
				"Compression", "PhotometricInterpretation", "Orientation",
				"SamplesPerPixel", "PlanarConfiguration", "YCbCrSubSampling",
				"YCbCrPositioning", "XResolution", "YResolution",
				"ResolutionUnit", "WhitePoint", "PrimaryChromaticities",
				"YCbCrCoefficients", "ReferenceBlackWhite", "DateTime",
				"ImageDescription", "Make", "Model", "Software", "Artist",
				"Copyright", "ExifIFD", "GPSIFD", "InteroperabilityIFD",
				"ExposureTime", "FNumber", "ISOSpeedRatings",
				"ShutterSpeedValue", "ApertureValue", "ExposureBiasValue",
				"MaxApertureValue", "SubjectDistance", "MeteringMode",
				"LightSource", "Flash", "FocalLength", "ExposureMode",
				"WhiteBalance", "DigitalZoomRatio", "FocalLengthIn35mmFilm",
				"SceneCaptureType", "GainControl", "Contrast", "Saturation",
				"Sharpness", "LightSource", "UserComment", "NewSubfileType",
				"SubfileType", "DateTimeOriginal", "DateTimeDigitized",
				"ExposureProgram", "MakerNote", "SubsecTime",
				"SubSecTimeOriginal", "SubSecTimeDigitized", "FlashpixVersion",
				"SensingMethod", "FileSource", "SceneType", "CFAPattern",
				"CustomRendered", "SubjectDistanceRange" };
		int tagTypes[] = { Tag.TagTypeLong, Tag.TagTypeLong, Tag.TagTypeShort,
				Tag.TagTypeShort, Tag.TagTypeShort, Tag.TagTypeShort,
				Tag.TagTypeShort, Tag.TagTypeShort, Tag.TagTypeShort,
				Tag.TagTypeShort, Tag.TagTypeRational, Tag.TagTypeRational,
				Tag.TagTypeShort, Tag.TagTypeRational, Tag.TagTypeRational,
				Tag.TagTypeRational, Tag.TagTypeRational, Tag.TagTypeASCII,
				Tag.TagTypeASCII, Tag.TagTypeASCII, Tag.TagTypeASCII,
				Tag.TagTypeASCII, Tag.TagTypeASCII, Tag.TagTypeASCII,
				Tag.TagTypePointer, Tag.TagTypePointer, Tag.TagTypePointer,
				Tag.TagTypeRational, Tag.TagTypeRational, Tag.TagTypeShort,
				Tag.TagTypeSRational, Tag.TagTypeRational,
				Tag.TagTypeSRational, Tag.TagTypeRational, Tag.TagTypeRational,
				Tag.TagTypeShort, Tag.TagTypeShort, Tag.TagTypeShort,
				Tag.TagTypeRational, Tag.TagTypeShort, Tag.TagTypeShort,
				Tag.TagTypeRational, Tag.TagTypeShort, Tag.TagTypeShort,
				Tag.TagTypeShort, Tag.TagTypeShort, Tag.TagTypeShort,
				Tag.TagTypeShort, Tag.TagTypeShort, Tag.TagTypeTypedString,
				Tag.TagTypeLong, Tag.TagTypeShort, Tag.TagTypeASCII,
				Tag.TagTypeASCII, Tag.TagTypeShort, Tag.TagTypeUndefined,
				Tag.TagTypeASCII, Tag.TagTypeASCII, Tag.TagTypeASCII,
				Tag.TagTypeUndefined, Tag.TagTypeShort, Tag.TagTypeUndefined,
				Tag.TagTypeUndefined, Tag.TagTypeUndefined, Tag.TagTypeShort,
				Tag.TagTypeShort };

		EXIFTagInfo = new HashMap<Integer, Tag>();
		for (int i = 0; i < tagMarkers.length; i += 1) {
			EXIFTagInfo.put(new Integer(tagMarkers[i]), new Tag(tagMarkers[i],
					tagNames[i], tagTypes[i]));
		}
	}

	public static Tag getTagInfo(int tagN) {
		Integer tagI = new Integer(tagN);
		if (EXIFTagInfo.containsKey(tagI)) {
			return EXIFTagInfo.get(tagI);
		}
		return null;
	}

	public class Rational {
		public final long numerator, denominator;

		public Rational(long num, long denom) {
			numerator = num;
			denominator = denom;
		}

		public String toDecimalString(int decimalDigits) {
			String ret = "";
			long wholepart = this.numerator / this.denominator;
			long denom = this.denominator;
			long num = this.numerator;
			if (denom < 0) {
				denom = -denom;
			}
			if (num < 0) {
				num = -num;
			}
			num = num % denom;
			ret = "" + wholepart;
			if (decimalDigits > 0) {
				ret += ".";
				while (decimalDigits > 0) {
					num = num * 10;
					ret += (num / denom);
					num = num % denom;
				}
			}
			return ret;
		}

		public String toFractionalString(boolean rationalized) {
			String ret;
			if (rationalized) {
				long wholepart = this.numerator / this.denominator;
				long denom = this.denominator;
				long num = this.numerator;
				if (denom < 0) {
					denom = -denom;
				}
				if (num < 0) {
					num = -num;
				}
				num = num % denom;
				long i = 2;
				while (i < (denom / 2)) {
					if (((denom % i) == 0) && ((num % i) == 0)) {
						num /= i;
						denom /= i;
					} else {
						i += 1;
					}
				}
				if (wholepart > 0) {
					ret = "" + wholepart + " " + num + "/" + denom;
				} else {
					ret = "" + num + "/" + denom;
				}
			} else {
				ret = "" + this.numerator + "/" + this.denominator;
			}
			return ret;
		}

		public String toString() {
			return "Rational[" + this.numerator + "," + this.denominator + "]";
		}
	}

	private abstract class Convert {
		abstract public int toUnsignedShort(byte[] data, int offset);

		abstract public long toUnsignedLong(byte[] data, int offset);

		abstract public int toSignedShort(byte[] data, int offset);

		abstract public long toSignedLong(byte[] data, int offset);
	}

	private class ConvertMM extends Convert {
		@Override
		public int toUnsignedShort(byte[] data, int offset) {
			int val = data[offset] * 0x100 + data[offset + 1];
			if (data[offset] < 0) {
				val += 65536;
			}
			if (data[offset + 1] < 0) {
				val += 256;
			}
			return val;
		}

		@Override
		public long toUnsignedLong(byte[] data, int offset) {
			long val = data[offset] * 0x1000000 + data[offset + 1] * 0x10000
					+ data[offset + 2] * 0x100 + data[offset + 3];
			if (data[offset] < 0) {
				val += 0x100000000L;
			}
			if (data[offset + 1] < 0) {
				val += 0x1000000L;
			}
			if (data[offset + 2] < 0) {
				val += 0x10000L;
			}
			if (data[offset + 3] < 0) {
				val += 0x100L;
			}
			return val;
		}

		@Override
		public int toSignedShort(byte[] data, int offset) {
			int val = data[offset] * 0x100 + data[offset + 1];
			if (data[offset + 1] < 0) {
				val += 0x100;
			}
			return (short) val;
		}

		@Override
		public long toSignedLong(byte[] data, int offset) {
			long val = data[offset] * 0x1000000 + data[offset + 1] * 0x10000
					+ data[offset + 2] * 0x100 + data[offset + 3];
			if (data[offset + 3] < 0) {
				val += 0x100L;
			}
			if (data[offset + 2] < 0) {
				val += 0x10000L;
			}
			if (data[offset + 1] < 0) {
				val += 0x1000000L;
			}
			return (int) val;
		}
	}

	private class ConvertII extends Convert {
		@Override
		public int toUnsignedShort(byte[] data, int offset) {
			int val = data[offset + 1] * 0x100 + data[offset];
			if (data[offset + 1] < 0) {
				val += 65536;
			}
			if (data[offset] < 0) {
				val += 256;
			}
			return val;
		}

		@Override
		public long toUnsignedLong(byte[] data, int offset) {
			long val = data[offset + 3] * 0x1000000 + data[offset + 2]
					* 0x10000 + data[offset + 1] * 0x100 + data[offset];
			if (data[offset + 3] < 0) {
				val += 0x100000000L;
			}
			if (data[offset + 2] < 0) {
				val += 0x1000000L;
			}
			if (data[offset + 1] < 0) {
				val += 0x10000L;
			}
			if (data[offset] < 0) {
				val += 0x100L;
			}
			return val;
		}

		@Override
		public int toSignedShort(byte[] data, int offset) {
			int val = data[offset + 1] * 0x100 + data[offset];
			if (data[offset] < 0) {
				val += 0x100;
			}
			return (short) val;
		}

		@Override
		public long toSignedLong(byte[] data, int offset) {
			long val = data[offset + 3] * 0x1000000 + data[offset + 2]
					* 0x10000 + data[offset + 1] * 0x100 + data[offset];
			if (data[offset] < 0) {
				val += 0x100L;
			}
			if (data[offset + 1] < 0) {
				val += 0x10000L;
			}
			if (data[offset + 2] < 0) {
				val += 0x1000000L;
			}
			return (int) val;
		}
	}

	/**
	 * Creates an empty EXIFInfo HashMap.
	 */
	public EXIFInfo() {
		super();
	}

	/**
	 * Creates the EXIFInfo HashMap from the passed-in EXIF data.
	 * 
	 * @param exifData
	 *            The byte[] array containing the data from the EXIF APP1 block.
	 * @throws IllegalArgumentException
	 *             if the byte array is not a valid EXIF data block.
	 */
	public EXIFInfo(byte[] exifData) throws IllegalArgumentException {
		this();
		loadEXIFInfo(exifData);
	}

	/**
	 * Reads the EXIF data from the given byte array and adds the contents to
	 * the EXIFInfo HashMap.
	 * 
	 * @param exifData
	 *            The byte array of EXIF information.
	 */
	public void loadEXIFInfo(byte[] exifData) {
		if (exifData[0] != 0x45 || exifData[1] != 0x78 || exifData[2] != 0x69
				|| exifData[3] != 0x66) {
			throw new IllegalArgumentException("Not an EXIF data block!");
		}
		{
			byte realData[] = new byte[exifData.length - 6];
			System.arraycopy(exifData, 6, realData, 0, realData.length);
			exifData = realData;
		}
		Convert converter = new ConvertII();
		if (exifData[0] == 0x4D) {
			converter = new ConvertMM();
		}
		Stack<Integer> offsetStack = new Stack<Integer>();
		int offset;
		offsetStack.push(new Integer(0));
		offsetStack.push(new Integer((int) converter
				.toUnsignedLong(exifData, 4)));
		while ((offset = offsetStack.pop().intValue()) > 0) {
			int ptr = offset;
			int ntags = converter.toUnsignedShort(exifData, ptr);
			ptr += 2;
			for (int i = 0; i < ntags; i += 1) {
				int tagn = converter.toUnsignedShort(exifData, ptr);
				/* int type = converter.toUnsignedShort(exifData, ptr + 2); */
				int count = (int) converter.toUnsignedLong(exifData, ptr + 4);
				int valOffset = ptr + 8;
				ptr += 12;
				Tag tag = getTagInfo(tagn);
				if (tag != null) {
					Object[] value = null;
					if (tag.tagType == Tag.TagTypePointer) {
						valOffset = (int) converter.toUnsignedLong(exifData,
								valOffset);
						offsetStack.push(new Integer(valOffset));
					} else if (tag.tagType == Tag.TagTypeASCII) {
						if (count > 4) {
							valOffset = (int) converter.toUnsignedLong(
									exifData, valOffset);
						}
						String valStr = "";
						while (exifData[valOffset] != 0) {
							valStr += (char) exifData[valOffset];
							valOffset += 1;
						}
						String[] valArray = new String[1];
						valArray[0] = valStr;
						value = valArray;
					} else if (tag.tagType == Tag.TagTypeTypedString) {
						valOffset = (int) converter.toUnsignedLong(exifData,
								valOffset);
						if ((exifData[valOffset] == 0x41)
								&& (exifData[valOffset + 1] == 0x53)
								&& (exifData[valOffset + 2] == 0x43)
								&& (exifData[valOffset + 3] == 0x49)
								&& (exifData[valOffset + 4] == 0x49)
								&& (exifData[valOffset + 5] == 0)
								&& (exifData[valOffset + 6] == 0)
								&& (exifData[valOffset + 7] == 0)) {
							String valStr = "";
							valOffset += 8;
							while (exifData[valOffset] != 0) {
								valStr += (char) exifData[valOffset];
								valOffset += 1;
							}
							String[] valArray = new String[1];
							valArray[0] = valStr;
							value = valArray;
						}
					} else if (tag.tagType == Tag.TagTypeUndefined) {
						if (count > 4) {
							valOffset = (int) converter.toUnsignedLong(
									exifData, valOffset);
						}
						byte valData[] = new byte[count];
						System.arraycopy(exifData, valOffset, valData, 0, count);
						byte[][] valArray = new byte[1][];
						valArray[0] = valData;
						value = valArray;
					} else {
						if ((tag.tagType % 100 * count) > 4) {
							valOffset = (int) converter.toUnsignedLong(
									exifData, valOffset);
						}
						Object valArray[] = new Object[count];
						value = valArray;
						for (int k = 0; k < count; k += 1) {
							switch (tag.tagType) {
							case Tag.TagTypeRational:
								valArray[k] = new Rational(
										converter.toUnsignedLong(exifData,
												valOffset),
										converter.toUnsignedLong(exifData,
												valOffset + 4));
								valOffset += 8;
								break;
							case Tag.TagTypeSRational:
								valArray[k] = new Rational(
										converter.toSignedLong(exifData,
												valOffset),
										converter.toSignedLong(exifData,
												valOffset + 4));
								valOffset += 8;
								break;
							case Tag.TagTypeLong:
								valArray[k] = new Long(
										converter.toUnsignedLong(exifData,
												valOffset));
								valOffset += 4;
								break;
							case Tag.TagTypeSLong:
								valArray[k] = new Long(converter.toSignedLong(
										exifData, valOffset));
								valOffset += 4;
								break;
							case Tag.TagTypeShort:
								valArray[k] = new Integer(
										converter.toUnsignedShort(exifData,
												valOffset));
								valOffset += 2;
								break;
							case Tag.TagTypeSShort:
								valArray[k] = new Integer(
										converter.toSignedShort(exifData,
												valOffset));
								valOffset += 2;
								break;
							case Tag.TagTypeByte:
								valArray[k] = new Short(
										(short) ((exifData[valOffset] + 256) % 256));
								valOffset += 1;
								break;
							case Tag.TagTypeSByte:
								valArray[k] = new Short(exifData[valOffset]);
								valOffset += 1;
								break;
							}
						}
					}
					if (value != null) {
						this.put(new Integer(tagn), value);
					}
				}
			}
		}
	}
}
