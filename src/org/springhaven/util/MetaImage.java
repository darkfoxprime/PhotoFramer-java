package org.springhaven.util;

import java.awt.Dimension;
import java.awt.image.BufferedImage;
import java.awt.image.WritableRaster;
import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.metadata.IIOMetadata;
import javax.imageio.metadata.IIOMetadataNode;
import javax.imageio.plugins.jpeg.JPEGImageWriteParam;
import javax.imageio.stream.ImageInputStream;
import javax.imageio.stream.ImageOutputStream;

import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

/**
 * @author Copyright (c) 2010 by Johnson Earls. All rights reserved. See <A
 *         HREF="http://www.springhaven.org/~darkfox/FoxJava/LICENSE">
 *         http://www.springhaven.org/~darkfox/FoxJava/LICENSE </A> for details.
 * 
 */
public class MetaImage implements Cloneable {
	/**
	 * The name of the image. Must not be null.
	 */
	private String name;
	/**
	 * The image itself.
	 */
	private BufferedImage image;
	/**
	 * The format of the original image.
	 */
	private String format;
	/**
	 * The ImageIO metadata associated with the image.
	 */
	private IIOMetadata metadata;
	/**
	 * The size of the image.
	 */
	private Dimension size;
	/**
	 * The EXIF info, if any, associated with the image.
	 */
	private EXIFInfo exifInfo;
	/**
	 * The date of the image.
	 */
	private Date date;

	protected MetaImage() {
		this.image = null;
		this.name = null;
		this.format = null;
		this.metadata = null;
		this.size = null;
		this.exifInfo = null;
		this.date = null;
	}

	public MetaImage(int imageWidth, int imageHeight, String name, String format) {
		this.setImage(new BufferedImage(imageWidth, imageHeight,
				BufferedImage.TYPE_INT_ARGB));
		this.setName(name);
		this.setFormat(format);
		this.setMetadata(null);
		this.setExifInfo(null);
		this.setDate(new Date());
	}

	public MetaImage(MetaImage source) {
		WritableRaster newRaster = source.image.getRaster()
				.createCompatibleWritableRaster();
		newRaster.setDataElements(0, 0, source.image.getRaster());
		BufferedImage newImage = source.image.getSubimage(0, 0,
				source.image.getWidth(), source.image.getHeight());
		newImage.setData(newRaster);
		this.setImage(newImage);
		this.setName(source.name);
		this.setFormat(source.format);
		this.setMetadata(source.metadata);
		this.setExifInfo((EXIFInfo) source.exifInfo.clone());
		this.setDate(source.date);
	}

	/**
	 * Reads an image file and returns the image with all metadata.
	 * 
	 * @param sourceImage
	 *            The File pointing to the image to be framed.
	 * @throws IOException
	 *             if there's an error reading the image.
	 */
	public static MetaImage readImage(File sourceImage) throws IOException {
		ImageInputStream iis = ImageIO.createImageInputStream(sourceImage);
		ImageReader sourceImageReader = ImageIO.getImageReaders(iis).next();
		sourceImageReader.setInput(iis, true, false);
		// IIOMetadata sourceStreamMetadata =
		// sourceImageReader.getStreamMetadata();
		IIOMetadata sourceImageMetadata = sourceImageReader.getImageMetadata(0);
		Dimension sourceImageSize = new Dimension(
				sourceImageReader.getWidth(0), sourceImageReader.getHeight(0));
		BufferedImage sourceImageData = sourceImageReader.read(0);
		String sourceImageName = sourceImage.getName();
		int idx = sourceImageName.lastIndexOf('.');
		sourceImageName = sourceImageName.substring(0, idx).replace('_', ' ');
		EXIFInfo sourceImageEXIFInfo = null;

		// fixJPEGImageMetadata(sourceImageMetadata);
		// dumpImageMetadata(sourceImageMetadata);

		String[] sourceImageMetadataFormats = sourceImageMetadata
				.getMetadataFormatNames();
		boolean jpeg_format_found = false;
		for (int i = sourceImageMetadataFormats.length; --i >= 0;) {
			if (sourceImageMetadataFormats[i] == "javax_imageio_jpeg_image_1.0") {
				jpeg_format_found = true;
				break;
			}
		}

		sourceImageEXIFInfo = new EXIFInfo();

		if (jpeg_format_found) {
			IIOMetadataNode metadataNode = (IIOMetadataNode) sourceImageMetadata
					.getAsTree("javax_imageio_jpeg_image_1.0");
			IIOMetadataNode childNode;

			childNode = (IIOMetadataNode) metadataNode.getFirstChild();
			while (childNode != null) {
				if (childNode.getNodeName().equals("markerSequence")) {
					break;
				}
				childNode = (IIOMetadataNode) childNode.getNextSibling();
			}
			if (childNode != null) {
				childNode = (IIOMetadataNode) childNode.getFirstChild();
			}
			while (childNode != null) {
				if (childNode.getNodeName().equals("unknown")) {
					NamedNodeMap attrMap = childNode.getAttributes();
					if (attrMap != null) {
						Node attrNode = attrMap.getNamedItem("MarkerTag");
						if (attrNode != null) {
							if (attrNode.getNodeValue().equals("225")) {
								byte[] exifInfo = (byte[]) childNode
										.getUserObject();
								if (exifInfo[0] == 0x45 && exifInfo[1] == 0x78
										&& exifInfo[2] == 0x69
										&& exifInfo[3] == 0x66) {
									sourceImageEXIFInfo = new EXIFInfo(exifInfo);
								}
								break;
							}
						}
					}
				}
				childNode = (IIOMetadataNode) childNode.getNextSibling();
			}
		}

		Date sourceImageDate = new Date(sourceImage.lastModified());
		try {
			SimpleDateFormat photoDateFormatter = new SimpleDateFormat(
					"yyyy:MM:dd HH:mm:ss");
			if (sourceImageEXIFInfo
					.containsKey(EXIFInfo.Tag.TagDateTimeOriginal)) {
				sourceImageDate = photoDateFormatter
						.parse((String) sourceImageEXIFInfo
								.get(EXIFInfo.Tag.TagDateTimeOriginal)[0]);
			} else if (sourceImageEXIFInfo
					.containsKey(EXIFInfo.Tag.TagDateTimeDigitized)) {
				sourceImageDate = photoDateFormatter
						.parse((String) sourceImageEXIFInfo
								.get(EXIFInfo.Tag.TagDateTimeDigitized)[0]);
			} else if (sourceImageEXIFInfo
					.containsKey(EXIFInfo.Tag.TagDateTime)) {
				sourceImageDate = photoDateFormatter
						.parse((String) sourceImageEXIFInfo
								.get(EXIFInfo.Tag.TagDateTime)[0]);
			}
		} catch (ParseException e) {
		}

		MetaImage newImage = new MetaImage();
		newImage.setImage(sourceImageData);
		newImage.setSize(sourceImageSize);
		newImage.setName(sourceImageName);
		newImage.setFormat(sourceImageReader.getFormatName());
		newImage.setDate(sourceImageDate);
		newImage.setExifInfo(sourceImageEXIFInfo);
		newImage.setMetadata(sourceImageMetadata);
		return newImage;
	}

	/**
	 * Saves the framed image.
	 * 
	 * @param framedImage
	 *            The File to which to save the framed image.
	 * @throws IOException
	 *             if there's an error saving the image.
	 */
	public void saveImage(File framedImage) throws IOException {
		IIOImage savedImage = new IIOImage(this.image, null, this.metadata);
		ImageWriteParam writerParams = new ImageWriteParam(Locale.getDefault());
		if (this.format.equalsIgnoreCase("jpeg")
				|| this.format.equalsIgnoreCase("jpg")) {
			writerParams = (new JPEGImageWriteParam(Locale.getDefault()));
			((JPEGImageWriteParam) writerParams).setOptimizeHuffmanTables(true);
		}
		ImageOutputStream ios = ImageIO.createImageOutputStream(framedImage);
		ImageWriter framedImageWriter = ImageIO.getImageWritersByFormatName(
				this.format).next();
		framedImageWriter.setOutput(ios);
		framedImageWriter.write(this.metadata, savedImage, writerParams);
		framedImage.setLastModified(this.date.getTime());
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param image
	 *            the image to set
	 */
	public void setImage(BufferedImage image) {
		this.image = image;
		this.setSize(new Dimension(image.getWidth(), image.getHeight()));
	}

	/**
	 * @return the image
	 */
	public BufferedImage getImage() {
		return image;
	}

	/**
	 * @param format
	 *            the format to set
	 */
	public void setFormat(String format) {
		this.format = format;
	}

	/**
	 * @return the format
	 */
	public String getFormat() {
		return format;
	}

	/**
	 * @param metadata
	 *            the metadata to set
	 */
	public void setMetadata(IIOMetadata metadata) {
		this.metadata = metadata;
	}

	/**
	 * @return the metadata
	 */
	public IIOMetadata getMetadata() {
		return metadata;
	}

	/**
	 * @param size
	 *            the size to set
	 */
	public void setSize(Dimension size) {
		this.size = size;
	}

	/**
	 * @return the size
	 */
	public Dimension getSize() {
		return size;
	}

	/**
	 * @param exifInfo
	 *            the exifInfo to set
	 */
	public void setExifInfo(EXIFInfo exifInfo) {
		this.exifInfo = exifInfo;
	}

	/**
	 * @return the exifInfo
	 */
	public EXIFInfo getExifInfo() {
		return exifInfo;
	}

	/**
	 * @param date
	 *            the date to set
	 */
	public void setDate(Date date) {
		this.date = date;
	}

	/**
	 * @return the date
	 */
	public Date getDate() {
		return date;
	}

	@Override
	public MetaImage clone() {
		return new MetaImage(this);
	}
}
