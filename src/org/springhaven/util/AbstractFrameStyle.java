package org.springhaven.util;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontFormatException;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InvalidClassException;
import java.util.HashMap;
import java.util.Properties;

import javax.imageio.metadata.IIOInvalidTreeException;
import javax.imageio.metadata.IIOMetadata;
import javax.imageio.metadata.IIOMetadataNode;

import org.springhaven.app.PhotoFramer.FrameStyle;

/**
 * @author Copyright (c) 2009 by Johnson Earls. All rights reserved. See <A
 *         HREF="http://www.springhaven.org/~darkfox/FoxJava/LICENSE">
 *         http://www.springhaven.org/~darkfox/FoxJava/LICENSE </A> for details.
 * 
 */
public abstract class AbstractFrameStyle implements FrameStyle {

	/**
	 * Used for parsePropertyAsColor convenience routine.
	 */
	static HashMap<String, Color> ColorNames = null;
	{
		String colorNames[] = { "white", "black", "red", "green", "blue",
				"magenta", "cyan", "yellow", "aquamarine", "baker's chocolate",
				"blue violet", "brass", "bright gold", "brown", "bronze",
				"bronze ii", "cadet blue", "cool copper", "copper", "coral",
				"corn flower", "dark brown", "dark green", "dark green copper",
				"dark olive green", "dark orchid", "dark purple",
				"dark slate blue", "dark slate grey", "dark tan",
				"dark turquoise", "dark wood", "dim grey", "dusty rose",
				"feldspar", "firebrick", "forest green", "gold", "goldenrod",
				"grey", "green copper", "green yellow", "hunter green",
				"indian red", "khaki", "light blue", "light grey",
				"light steel blue", "light wood", "lime green",
				"mandarian orange", "maroon", "medium aquamarine",
				"medium blue", "medium forest green", "medium goldenrod",
				"medium orchid", "medium sea green", "medium slate blue",
				"medium spring green", "medium turquoise", "medium violet red",
				"medium wood", "midnight blue", "navy blue", "neon blue",
				"neon pink", "new midnight blue", "new tan", "old gold",
				"orange", "orange red", "orchid", "pale green", "pink", "plum",
				"quartz", "rich blue", "salmon", "scarlet", "sea green",
				"semi-sweet chocolate", "sienna", "silver", "sky blue",
				"slate blue", "spicy pink", "spring green", "steel blue",
				"summer sky", "tan", "thistle", "turquoise", "very dark brown",
				"very light grey", "violet", "violet red", "wheat",
				"yellow green", };
		String colorValues[] = { "ffffff", "000000", "ff0000", "00ff00",
				"0000ff", "ff00ff", "00ffff", "ffff00", "70db93", "5c3317",
				"9f5f9f", "b5a642", "d9d919", "a62a2a", "8c7853", "a67d3d",
				"5f9f9f", "d98719", "b87333", "ff7f00", "42426f", "5c4033",
				"2f4f2f", "4a766e", "4f4f2f", "9932cd", "871f78", "6b238e",
				"2f4f4f", "97694f", "7093db", "855e42", "545454", "856363",
				"d19275", "8e2323", "238e23", "cd7f32", "dbdb70", "c0c0c0",
				"527f76", "93db70", "215e21", "4e2f2f", "9f9f5f", "c0d9d9",
				"a8a8a8", "8f8fbd", "e9c2a6", "32cd32", "e47833", "8e236b",
				"32cd99", "3232cd", "6b8e23", "eaeaae", "9370db", "426f42",
				"7f00ff", "7fff00", "70dbdb", "db7093", "a68064", "2f2f4f",
				"23238e", "4d4dff", "ff6ec7", "00009c", "ebc79e", "cfb53b",
				"ff7f00", "ff2400", "db70db", "8fbc8f", "bc8f8f", "eaadea",
				"d9d9f3", "5959ab", "6f4242", "8c1717", "238e68", "6b4226",
				"8e6b23", "e6e8fa", "3299cc", "007fff", "ff1cae", "00ff7f",
				"236b8e", "38b0de", "db9370", "d8bfd8", "adeaea", "5c4033",
				"cdcdcd", "4f2f4f", "cc3299", "d8d8bf", "99cc32", };
		ColorNames = new HashMap<String, Color>();
		for (int i = 0; i < colorNames.length; i += 1) {
			ColorNames.put(colorNames[i],
					new Color(Integer.parseInt(colorValues[i], 16)));
		}
	}

	/**
	 * Convenience function which parses a property as a color value. The
	 * property must either be a known color name (see ColorNames above) or a
	 * string of the form '#RRGGBB' where RR,GG,BB are hex digits. Throws
	 * IllegalArgumentException with the name of the property if the value is
	 * not parseable as a color.
	 * 
	 * @param options
	 *            The properties to use to retrieve the property value.
	 * @param propertyName
	 *            The name of the property to parse.
	 * @param defaultValue
	 *            The default value to use if the property does not exist.
	 * @return The Color value parsed from the property.
	 * @throws IllegalArgumentException
	 *             if the property value is not parseable as a color.
	 */
	public Color parsePropertyAsColor(Properties options, String propertyName,
			String defaultValue) throws IllegalArgumentException {
		String colorName = options.getProperty(propertyName, defaultValue);
		Color retVal = null;
		if (ColorNames.containsKey(colorName.toLowerCase())) {
			retVal = ColorNames.get(colorName.toLowerCase());
		} else if (colorName.startsWith("#")) {
			try {
				retVal = Color.decode("0x" + colorName.substring(1));
			} catch (NumberFormatException e) {
				throw new IllegalArgumentException(propertyName);
			}
		} else {
			throw new IllegalArgumentException(propertyName);
		}
		return retVal;
	}

	/**
	 * Convenience function which parses a property as a color value. The
	 * property must either be a known color name (see ColorNames above) or a
	 * string of the form '#RRGGBB' where RR,GG,BB are hex digits. Throws
	 * IllegalArgumentException with the name of the property if the value is
	 * not parseable as a color.
	 * 
	 * @param options
	 *            The properties to use to retrieve the property value.
	 * @param propertyName
	 *            The name of the property to parse.
	 * @param defaultValue
	 *            The default value to use if the property does not exist.
	 * @return The Color value parsed from the property.
	 * @throws IllegalArgumentException
	 *             if the property value is not parseable as a color.
	 */
	public Color parsePropertyAsColor(Properties options, String propertyName,
			Color defaultValue) throws IllegalArgumentException {
		if (options.containsKey(propertyName)) {
			return this.parsePropertyAsColor(options, propertyName, "");
		} else {
			return defaultValue;
		}
	}

	/**
	 * Convenience function which parses a property as a font value. The
	 * property must either be known to the {@link java.awt.Font} class, or be
	 * the name of a font file loadable by resourceLoader. The font will have to
	 * be resized before being used.
	 * 
	 * @param options
	 *            The properties to use to retrieve the property value.
	 * @param propertyName
	 *            The name of the property to parse.
	 * @param defaultValue
	 *            The default value to use if the property does not exist.
	 * @param resourceLoader
	 *            The ClassLoader needed to locate the font file.
	 * @return The Font value parsed from the property.
	 * @throws IllegalArgumentException
	 *             if the property value is not parseable as a font.
	 */
	public Font parsePropertyAsFont(Properties options, String propertyName,
			String defaultValue, ClassLoader resourceLoader)
			throws IllegalArgumentException {
		String fontName = options.getProperty(propertyName, defaultValue);
		Font foundFont = new Font(fontName, Font.PLAIN, 1000);
		if (foundFont.getFamily().equals("Dialog")) {
			foundFont = null;
			try {
				InputStream foundResource = resourceLoader
						.getResourceAsStream(fontName);
				if (foundResource != null) {
					try {
						foundFont = Font.createFont(Font.TRUETYPE_FONT,
								foundResource).deriveFont(1000.0f);
					} catch (FontFormatException e) {
						try {
							foundFont = Font.createFont(Font.TYPE1_FONT,
									foundResource).deriveFont(1000.0f);
						} catch (FontFormatException e2) {
							throw new IllegalArgumentException(propertyName);
						}
					}
				}
				if (foundFont == null) {
					foundResource = resourceLoader.getResourceAsStream(fontName
							+ ".ttf");
					if (foundResource != null) {
						try {
							foundFont = Font.createFont(Font.TRUETYPE_FONT,
									foundResource).deriveFont(1000.0f);
						} catch (FontFormatException e) {
							throw new IllegalArgumentException(propertyName);
						}
					}
				}
				if (foundFont == null) {
					foundResource = resourceLoader.getResourceAsStream(fontName
							+ ".pfb");
					if (foundResource != null) {
						try {
							foundFont = Font.createFont(Font.TYPE1_FONT,
									foundResource).deriveFont(1000.0f);
						} catch (FontFormatException e) {
							throw new IllegalArgumentException(propertyName);
						}
					}
				}
				if (foundFont == null) {
					throw new IllegalArgumentException(propertyName);
				}
			} catch (IOException e) {
				throw new IllegalArgumentException(propertyName);
			}
		}
		return foundFont;
	}

	/**
	 * Convenience function which parses a property as a font value. The
	 * property must either be known to the {@link java.awt.Font} class, or be
	 * the name of a font file loadable by resourceLoader. The font will have to
	 * be resized before being used.
	 * 
	 * @param options
	 *            The properties to use to retrieve the property value.
	 * @param propertyName
	 *            The name of the property to parse.
	 * @param defaultValue
	 *            The default value to use if the property does not exist.
	 * @param resourceLoader
	 *            The ClassLoader needed to locate the font file.
	 * @return The Font value parsed from the property.
	 * @throws IllegalArgumentException
	 *             if the property value is not parseable as a font.
	 */
	public Font parsePropertyAsFont(Properties options, String propertyName,
			Font defaultValue, ClassLoader resourceLoader)
			throws IllegalArgumentException {
		if (options.containsKey(propertyName)) {
			return this.parsePropertyAsFont(options, propertyName, "",
					resourceLoader);
		} else {
			return defaultValue;
		}
	}

	/**
	 * Convenience function which parses a property as an integer value. Throws
	 * IllegalArgumentException with the name of the property if the value is
	 * not parseable as an integer.
	 * 
	 * @param options
	 *            The properties to use to retrieve the property value.
	 * @param propertyName
	 *            The name of the property to parse.
	 * @param defaultValue
	 *            The default value to use if the property does not exist.
	 * @return The integer value parsed from the property.
	 * @throws IllegalArgumentException
	 *             if the property value is not parseable as an integer.
	 */
	public int parsePropertyAsInteger(Properties options, String propertyName,
			String defaultValue) throws IllegalArgumentException {
		int retVal = 0;
		try {
			retVal = Integer.parseInt(options.getProperty(propertyName,
					defaultValue));
		} catch (NumberFormatException e) {
			throw new IllegalArgumentException(propertyName);
		}
		return retVal;
	}

	/**
	 * Convenience function which parses a property as an integer value. Throws
	 * IllegalArgumentException with the name of the property if the value is
	 * not parseable as an integer.
	 * 
	 * @param options
	 *            The properties to use to retrieve the property value.
	 * @param propertyName
	 *            The name of the property to parse.
	 * @param defaultValue
	 *            The default value to use if the property does not exist.
	 * @return The integer value parsed from the property.
	 * @throws IllegalArgumentException
	 *             if the property value is not parseable as an integer.
	 */
	public int parsePropertyAsInteger(Properties options, String propertyName,
			int defaultValue) throws IllegalArgumentException {
		if (options.containsKey(propertyName)) {
			return this.parsePropertyAsInteger(options, propertyName, "");
		} else {
			return defaultValue;
		}
	}

	/**
	 * Convenience function which parses a property as a percentage value
	 * (integer from 0 to 100 followed by a % sign). Throws
	 * IllegalArgumentException with the name of the property if the value is
	 * not parseable as a percentage.
	 * 
	 * @param options
	 *            The properties to use to retrieve the property value.
	 * @param propertyName
	 *            The name of the property to parse.
	 * @param defaultValue
	 *            The default value to use if the property does not exist.
	 * @return The integer percentage value from 0 to 100 parsed from the
	 *         property.
	 * @throws IllegalArgumentException
	 *             if the property value is not parseable as a percentage.
	 */
	public int parsePropertyAsPercentage(Properties options,
			String propertyName, String defaultValue)
			throws IllegalArgumentException {
		int retVal = 0;
		try {
			String propValue = options.getProperty(propertyName, defaultValue);
			if (propValue.endsWith("%")) {
				retVal = Integer.parseInt(propValue.substring(0,
						propValue.length() - 1));
				if (retVal < 0 || retVal > 100) {
					throw new IllegalArgumentException(propertyName);
				}
			} else {
				throw new IllegalArgumentException(propertyName);
			}
		} catch (NumberFormatException e) {
			throw new IllegalArgumentException(propertyName);
		}
		return retVal;
	}

	/**
	 * Convenience function which parses a property as a percentage value
	 * (integer from 0 to 100 followed by a % sign). Throws
	 * IllegalArgumentException with the name of the property if the value is
	 * not parseable as a percentage.
	 * 
	 * @param options
	 *            The properties to use to retrieve the property value.
	 * @param propertyName
	 *            The name of the property to parse.
	 * @param defaultValue
	 *            The default value to use if the property does not exist.
	 * @return The integer percentage value from 0 to 100 parsed from the
	 *         property.
	 * @throws IllegalArgumentException
	 *             if the property value is not parseable as a percentage.
	 */
	public int parsePropertyAsPercentage(Properties options,
			String propertyName, int defaultValue)
			throws IllegalArgumentException {
		if (options.containsKey(propertyName)) {
			return this.parsePropertyAsPercentage(options, propertyName, "");
		} else {
			return defaultValue;
		}
	}

	/**
	 * Convenience function which parses a property as either a percentage value
	 * (integer from 0 to 100 followed by a % sign) or as a non-negative
	 * integer. Throws IllegalArgumentException with the name of the property if
	 * the value is not parseable as either.
	 * 
	 * @param options
	 *            The properties to use to retrieve the property value.
	 * @param propertyName
	 *            The name of the property to parse.
	 * @param defaultValue
	 *            The default value to use if the property does not exist.
	 * @return If a percentage value was parsed, it is returned as -1 through
	 *         -100; otherwise, the integer value is returned.
	 * @throws IllegalArgumentException
	 *             if the property value is not parseable as a percentage or
	 *             non-negative integer.
	 */
	public int parsePropertyAsIntegerOrPercentage(Properties options,
			String propertyName, String defaultValue)
			throws IllegalArgumentException {
		int retVal = 0;
		try {
			String propValue = options.getProperty(propertyName, defaultValue);
			if (propValue.endsWith("%")) {
				retVal = -Integer.parseInt(propValue.substring(0,
						propValue.length() - 1));
				if (retVal > 0 || retVal < -100) {
					throw new IllegalArgumentException(propertyName);
				}
			} else {
				retVal = Integer.parseInt(propValue);
			}
		} catch (NumberFormatException e) {
			throw new IllegalArgumentException(propertyName);
		}
		return retVal;
	}

	/**
	 * Convenience function which parses a property as either a percentage value
	 * (integer from 0 to 100 followed by a % sign) or as a non-negative
	 * integer. Throws IllegalArgumentException with the name of the property if
	 * the value is not parseable as either.
	 * 
	 * @param options
	 *            The properties to use to retrieve the property value.
	 * @param propertyName
	 *            The name of the property to parse.
	 * @param defaultValue
	 *            The default value to use if the property does not exist.
	 * @return If a percentage value was parsed, it is returned as -1 through
	 *         -100; otherwise, the integer value is returned.
	 * @throws IllegalArgumentException
	 *             if the property value is not parseable as a percentage or
	 *             non-negative integer.
	 */
	public int parsePropertyAsIntegerOrPercentage(Properties options,
			String propertyName, int defaultValue)
			throws IllegalArgumentException {
		if (options.containsKey(propertyName)) {
			return this.parsePropertyAsIntegerOrPercentage(options,
					propertyName, "");
		} else {
			return defaultValue;
		}
	}

	/**
	 * Convenience function which parses a property as either a boolean value.
	 * The value is <b>true</b> if the property is equal to one of <b>True</b>,
	 * <b>Yes</b>, or <b>On</b>. The value is <b>false</b> if the property is
	 * equal to one of <b>False</b>, <b>No</b>, or <b>Off</b>.
	 * (Uppercase/lowercase does not matter for this.) Throws
	 * IllegalArgumentException with the name of the property if the value does
	 * not match any of those strings.
	 * 
	 * @param options
	 *            The properties to use to retrieve the property value.
	 * @param propertyName
	 *            The name of the property to parse.
	 * @param defaultValue
	 *            The default value to use if the property does not exist.
	 * @return The boolean value.
	 * @throws IllegalArgumentException
	 *             if the property value is not parseable as a boolean.
	 */
	public boolean parsePropertyAsBoolean(Properties options,
			String propertyName, String defaultValue) {
		boolean retVal = false;
		String propValue = options.getProperty(propertyName, defaultValue)
				.toLowerCase();
		if (propValue.equals("true") || propValue.equals("yes")
				|| propValue.equals("on")) {
			retVal = true;
		} else if (propValue.equals("false") || propValue.equals("no")
				|| propValue.equals("off")) {
			retVal = false;
		} else {
			throw new IllegalArgumentException(propertyName);
		}
		return retVal;

	}

	/**
	 * Convenience function which parses a property as either a boolean value.
	 * The value is <b>true</b> if the property is equal to one of <b>True</b>,
	 * <b>Yes</b>, or <b>On</b>. The value is <b>false</b> if the property is
	 * equal to one of <b>False</b>, <b>No</b>, or <b>Off</b>.
	 * (Uppercase/lowercase does not matter for this.) Throws
	 * IllegalArgumentException with the name of the property if the value does
	 * not match any of those strings.
	 * 
	 * @param options
	 *            The properties to use to retrieve the property value.
	 * @param propertyName
	 *            The name of the property to parse.
	 * @param defaultValue
	 *            The default value to use if the property does not exist.
	 * @return The boolean value.
	 * @throws IllegalArgumentException
	 *             if the property value is not parseable as a boolean.
	 */
	public boolean parsePropertyAsBoolean(Properties options,
			String propertyName, boolean defaultValue) {
		if (options.containsKey(propertyName)) {
			return this.parsePropertyAsBoolean(options, propertyName, "");
		} else {
			return defaultValue;
		}
	}

	/**
	 * Processes an image: loads it, creates its frame, and saves the framed
	 * image.
	 * 
	 * @param sourceImageFile
	 *            The File pointing to the source image.
	 * @param framedImageFile
	 *            The File to which to write the framed image.
	 * @param options
	 *            The Properties to use when framing the image. See
	 *            createFramedImage for details.
	 * @param resourceLoader
	 *            The ClassLoader used to load resources needed by the frame.
	 * @throws IOException
	 *             if an error occurs while reading the sourceImage or saving
	 *             the framedImage.
	 * @throws IllegalArgumentException
	 *             if an error occurs while creating the framed image.
	 */
	public void processImage(File sourceImageFile, File framedImageFile,
			Properties options, ClassLoader resourceLoader) throws IOException,
			IllegalArgumentException {
		MetaImage img = MetaImage.readImage(sourceImageFile);
		img = processImage(img, options, resourceLoader);
		img.saveImage(framedImageFile);
	}

	/**
	 * Processes a MetaImage to return the new, framed, MetaImage. Subclasses
	 * must override this to do the actual work.
	 * 
	 * @param sourceImageFile
	 *            The MetaImage holding the source image.
	 * @param options
	 *            The Properties to use when framing the image. See
	 *            createFramedImage for details.
	 * @param resourceLoader
	 *            The ClassLoader used to load resources needed by the frame.
	 * @throws IllegalArgumentException
	 *             if an error occurs while creating the framed image.
	 */
	abstract public MetaImage processImage(MetaImage sourceImage,
			Properties options, ClassLoader resourceLoader)
			throws IllegalArgumentException;

	/**
	 * Processes an image through a framing style, returning the new image.
	 * 
	 * @param frameStyle
	 *            The framing style to use.
	 * @param sourceImage
	 *            The image to process.
	 * @param options
	 *            The framing options to use.
	 * @param resourceLoader
	 *            The ClassLoader to use to load resources from disk.
	 * @return the framed image.
	 * @throws IllegalArgumentException
	 *             if the named framing style could not be located.
	 * @throws InvalidClassException
	 *             if the named framing style could not be instantiated.
	 */
	public static MetaImage processImageStyle(String frameStyle,
			MetaImage sourceImage, Properties options,
			ClassLoader resourceLoader) throws IllegalArgumentException,
			InvalidClassException {
		FrameStyle imageFramer;
		try {
			imageFramer = (FrameStyle) Class.forName("FrameStyle" + frameStyle,
					true, resourceLoader).newInstance();
		} catch (ClassNotFoundException e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			throw new IllegalArgumentException("Style " + frameStyle
					+ " not found!");
		} catch (InstantiationException e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			throw new InvalidClassException("The FrameStyle" + frameStyle
					+ " class is not valid!");
		} catch (IllegalAccessException e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			throw new InvalidClassException("The FrameStyle" + frameStyle
					+ " class is not valid!");
		}
		return imageFramer.processImage(sourceImage, options, resourceLoader);
	}

	/**
	 * Fix the JPEG Image Metadata to ensure that there is only one <dqt> node
	 * and one <dht> node in the metadata.
	 * 
	 * @param md
	 *            The top-level IIOMetadata node
	 */
	void fixJPEGImageMetadata(IIOMetadata md) {
		IIOMetadataNode metadataNode = (IIOMetadataNode) md
				.getAsTree("javax_imageio_jpeg_image_1.0");
		IIOMetadataNode markerNode;
		// search for markerSequence node
		markerNode = (IIOMetadataNode) metadataNode.getFirstChild();
		while ((markerNode != null)
				&& (!markerNode.getNodeName().equals("markerSequence"))) {
			markerNode = (IIOMetadataNode) markerNode.getNextSibling();
		}
		if (markerNode != null) {
			// Record the first dqt and dht nodes, and for any
			// subsequent like nodes found, move their contents into the first
			// nodes.
			IIOMetadataNode nodeDQT = null;
			IIOMetadataNode nodeDHT = null;
			IIOMetadataNode node = (IIOMetadataNode) markerNode.getFirstChild();
			while (node != null) {
				IIOMetadataNode nextNode = (IIOMetadataNode) node
						.getNextSibling();
				if (node.getNodeName().equals("dqt")) {
					if (nodeDQT == null) {
						nodeDQT = node;
					} else {
						IIOMetadataNode child = (IIOMetadataNode) node
								.getFirstChild();
						while (child != null) {
							nodeDQT.appendChild(node.removeChild(child));
							child = (IIOMetadataNode) node.getFirstChild();
						}
						markerNode.removeChild(node);
					}
				} else if (node.getNodeName().equals("dht")) {
					if (nodeDHT == null) {
						nodeDHT = node;
					} else {
						IIOMetadataNode child = (IIOMetadataNode) node
								.getFirstChild();
						while (child != null) {
							nodeDHT.appendChild(node.removeChild(child));
							child = (IIOMetadataNode) node.getFirstChild();
						}
						markerNode.removeChild(node);
					}
				}
				node = nextNode;
			}
		}
		try {
			md.setFromTree("javax_imageio_jpeg_image_1.0", metadataNode);
		} catch (IIOInvalidTreeException e) {
		}
	}
}
