package org.springhaven.util;

import java.awt.Dimension;
import java.awt.Point;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Vector;

/**
 * 
 */

/**
 * @author Copyright (c) 2009 by Johnson Earls. All rights reserved. See <A
 *         HREF="http://www.springhaven.org/~darkfox/FoxJava/LICENSE">
 *         http://www.springhaven.org/~darkfox/FoxJava/LICENSE </A> for details.
 * 
 */
public class SpacePacker {
	public static final int DIRECTION_CENTER = 0;
	public static final int DIRECTION_NORTH = 1;
	public static final int DIRECTION_SOUTH = 2;
	public static final int DIRECTION_EAST = 4;
	public static final int DIRECTION_WEST = 8;
	public static final int DIRECTION_NORTHEAST = DIRECTION_NORTH
			+ DIRECTION_EAST;
	public static final int DIRECTION_NORTHWEST = DIRECTION_NORTH
			+ DIRECTION_WEST;
	public static final int DIRECTION_SOUTHEAST = DIRECTION_SOUTH
			+ DIRECTION_EAST;
	public static final int DIRECTION_SOUTHWEST = DIRECTION_SOUTH
			+ DIRECTION_WEST;
	public static final int DIRECTION_NORTHSOUTH = DIRECTION_NORTH
			+ DIRECTION_SOUTH;
	public static final int DIRECTION_EASTWEST = DIRECTION_EAST
			+ DIRECTION_WEST;
	public static final int DIRECTION_ALL = DIRECTION_NORTHSOUTH
			+ DIRECTION_EASTWEST;

	public static enum Anchor {
		CENTER(DIRECTION_CENTER), NORTH(DIRECTION_NORTH), SOUTH(DIRECTION_SOUTH), EAST(
				DIRECTION_EAST), WEST(DIRECTION_WEST), NORTHEAST(
				DIRECTION_NORTHEAST), NORTHWEST(DIRECTION_NORTHWEST), SOUTHEAST(
				DIRECTION_SOUTHEAST), SOUTHWEST(DIRECTION_SOUTHWEST);

		final public int value;

		Anchor(int value) {
			this.value = value;
		}

		boolean equals(Anchor other) {
			return this.value == other.value;
		}
	};

	public static enum Edge {
		NORTH(DIRECTION_NORTH), SOUTH(DIRECTION_SOUTH), EAST(DIRECTION_EAST), WEST(
				DIRECTION_WEST), TOP(DIRECTION_NORTH), BOTTOM(DIRECTION_SOUTH), RIGHT(
				DIRECTION_EAST), LEFT(DIRECTION_WEST);

		final public int value;

		Edge(int value) {
			this.value = value;
		}

		boolean equals(Edge other) {
			return this.value == other.value;
		}
	}

	public static enum Orientation {
		NONE(DIRECTION_CENTER), EASTWEST(DIRECTION_EASTWEST), X(
				DIRECTION_EASTWEST), HORIZONTAL(DIRECTION_EASTWEST), NORTHSOUTH(
				DIRECTION_NORTHSOUTH), Y(DIRECTION_NORTHSOUTH), VERTICAL(
				DIRECTION_NORTHSOUTH), BOTH(DIRECTION_ALL), ALL(DIRECTION_ALL);

		final public int value;

		Orientation(int value) {
			this.value = value;
		}

		boolean equals(Orientation other) {
			return this.value == other.value;
		}
	}

	public static enum Expand {
		NONE(DIRECTION_CENTER), NO(DIRECTION_CENTER), EASTWEST(
				DIRECTION_EASTWEST), X(DIRECTION_EASTWEST), HORIZONTAL(
				DIRECTION_EASTWEST), NORTHSOUTH(DIRECTION_NORTHSOUTH), Y(
				DIRECTION_NORTHSOUTH), VERTICAL(DIRECTION_NORTHSOUTH), BOTH(
				DIRECTION_ALL), ALL(DIRECTION_ALL), YES(DIRECTION_ALL);

		final public int value;

		Expand(int value) {
			this.value = value;
		}

		boolean equals(Expand other) {
			return this.value == other.value;
		}
	}

	public static enum Fill {
		NONE(DIRECTION_CENTER), NO(DIRECTION_CENTER), EASTWEST(
				DIRECTION_EASTWEST), X(DIRECTION_EASTWEST), HORIZONTAL(
				DIRECTION_EASTWEST), NORTHSOUTH(DIRECTION_NORTHSOUTH), Y(
				DIRECTION_NORTHSOUTH), VERTICAL(DIRECTION_NORTHSOUTH), BOTH(
				DIRECTION_ALL), ALL(DIRECTION_ALL), YES(DIRECTION_ALL);

		final public int value;

		Fill(int value) {
			this.value = value;
		}

		boolean equals(Fill other) {
			return this.value == other.value;
		}

	}

	public class Parcel {
		Object contents;
		Point contentLocation;
		Dimension contentSize;
		Dimension contentMinimumSize;
		Point parcelLocation;
		Dimension parcelSize;
		Dimension parcelMinimumSize;
		Anchor anchor;
		Fill fill;
		Expand expand;

		@Override
		public String toString() {
			return "Parcel[contents=" + contents + ",anchor=" + anchor
					+ ",fill=" + fill + ",expand=" + expand
					+ ",contentLocation=" + contentLocation + ",contentSize="
					+ contentSize + ",contentMinimumSize=" + contentMinimumSize
					+ ",parcelLocation=" + parcelLocation + ",parcelSize="
					+ parcelSize + ",parcelMinimumSize=" + parcelMinimumSize
					+ "]";
		}

		public Parcel(Object contents, Dimension contentMinimumSize,
				Anchor anchor, Fill fill, Expand expand) {
			this.contents = contents;
			this.contentMinimumSize = new Dimension(contentMinimumSize);
			this.anchor = anchor;
			this.fill = fill;
			this.expand = expand;

			this.contentSize = null;
			this.parcelMinimumSize = new Dimension(contentMinimumSize);
			this.parcelSize = new Dimension(contentMinimumSize);
			this.parcelLocation = null;
			this.contentLocation = null;
		}

		/**
		 * @return The contents of the parcel
		 */
		public Object getContents() {
			return contents;
		}

		/**
		 * @param contents
		 *            The new contents of the parcel
		 */
		public void setContents(Object contents) {
			this.contents = contents;
		}

		/**
		 * @return The minimum size of the parcel's contents
		 */
		public Dimension getContentMinimumSize() {
			return contentMinimumSize;
		}

		/**
		 * @param contentMinimumSize
		 *            The new minimum size of the parcel's contents
		 */
		public void setContentMinimumSize(Dimension contentMinimumSize) {
			this.contentMinimumSize = contentMinimumSize;
		}

		/**
		 * @return The anchor position for the parcel
		 */
		public Anchor getAnchor() {
			return anchor;
		}

		/**
		 * @param anchor
		 *            The new anchor position for the parcel
		 */
		public void setAnchor(Anchor anchor) {
			this.anchor = anchor;
		}

		/**
		 * @return The fill direction for the parcel contents
		 */
		public Fill getFill() {
			return fill;
		}

		/**
		 * @param fill
		 *            The new fill direction for the parcel contents
		 */
		public void setFill(Fill fill) {
			this.fill = fill;
		}

		/**
		 * @return The expansion direction for the parcel
		 */
		public Expand getExpand() {
			return expand;
		}

		/**
		 * @param expand
		 *            The new expansion direction for the parcel
		 */
		public void setExpand(Expand expand) {
			this.expand = expand;
		}

		/**
		 * @return The actual size of the content area. Until the containing
		 *         SpacePacker is packed, this returns null.
		 */
		public Dimension getContentSize() {
			return new Dimension(contentSize);
		}

		/**
		 * @return The actual location of the content area. Until the containing
		 *         SpacePacker is packed, this returns null.
		 */
		public Point getContentLocation() {
			return new Point(contentLocation);
		}

		/**
		 * @return The size of the entire parcel. Until the containing
		 *         SpacePacker is packed, this returns null.
		 */
		public Dimension getParcelSize() {
			return new Dimension(parcelSize);
		}

		/**
		 * @return The location of the entire parcel. Until the containing
		 *         SpacePacker is packed, this returns null.
		 */
		public Point getParcelLocation() {
			return new Point(parcelLocation);
		}
	}

	class BoxIterator implements Iterator<Parcel> {
		SpacePacker box;
		int position; // -1 = before, 0 = cavity, 1 =
		// after;
		Iterator<Parcel> parcelIterator;

		BoxIterator(SpacePacker box) {
			this.box = box;
			this.position = -1;
			parcelIterator = box.parcelsBefore.iterator();
		}

		public boolean hasNext() {
			while (position < 2) {
				if (parcelIterator.hasNext()) {
					return true;
				}
				parcelIterator = null;
				while ((position < 2) && (parcelIterator == null)) {
					position += 1;
					if (position == 0) {
						if (box.cavity != null) {
							parcelIterator = box.cavity.iterator();
						}
					} else if (position == 1) {
						parcelIterator = box.parcelsAfter.iterator();
					}
				}
			}
			return false;
		}

		public Parcel next() throws NoSuchElementException {
			if (!this.hasNext()) {
				throw new NoSuchElementException();
			}
			return parcelIterator.next();
		}

		public void remove() throws UnsupportedOperationException {
			throw new UnsupportedOperationException(
					"Remove operation not supported.");
		}
	}

	Point location = null;
	Dimension size = null;
	Dimension minSize = null;
	Dimension contentSize = null;
	SpacePacker cavity = null;
	Vector<Parcel> parcelsBefore = null;
	Vector<Parcel> parcelsAfter = null;
	Orientation orientation = null;
	Expand overallExpansion = null;

	void setup() {
		this.parcelsBefore = new Vector<Parcel>();
		this.parcelsAfter = new Vector<Parcel>();
		this.overallExpansion = Expand.NONE;
	}

	public SpacePacker(Dimension size) {
		this.size = new Dimension(size);
		setup();
	}

	public SpacePacker() {
		setup();
	}

	public void setSize(Dimension size) {
		this.size = new Dimension(size);
	}

	public void add(Parcel parcel, Edge side) throws IllegalArgumentException {
		if (this.cavity != null) {
			this.cavity.add(parcel, side);
		} else {
			switch (side) {
			case NORTH:
			case SOUTH:
				if (this.orientation == null) {
					this.orientation = Orientation.NORTHSOUTH;
				}
				if (this.orientation == Orientation.NORTHSOUTH) {
					if (side == Edge.NORTH) {
						this.parcelsBefore.add(parcel);
					} else {
						this.parcelsAfter.add(parcel);
					}
				} else {
					this.cavity = new SpacePacker();
					this.cavity.add(parcel, side);
				}
				break;
			case EAST:
			case WEST:
				if (this.orientation == null) {
					this.orientation = Orientation.EASTWEST;
				}
				if (this.orientation == Orientation.EASTWEST) {
					if (side == Edge.WEST) {
						this.parcelsBefore.add(parcel);
					} else {
						this.parcelsAfter.add(parcel);
					}
				} else {
					this.cavity = new SpacePacker();
					this.cavity.add(parcel, side);
				}
				break;
			}
		}
	}

	/**
	 * Compute the minimum size of the box. For an EASTWEST box, this is the
	 * largest of the minimum heights for each parcel and the contained cavity,
	 * and the sum of the minimum widths. For a NORTHSOUTH box, this is the
	 * largest of the minimum widths and the sum of the minimum heights. First,
	 * finds the minimum size for the cavity (if any) and uses that for the
	 * initial minimum size (or 0,0 if there is no cavity). Then iterates
	 * through each of the "before" and "after" parcels to account for their
	 * minimum sizes, either adding to or maximizing the appropriate axis of the
	 * minimum size, based on orientation.
	 */
	Dimension computeMinimumSize() {
		if (this.cavity != null) {
			this.minSize = new Dimension(this.cavity.computeMinimumSize());
		} else {
			this.minSize = new Dimension(0, 0);
		}
		Iterator<Parcel> parcels;
		if (this.orientation == Orientation.EASTWEST) {
			parcels = parcelsBefore.iterator();
			while (parcels.hasNext()) {
				Dimension minimumParcelSize = parcels.next().parcelMinimumSize;
				minSize.width += minimumParcelSize.width;
				if (minimumParcelSize.height > minSize.height) {
					minSize.height = minimumParcelSize.height;
				}
			}
			parcels = parcelsAfter.iterator();
			while (parcels.hasNext()) {
				Dimension minimumParcelSize = parcels.next().parcelMinimumSize;
				minSize.width += minimumParcelSize.width;
				if (minimumParcelSize.height > minSize.height) {
					minSize.height = minimumParcelSize.height;
				}
			}
		} else {
			parcels = parcelsBefore.iterator();
			while (parcels.hasNext()) {
				Dimension minimumParcelSize = parcels.next().parcelMinimumSize;
				minSize.height += minimumParcelSize.height;
				if (minimumParcelSize.width > minSize.width) {
					minSize.width = minimumParcelSize.width;
				}
			}
			parcels = parcelsAfter.iterator();
			while (parcels.hasNext()) {
				Dimension minimumParcelSize = parcels.next().parcelMinimumSize;
				minSize.height += minimumParcelSize.height;
				if (minimumParcelSize.width > minSize.width) {
					minSize.width = minimumParcelSize.width;
				}
			}
		}
		if (this.size == null) {
			this.size = new Dimension(this.minSize);
		}
		return this.minSize;
	}

	/**
	 * Propagate the "expand" values from parcels to cavities, then up to the
	 * containing boxes. Expansion values propagated from the cavity lose the
	 * expansion direction that does not match this box's orientation (in other
	 * words, for an EASTWEST box, the propagated expansion will be converted to
	 * either ExpandNone or ExpandX before this SpacePacker's parcels are
	 * accumulated).
	 */
	Expand propagateExpansion() {
		this.overallExpansion = Expand.NONE;
		if (this.cavity != null) {
			switch (this.orientation) {
			case EASTWEST:
				if (this.cavity.propagateExpansion() == Expand.X) {
					this.overallExpansion = Expand.X;
				}
				break;
			case NORTHSOUTH:
				if (this.cavity.propagateExpansion() == Expand.Y) {
					this.overallExpansion = Expand.Y;
				}
				break;
			}
		}
		Iterator<Parcel> parcels = parcelsBefore.iterator();
		while (this.overallExpansion != Expand.BOTH && parcels.hasNext()) {
			Expand parcelExpansion = parcels.next().expand;
			if ((parcelExpansion.value | this.overallExpansion.value) == Expand.BOTH.value) {
				this.overallExpansion = Expand.BOTH;
			} else if ((parcelExpansion.value | this.overallExpansion.value) == Expand.X.value) {
				this.overallExpansion = Expand.X;
			} else if ((parcelExpansion.value | this.overallExpansion.value) == Expand.Y.value) {
				this.overallExpansion = Expand.Y;
			}
		}
		parcels = parcelsAfter.iterator();
		while (this.overallExpansion != Expand.BOTH && parcels.hasNext()) {
			Expand parcelExpansion = parcels.next().expand;
			if ((parcelExpansion.value | this.overallExpansion.value) == Expand.BOTH.value) {
				this.overallExpansion = Expand.BOTH;
			} else if ((parcelExpansion.value | this.overallExpansion.value) == Expand.X.value) {
				this.overallExpansion = Expand.X;
			} else if ((parcelExpansion.value | this.overallExpansion.value) == Expand.Y.value) {
				this.overallExpansion = Expand.Y;
			}
		}
		return this.overallExpansion;
	}

	/**
	 * Expand the parcels within a box to fill the box's space. First, calculate
	 * how much excess space we have available to expand into. Then count the
	 * number of parcels (including the cavity as one "parcel") that want to
	 * expand in the box's orientation. Distribute the space available through
	 * the parcels, using a semi-even distribution algorithm described below.
	 * Each parcel that wishes to expand crosswise to the box's orientation does
	 * so to the full amount of excess space available. Finally, call the
	 * cavity, if any, to expand itself.
	 * 
	 * The semi-even distribution algorithm:<br>
	 * Inputs: space, count<br>
	 * Temporary variable: accumulator, index<br>
	 * Output: Array of count integers.<br>
	 * Initialize: accumulator = 0, index = 0<br>
	 * while (index < count):<br>
	 * &nbsp; array[index] = 0<br>
	 * &nbsp; while (accumulator <= space + (count/2)):<br>
	 * &nbsp; &nbsp; array[index] += 1<br>
	 * &nbsp; &nbsp; accumulator += count<br>
	 * &nbsp; accumulator = (accumulator % space) - count<br>
	 * &nbsp; index += 1
	 * 
	 * Example: Inputs: space=23, count=5
	 * 
	 * For index 0:<br>
	 * &nbsp; accumulator goes 5,10,15,20,25 - but at 30, breaks out of the
	 * counting loop, leaving array[0] at 5 and accumulator at 2.<br>
	 * For index 1:<br>
	 * &nbsp; accumulator goes 7,12,17,22 - breaks loop at 27, leaving array[1]
	 * at 4 and accumulator at -1.<br>
	 * For index 2:<br>
	 * &nbsp; accumulator goes 4,9,14,19,24 - breaks loop at 29, leaving
	 * array[2] at 5 and accumulator at 1.<br>
	 * For index 3:<br>
	 * &nbsp; accumulator goes 6,11,16,21 - breaks loop at 26, leaving array[3]
	 * at 4 and accumulator at -2.<br>
	 * For index 4:<br>
	 * &nbsp; accumulator goes 3,8,13,18,23 - breaks loop at 28, leaving
	 * array[4] at 5 and accumulator at 0.<br>
	 * And we're done. Array counts: 5,4,5,4,5 .
	 */
	void expandParcels() {
		Dimension excess = new Dimension(this.size.width - this.minSize.width,
				this.size.height - this.minSize.height);
		this.contentSize = new Dimension(0, 0);
		int expansionCount = 0;
		Iterator<Parcel> parcels = null;

		parcels = parcelsBefore.iterator();
		while (parcels.hasNext()) {
			Parcel parcel = parcels.next();
			if ((parcel.expand.value & this.orientation.value) != 0) {
				expansionCount += 1;
			}
		}
		if ((this.cavity != null)
				&& ((this.cavity.overallExpansion.value & this.orientation.value) != 0)) {
			expansionCount += 1;
		}
		parcels = parcelsAfter.iterator();
		while (parcels.hasNext()) {
			Parcel parcel = parcels.next();
			if ((parcel.expand.value & this.orientation.value) != 0) {
				expansionCount += 1;
			}
		}

		// System.err.println("** packing " + this + " : excess space = " +
		// excess + " : expansionCount = " + expansionCount);

		int extraSpace;
		if (this.orientation == Orientation.EASTWEST) {
			extraSpace = excess.width;
			int spaceOverflow = expansionCount / 2;
			parcels = parcelsBefore.iterator();
			while (parcels.hasNext()) {
				Parcel parcel = parcels.next();
				if ((parcel.expand.value & Expand.X.value) != 0) {
					int spaceToAdd = (extraSpace + spaceOverflow)
							/ expansionCount;
					spaceOverflow = (extraSpace + spaceOverflow)
							% expansionCount;
					parcel.parcelSize.width += spaceToAdd;
				}
				// if ((parcel.expand.value & Expand.Y.value) != 0) {
				parcel.parcelSize.height = this.size.height;
				// }
				this.contentSize.width += parcel.parcelSize.width;
				if (parcel.parcelSize.height > this.contentSize.height) {
					this.contentSize.height = parcel.parcelSize.height;
				}
			}
			if (this.cavity != null) {
				if ((this.cavity.overallExpansion.value & Expand.X.value) != 0) {
					int spaceToAdd = (extraSpace + spaceOverflow)
							/ expansionCount;
					spaceOverflow = (extraSpace + spaceOverflow)
							% expansionCount;
					this.cavity.size.width += spaceToAdd;
				}
				if ((this.cavity.overallExpansion.value & Expand.Y.value) != 0) {
					this.cavity.size.height = this.size.height;
				}
				this.contentSize.width += this.cavity.size.width;
				if (this.cavity.size.height > this.contentSize.height) {
					this.contentSize.height = this.cavity.size.height;
				}
			}
			parcels = parcelsAfter.iterator();
			while (parcels.hasNext()) {
				Parcel parcel = parcels.next();
				if ((parcel.expand.value & Expand.X.value) != 0) {
					int spaceToAdd = (extraSpace + spaceOverflow)
							/ expansionCount;
					spaceOverflow = (extraSpace + spaceOverflow)
							% expansionCount;
					parcel.parcelSize.width += spaceToAdd;
				}
				// if ((parcel.expand.value & Expand.Y.value) != 0) {
				parcel.parcelSize.height = this.size.height;
				// }
				this.contentSize.width += parcel.parcelSize.width;
				if (parcel.parcelSize.height > this.contentSize.height) {
					this.contentSize.height = parcel.parcelSize.height;
				}
			}
		} else {
			extraSpace = excess.height;
			int spaceOverflow = 0;
			parcels = parcelsBefore.iterator();
			while (parcels.hasNext()) {
				Parcel parcel = parcels.next();
				if ((parcel.expand.value & Expand.Y.value) != 0) {
					int spaceToAdd = (extraSpace + spaceOverflow)
							/ expansionCount;
					spaceOverflow = (extraSpace + spaceOverflow)
							% expansionCount;
					parcel.parcelSize.height += spaceToAdd;
				}
				// if ((parcel.expand.value & Expand.X.value) != 0) {
				parcel.parcelSize.width = this.size.width;
				// }
				this.contentSize.height += parcel.parcelSize.height;
				if (parcel.parcelSize.width > this.contentSize.width) {
					this.contentSize.width = parcel.parcelSize.width;
				}
			}
			if (this.cavity != null) {
				if ((this.cavity.overallExpansion.value & Expand.Y.value) != 0) {
					int spaceToAdd = (extraSpace + spaceOverflow)
							/ expansionCount;
					spaceOverflow = (extraSpace + spaceOverflow)
							% expansionCount;
					this.cavity.size.height += spaceToAdd;
				}
				if ((this.cavity.overallExpansion.value & Expand.X.value) != 0) {
					this.cavity.size.width = this.size.width;
				}
				this.contentSize.height += this.cavity.size.height;
				if (this.cavity.size.width > this.contentSize.width) {
					this.contentSize.width = this.cavity.size.width;
				}
			}
			parcels = parcelsAfter.iterator();
			while (parcels.hasNext()) {
				Parcel parcel = parcels.next();
				if ((parcel.expand.value & Expand.Y.value) != 0) {
					int spaceToAdd = (extraSpace + spaceOverflow)
							/ expansionCount;
					spaceOverflow = (extraSpace + spaceOverflow)
							% expansionCount;
					parcel.parcelSize.height += spaceToAdd;
				}
				// if ((parcel.expand.value & Expand.X.value) != 0) {
				parcel.parcelSize.width = this.size.width;
				// }
				this.contentSize.height += parcel.parcelSize.height;
				if (parcel.parcelSize.width > this.contentSize.width) {
					this.contentSize.width = parcel.parcelSize.width;
				}
			}
		}
		if (this.cavity != null) {
			// && (this.cavity.overallExpansion.value != Expand.NONE.value)) {
			this.cavity.expandParcels();
		}
	}

	/**
	 * Assign the parcel's location and compute content information based on
	 * parcel's size and passed-in location.
	 * 
	 * @param parcel
	 *            The parcel to assign.
	 * @param location
	 *            The location at which the parcel is located.
	 */
	void assignParcel(Parcel parcel, Point location) {
		parcel.parcelLocation = new Point(location);
		parcel.contentSize = new Dimension(parcel.contentMinimumSize);
		if ((parcel.fill.value & Fill.X.value) != 0) {
			parcel.contentSize.width = parcel.parcelSize.width;
		}
		if ((parcel.fill.value & Fill.Y.value) != 0) {
			parcel.contentSize.height = parcel.parcelSize.height;
		}
		int deltaX = parcel.parcelSize.width - parcel.contentSize.width;
		int deltaY = parcel.parcelSize.height - parcel.contentSize.height;
		if ((parcel.anchor.value & Anchor.WEST.value) != 0) {
			deltaX = 0;
		} else if ((parcel.anchor.value & Anchor.EAST.value) == 0) {
			deltaX /= 2;
		}
		if ((parcel.anchor.value & Anchor.NORTH.value) != 0) {
			deltaY = 0;
		} else if ((parcel.anchor.value & Anchor.SOUTH.value) == 0) {
			deltaY /= 2;
		}
		parcel.contentLocation = new Point(location.x + deltaX, location.y
				+ deltaY);
	}

	/**
	 * Sets each parcel's location based on its size and this box's location
	 * (which must be assigned before coming into this routine). Recursively
	 * descends into the cavity boxes.
	 */
	void locateParcels(Point myLocation) {
		this.location = new Point(myLocation);
		Point parcelLocation = new Point(myLocation);
		if (this.orientation == Orientation.EASTWEST) {
			Iterator<Parcel> parcels = this.parcelsBefore.iterator();
			while (parcels.hasNext()) {
				Parcel parcel = parcels.next();
				Point loc = new Point(parcelLocation);
				if (parcel.parcelSize.height < this.size.height) {
					loc.y += (this.size.height - parcel.parcelSize.height) / 2;
				}
				this.assignParcel(parcel, loc);
				parcelLocation.x += parcel.parcelSize.width;
			}
			if (this.cavity != null) {
				Point loc = new Point(parcelLocation);
				if (this.cavity.size.height < this.size.height) {
					loc.y += (this.size.height - this.cavity.size.height) / 2;
				}
				this.cavity.locateParcels(loc);
				parcelLocation.x += this.cavity.size.width;
			}
			parcels = this.parcelsAfter.iterator();
			while (parcels.hasNext()) {
				Parcel parcel = parcels.next();
				Point loc = new Point(parcelLocation);
				if (parcel.parcelSize.height < this.size.height) {
					loc.y += (this.size.height - parcel.parcelSize.height) / 2;
				}
				this.assignParcel(parcel, loc);
				parcelLocation.x += parcel.parcelSize.width;
			}
		} else {
			Iterator<Parcel> parcels = this.parcelsBefore.iterator();
			while (parcels.hasNext()) {
				Parcel parcel = parcels.next();
				Point loc = new Point(parcelLocation);
				if (parcel.parcelSize.width < this.size.width) {
					loc.x += (this.size.width - parcel.parcelSize.width) / 2;
				}
				this.assignParcel(parcel, loc);
				parcelLocation.y += parcel.parcelSize.height;
			}
			if (this.cavity != null) {
				Point loc = new Point(parcelLocation);
				if (this.cavity.size.width < this.size.width) {
					loc.x += (this.size.width - this.cavity.size.width) / 2;
				}
				this.cavity.locateParcels(loc);
				parcelLocation.y += this.cavity.size.height;
			}
			parcels = this.parcelsAfter.iterator();
			while (parcels.hasNext()) {
				Parcel parcel = parcels.next();
				Point loc = new Point(parcelLocation);
				if (parcel.parcelSize.width < this.size.width) {
					loc.x += (this.size.width - parcel.parcelSize.width) / 2;
				}
				this.assignParcel(parcel, loc);
				parcelLocation.y += parcel.parcelSize.height;
			}
		}
	}

	/**
	 * pack all the parcels into the space provided by the top-level box. First,
	 * compute the minimum size for each parcel, and for each containing box
	 * (recursively). Then propagate the requested expansion up from the parcels
	 * through the containing boxes (recursively). From the top down, expand
	 * each parcel and container to fill the available space, as requested by
	 * that particular parcel or container. Finally, from the top down, assign
	 * each parcel an X,Y location and determine the content location and size
	 * for that parcel.
	 * 
	 * @throws UnsupportedOperationException
	 *             if the box doesn't contain parcels.
	 */
	public void pack() throws UnsupportedOperationException {
		if (this.orientation == null) {
			throw new UnsupportedOperationException("Nothing to pack!");
		}
		this.computeMinimumSize();
		this.propagateExpansion();
		this.expandParcels();
		// compute our location and recursively locate parcels.
		// special case hack for top-level box: If content size is smaller
		// than box size, calculate the point needed to center the contents,
		// then set box size to content size. Restore it after we're done.
		Dimension realSize = this.size;
		this.size = new Dimension(this.contentSize);
		this.locateParcels(new Point(
				(realSize.width - this.contentSize.width) / 2,
				(realSize.height - this.contentSize.height) / 2));
		this.size = realSize;
	}

	public Iterator<Parcel> iterator() {
		return new BoxIterator(this);
	}

	@Override
	public String toString() {
		String ret = "SpacePacker[";
		ret += "location=" + location;
		ret += ",size=" + size;
		ret += ",minSize=" + minSize;
		ret += ",contentSize=" + contentSize;
		ret += ",orientation=" + orientation;
		ret += ",overallExpansion=" + overallExpansion;
		Iterator<Parcel> parcels = parcelsBefore.iterator();
		ret += ",parcelsBefore=[";
		String sep = "";
		while (parcels.hasNext()) {
			ret += sep + parcels.next();
			sep = ",";
		}
		ret += "]";
		ret += ",cavity=" + this.cavity;
		parcels = parcelsAfter.iterator();
		ret += ",parcelsAfter=[";
		sep = "";
		while (parcels.hasNext()) {
			ret += sep + parcels.next();
			sep = ",";
		}
		ret += "]";
		return ret;
	}

	/**
	 * Test the class. Performs exhaustive tests on one-parcel boxes, parcel
	 * placement tests on two-parcel boxes, then point tests for multi-parcel
	 * boxes.
	 * 
	 * @return true if all test cases perform correctly.
	 */
	public static void main(String args[]) {
		int incorrect = 0;
		final Expand expandValues[] = { Expand.NONE, Expand.X, Expand.Y,
				Expand.BOTH };
		final Fill fillValues[] = { Fill.NONE, Fill.X, Fill.Y, Fill.BOTH };
		final Anchor anchorValues[] = { Anchor.CENTER, Anchor.NORTH,
				Anchor.SOUTH, Anchor.EAST, Anchor.WEST, Anchor.NORTHEAST,
				Anchor.SOUTHEAST, Anchor.SOUTHWEST, Anchor.NORTHWEST };
		final Edge edgeValues[] = { Edge.NORTH, Edge.SOUTH, Edge.EAST,
				Edge.WEST };
		final Dimension boxSize = new Dimension(1000, 500);
		final Dimension contentMinimumSize = new Dimension(200, 100);
		for (int expandIdx = 0; expandIdx < expandValues.length; expandIdx += 1) {
			for (int fillIdx = 0; fillIdx < fillValues.length; fillIdx += 1) {
				for (int anchorIdx = 0; anchorIdx < anchorValues.length; anchorIdx += 1) {
					for (int sideIdx = 0; sideIdx < edgeValues.length; sideIdx += 1) {
						int resultingX = 400;
						int resultingY = 200;
						final Expand expand = expandValues[expandIdx];
						final Fill fill = fillValues[fillIdx];
						final Anchor anchor = anchorValues[anchorIdx];
						final Edge side = edgeValues[sideIdx];
						if ((expand.value & Expand.X.value) == 0) {
							resultingX = 400;
						} else if ((fill.value & Fill.X.value) != 0) {
							resultingX = 0;
						} else if ((anchor.value & Anchor.WEST.value) != 0) {
							resultingX = 0;
						} else if ((anchor.value & Anchor.EAST.value) != 0) {
							resultingX = 800;
						} else {
							resultingX = 400;
						}
						if ((expand.value & Expand.Y.value) == 0) {
							resultingY = 200;
						} else if ((fill.value & Fill.Y.value) != 0) {
							resultingY = 0;
						} else if ((anchor.value & Anchor.NORTH.value) != 0) {
							resultingY = 0;
						} else if ((anchor.value & Anchor.SOUTH.value) != 0) {
							resultingY = 400;
						} else {
							resultingY = 200;
						}
						Point resultingLocation = new Point(resultingX,
								resultingY);
						SpacePacker testBox = new SpacePacker(boxSize);
						SpacePacker.Parcel testParcel = testBox.new Parcel(
								"parcel", contentMinimumSize, anchor, fill,
								expand);
						testBox.add(testParcel, side);
						testBox.pack();
						if (!resultingLocation
								.equals(testParcel.contentLocation)) {
							incorrect += 1;
							System.out.println(side + "@" + testParcel
									+ ": contentLocation should be "
									+ resultingLocation);
						}
					}
				}
			}
		}
		// set 2: 2 parcels. Test all combinations of two edges
		// against expand/not-expand for both parcels.
		for (int side1Index = 0; side1Index < edgeValues.length; side1Index += 1) {
			for (int side2Index = 0; side2Index < edgeValues.length; side2Index += 1) {
				for (int expand1Index = 0; expand1Index < expandValues.length; expand1Index += 1) {
					for (int expand2Index = 0; expand2Index < expandValues.length; expand2Index += 1) {
						final Edge side1 = edgeValues[side1Index];
						final Edge side2 = edgeValues[side2Index];
						final Expand expand1 = expandValues[expand1Index];
						final Fill fill1 = fillValues[expand1Index];
						final Expand expand2 = expandValues[expand2Index];
						final Fill fill2 = fillValues[expand2Index];
						SpacePacker testBox = new SpacePacker(boxSize);
						SpacePacker.Parcel test1Parcel = testBox.new Parcel(
								"1", contentMinimumSize, Anchor.CENTER, fill1,
								expand1);
						SpacePacker.Parcel test2Parcel = testBox.new Parcel(
								"2", contentMinimumSize, Anchor.CENTER, fill2,
								expand2);
						testBox.add(test1Parcel, side1);
						testBox.add(test2Parcel, side2);
						testBox.pack();
						Iterator<SpacePacker.Parcel> parcels = testBox
								.iterator();
						SpacePacker.Parcel parcel1, parcel2;
						parcel1 = parcels.next();
						parcel2 = parcels.next();
						Point loc1 = new Point();
						Point loc2 = new Point();
						if ((side1.value & Orientation.NORTHSOUTH.value) != 0) {
							if ((parcel1.expand.value & Expand.NORTHSOUTH.value) == 0) {
								if ((parcel2.expand.value & Expand.NORTHSOUTH.value) == 0) {
									loc1.y = 150;
									loc2.y = 250;
								} else {
									loc1.y = 0;
									loc2.y = 100;
								}
							} else {
								if ((parcel2.expand.value & Expand.NORTHSOUTH.value) == 0) {
									loc1.y = 0;
									loc2.y = 400;
								} else {
									loc1.y = 0;
									loc2.y = 250;
								}
							}
							if ((parcel1.expand.value & Expand.EASTWEST.value) == 0) {
								loc1.x = 400;
							} else {
								loc1.x = 0;
							}
							if ((parcel2.expand.value & Expand.EASTWEST.value) == 0) {
								loc2.x = 400;
							} else {
								loc2.x = 0;
							}
						} else {
							if ((parcel1.expand.value & Expand.EASTWEST.value) == 0) {
								if ((parcel2.expand.value & Expand.EASTWEST.value) == 0) {
									loc1.x = 300;
									loc2.x = 500;
								} else {
									loc1.x = 0;
									loc2.x = 200;
								}
							} else {
								if ((parcel2.expand.value & Expand.EASTWEST.value) == 0) {
									loc1.x = 0;
									loc2.x = 800;
								} else {
									loc1.x = 0;
									loc2.x = 500;
								}
							}
							if ((parcel1.expand.value & Expand.NORTHSOUTH.value) == 0) {
								loc1.y = 200;
							} else {
								loc1.y = 0;
							}
							if ((parcel2.expand.value & Expand.NORTHSOUTH.value) == 0) {
								loc2.y = 200;
							} else {
								loc2.y = 0;
							}
						}
						if ((!parcel1.contentLocation.equals(loc1))
								|| (!parcel2.contentLocation.equals(loc2))) {
							incorrect += 1;
							String side1name;
							String side2name;
							if (parcel1.contents.equals("1")) {
								side1name = side1.name();
								side2name = side2.name();
							} else {
								side1name = side2.name();
								side2name = side1.name();
							}
							System.out.println("" + testBox);
							System.out.println(side1name + "@" + parcel1
									+ " contentLocation should be " + loc1
									+ " / " + side2name + "@" + parcel2
									+ " contentLocation should be " + loc2);
						}
					}
				}
			}
		}
		System.out.println("" + incorrect + " test cases failed.");
		{
			SpacePacker testBox = new SpacePacker(boxSize);
			SpacePacker.Parcel p1 = testBox.new Parcel("1", contentMinimumSize,
					Anchor.CENTER, Fill.BOTH, Expand.X);
			SpacePacker.Parcel p2 = testBox.new Parcel("2", contentMinimumSize,
					Anchor.CENTER, Fill.BOTH, Expand.X);
			SpacePacker.Parcel p3 = testBox.new Parcel("3", contentMinimumSize,
					Anchor.CENTER, Fill.BOTH, Expand.X);
			testBox.add(p1, Edge.WEST);
			testBox.add(p2, Edge.NORTH);
			testBox.add(p3, Edge.SOUTH);
			testBox.pack();
			Iterator<Parcel> parcels = testBox.iterator();
			while (parcels.hasNext()) {
				System.out.println(parcels.next());
			}
		}
		{
			SpacePacker testBox = new SpacePacker(boxSize);
			SpacePacker.Parcel p1 = testBox.new Parcel("1", contentMinimumSize,
					Anchor.CENTER, Fill.BOTH, Expand.X);
			SpacePacker.Parcel p2 = testBox.new Parcel("2", contentMinimumSize,
					Anchor.CENTER, Fill.BOTH, Expand.X);
			SpacePacker.Parcel p3 = testBox.new Parcel("3", contentMinimumSize,
					Anchor.CENTER, Fill.BOTH, Expand.X);
			testBox.add(p1, Edge.WEST);
			testBox.add(p2, Edge.WEST);
			testBox.add(p3, Edge.SOUTH);
			testBox.pack();
			Iterator<Parcel> parcels = testBox.iterator();
			while (parcels.hasNext()) {
				System.out.println(parcels.next());
			}
		}
		{
			SpacePacker testBox = new SpacePacker(boxSize);
			SpacePacker.Parcel p1 = testBox.new Parcel("1", contentMinimumSize,
					Anchor.CENTER, Fill.BOTH, Expand.Y);
			SpacePacker.Parcel p2 = testBox.new Parcel("2", contentMinimumSize,
					Anchor.CENTER, Fill.BOTH, Expand.Y);
			SpacePacker.Parcel p3 = testBox.new Parcel("3", contentMinimumSize,
					Anchor.CENTER, Fill.BOTH, Expand.Y);
			testBox.add(p1, Edge.WEST);
			testBox.add(p2, Edge.NORTH);
			testBox.add(p3, Edge.SOUTH);
			testBox.pack();
			Iterator<Parcel> parcels = testBox.iterator();
			while (parcels.hasNext()) {
				System.out.println(parcels.next());
			}
		}
	}
}
