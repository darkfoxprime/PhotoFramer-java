/**
 * 
 */
package org.springhaven.app.PhotoFramer;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InvalidClassException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.HashMap;
import java.util.Properties;
import java.util.Stack;
import java.util.Vector;

import org.springhaven.util.AbstractFrameStyle;
import org.springhaven.util.MetaImage;

/**
 * @author Copyright (c) 2009 by Johnson Earls. All rights reserved. See <A
 *         HREF="http://www.springhaven.org/~darkfox/FoxJava/LICENSE">
 *         http://www.springhaven.org/~darkfox/FoxJava/LICENSE </A> for details.
 * 
 */
public class App {

	static String VERSION = "2.0";

	static Properties DefaultOptions = null;
	static {
		DefaultOptions = new Properties();
		DefaultOptions.put("Artist", System.getProperty("user.name"));
		DefaultOptions.put("Style", "Border");
		DefaultOptions.put("OutputFilename", "%D%S%B.framed.%X");
		DefaultOptions.put("PropertyFile", "frame.prop");
		DefaultOptions.put("ImagePropertyExt", "prop");
	}

	/**
	 * Displays usage information for the application.
	 * 
	 * @param message
	 *            The error message (<b>null</b> for no message).
	 * @param exitCode
	 *            The exit code from the application.
	 */
	public static void usage(String message, int exitCode) {
		if (message != null) {
			System.err.println(message);
			System.err.println("");
		}
		System.err.println("PhotoFramer version " + VERSION + " usage:");
		System.err
				.println("  PhotoFramer [ <options> ] <input-filename> [ <input-filename>... ]");
		System.err.println("Options:");
		System.err.println("  -A <artist> | --artist=<artist>");
		System.err
				.println("    Set the artist of the photo, overriding any frame.prop files.");
		System.err.println("  -D <date> | --date=<date>");
		System.err
				.println("    Set the date of the photo, overriding any frame.prop files and the contents of the file itself.");
		System.err.println("  -S <style> | --style=<style>");
		System.err
				.println("    Use <style> as the frame style, overriding any frame.prop files.");
		System.err
				.println("  -p <property-file> | --propertyfile=<property-file>");
		System.err
				.println("    Use <property-file> instead of frame.prop to find the framing properties.");
		System.err
				.println("  -e <image-extension> | --imagepropertyext=<image-extension>");
		System.err
				.println("    Add <image-extension> to the end of the image name to check for an image-specific framing properties file.");
		System.err
				.println("  -o <output-filename> | --output=<output-filename>");
		System.err
				.println("    Use <output-filename> as the filename for the framed output image.");
		System.err
				.println("    If more than one input file is given, then this option should");
		System.err
				.println("    include one or more of the following wildcards:");
		System.err
				.println("      %B - base filename of the input filename, not including any extension.");
		System.err.println("      %D - directory name of the input filename.");
		System.err
				.println("      %S - The directory separator (/ or \\ as appropriate).");
		System.err.println("      %X - extension of the input filename.");
		System.err.println("    This defaults to \"%D%S%B.framed.%X\".");
		System.exit(exitCode);
	}

	/**
	 * Display usage information for the application, and exit with exit code
	 * 22.
	 * 
	 * @param message
	 *            The error message (<b>null</b> for no message).
	 */
	public static void usage(String message) {
		usage(message, 22);
	}

	/**
	 * Displays usage information for the application.
	 * 
	 * @param exitCode
	 *            The exit code from the application.
	 */
	public static void usage(int exitCode) {
		usage(null, exitCode);
	}

	/**
	 * Displays usage information for the application.
	 */
	public static void usage() {
		usage(null, 22);
	}

	/**
	 * @param args
	 *            The command line arguments. Should be:<br>
	 *            [ &lt;options&gt; ] &lt;jpegfile&gt; &lt;framedFile&gt;<br>
	 *            Options:
	 *            <ul>
	 *            <li>-A|--artist &lt;artist&gt;</li>
	 *            <li>-D|--date &lt;YYYY-MMM-DD&gt;</li>
	 *            </ul>
	 */
	public static void main(String[] args) {
		// create the command line options map
		Properties options = new Properties();
		// parse the command line options
		args = parseCommandLineOptions(args, options);
		if (args.length == 0) {
			usage(0);
		}

		// copy in specific properties that have to be known now
		if (!options.containsKey("PropertyFile")) {
			options.setProperty("PropertyFile",
					DefaultOptions.getProperty("PropertyFile"));
		}
		if (!options.containsKey("ImagePropertyExt")) {
			options.setProperty("ImagePropertyExt",
					DefaultOptions.getProperty("ImagePropertyExt"));
		}

		for (int i = 0; i < args.length; i += 1) {
			try {
				File imgFile = new File(args[i]).getCanonicalFile();
				String outFileName = options.getProperty("OutputFilename");
				int j = 0;
				while ((j + 1) < outFileName.length()) {
					if (outFileName.charAt(j) == '%') {
						j += 2;
						String replace = "%" + outFileName.charAt(j - 1);
						switch (outFileName.charAt(j - 1)) {
						case '%':
							replace = "%";
							break;
						case 'D':
							replace = imgFile.getParent();
							break;
						case 'S':
							replace = File.separator;
							break;
						case 'B':
							replace = imgFile.getName();
							{
								int x = replace.lastIndexOf('.');
								if (x > -1) {
									replace = replace.substring(0, x);
								}
							}
							break;
						case 'X':
							replace = imgFile.getName();
							{
								int x = replace.lastIndexOf('.');
								if (x > -1) {
									replace = replace.substring(x + 1);
								} else {
									replace = "";
								}
							}
							break;
						}
						outFileName = outFileName.substring(0, j - 2) + replace
								+ outFileName.substring(j);
						j += replace.length() - 2;
					} else {
						j += 1;
					}
				}
				File outFile = new File(outFileName);
				System.out.println("Framing " + args[i] + " -> " + outFileName);
				loadFilePropsAndProcessImage(imgFile, outFile, options);
			} catch (InvalidClassException e) {
				System.err.println(e.getMessage());
			} catch (IllegalArgumentException e) {
				System.err.println(e.getMessage());
			} catch (IOException e) {
				System.err.println(e.getMessage());
			}
		}
	}

	/**
	 * Parse the command line options.
	 * 
	 * @param args
	 *            The original command line array.
	 * @param options
	 *            A hashmap containing initial option values. Modified by
	 *            options processed by the function.
	 * @return The command line array with no options remaining.
	 */
	public static String[] parseCommandLineOptions(String[] args,
			Properties options) {
		Properties optionMap = new Properties();
		optionMap.setProperty("-A", "Artist");
		optionMap.setProperty("--artist", "Artist");
		optionMap.setProperty("--artis", "Artist");
		optionMap.setProperty("--arti", "Artist");
		optionMap.setProperty("--art", "Artist");
		optionMap.setProperty("--ar", "Artist");
		optionMap.setProperty("--a", "Artist");
		optionMap.setProperty("-D", "Date");
		optionMap.setProperty("--date", "Date");
		optionMap.setProperty("--dat", "Date");
		optionMap.setProperty("--da", "Date");
		optionMap.setProperty("--d", "Date");
		optionMap.setProperty("-S", "Style");
		optionMap.setProperty("--style", "Style");
		optionMap.setProperty("--styl", "Style");
		optionMap.setProperty("--sty", "Style");
		optionMap.setProperty("--st", "Style");
		optionMap.setProperty("--s", "Style");
		optionMap.setProperty("-o", "OutputFilename");
		optionMap.setProperty("--output", "OutputFilename");
		optionMap.setProperty("--outpu", "OutputFilename");
		optionMap.setProperty("--outp", "OutputFilename");
		optionMap.setProperty("--out", "OutputFilename");
		optionMap.setProperty("--ou", "OutputFilename");
		optionMap.setProperty("--o", "OutputFilename");
		optionMap.setProperty("-p", "PropertyFile");
		optionMap.setProperty("--propertyfile", "PropertyFile");
		optionMap.setProperty("--propertyfil", "PropertyFile");
		optionMap.setProperty("--propertyfi", "PropertyFile");
		optionMap.setProperty("--propertyf", "PropertyFile");
		optionMap.setProperty("--property", "PropertyFile");
		optionMap.setProperty("--propert", "PropertyFile");
		optionMap.setProperty("--proper", "PropertyFile");
		optionMap.setProperty("--prope", "PropertyFile");
		optionMap.setProperty("--props", "PropertyFile");
		optionMap.setProperty("--prop", "PropertyFile");
		optionMap.setProperty("--pro", "PropertyFile");
		optionMap.setProperty("--pr", "PropertyFile");
		optionMap.setProperty("--p", "PropertyFile");
		optionMap.setProperty("-e", "ImagePropertyExt");
		optionMap.setProperty("--imagepropertyext", "ImagePropertyExt");
		optionMap.setProperty("--imagepropertyex", "ImagePropertyExt");
		optionMap.setProperty("--imagepropertye", "ImagePropertyExt");
		optionMap.setProperty("--imageproperty", "ImagePropertyExt");
		optionMap.setProperty("--imagepropert", "ImagePropertyExt");
		optionMap.setProperty("--imageproper", "ImagePropertyExt");
		optionMap.setProperty("--imageprope", "ImagePropertyExt");
		optionMap.setProperty("--imageprop", "ImagePropertyExt");
		optionMap.setProperty("--imagepro", "ImagePropertyExt");
		optionMap.setProperty("--imagepr", "ImagePropertyExt");
		optionMap.setProperty("--imagep", "ImagePropertyExt");
		optionMap.setProperty("--image", "ImagePropertyExt");
		optionMap.setProperty("--imag", "ImagePropertyExt");
		optionMap.setProperty("--ima", "ImagePropertyExt");
		optionMap.setProperty("--im", "ImagePropertyExt");
		optionMap.setProperty("--i", "ImagePropertyExt");
		int i = 0;
		while (i < args.length - 1) {
			if (optionMap.containsKey(args[i])) {
				options.setProperty(optionMap.getProperty(args[i]), args[i + 1]);
				i += 2;
			} else if (args[i].startsWith("-")) {
				usage("Invalid option: " + args[i]);
			} else {
				break;
			}
		}
		String argsRemaining[] = new String[args.length - i];
		System.arraycopy(args, i, argsRemaining, 0, argsRemaining.length);
		return argsRemaining;
	}

	/**
	 * A cache of properties files loaded from the disk, to speed up processing
	 * of multiple images in one run.
	 */
	private static HashMap<String, Properties> PathProperties = null;

	/**
	 * Load the properties for an image file and process the image. This loads
	 * "frame.prop" properties files from the drive root up to the path
	 * containing the image, applying the command line properties last. It then
	 * loads the "FrameStyle<style>" class based on the "style" property found
	 * in the properties files (or "Default" style if none found), and
	 * instantiates that class to create the frame.
	 * 
	 * @param imgFile
	 *            The File object for the image filename.
	 * @param framedFile
	 *            The File object to which to write the framed image.
	 * @param options
	 *            The command line options.
	 * @throws IllegalArgumentException
	 *             if an error occurs during processing.
	 * @throws InvalidClassException
	 *             if a plug-in frame style class is invalid.
	 * @throws IOException
	 *             if either the original file could not be read or the framed
	 *             file could not be written.
	 */
	public static void loadFilePropsAndProcessImage(File imgFile,
			File framedFile, Properties options)
			throws IllegalArgumentException, InvalidClassException, IOException {
		if (PathProperties == null) {
			PathProperties = new HashMap<String, Properties>();
		}
		Stack<File> PathStack = new Stack<File>();
		Vector<URL> ClassPath = new Vector<URL>();
		File imgPath = imgFile.getAbsoluteFile();
		{
			String propertyFile = options.getProperty("ImagePropertyExt");
			if (propertyFile.charAt(0) != '.') {
				propertyFile = "." + propertyFile;
			}
			File imgPropFile = new File(imgFile.getPath() + propertyFile);
			System.err.println("Checking for image-specific property file "
					+ imgPropFile.toString());
			if (imgPropFile.canRead()) {
				PathStack.push(imgPropFile);
			}
		}
		String propertyFile = options.getProperty("PropertyFile");
		while (imgPath.getParent() != null) {
			imgPath = imgPath.getParentFile();
			ClassPath.addElement(imgPath.toURI().toURL());
			File propFile = new File(imgPath, propertyFile);
			if (propFile.canRead()) {
				PathStack.push(propFile);
			}
		}
		String systemClassPath = System.getProperty("java.class.path");
		while (true) {
			int i = systemClassPath.indexOf(File.pathSeparatorChar);
			if (i < 0) {
				ClassPath.addElement(new File(systemClassPath).toURI().toURL());
				break;
			}
			ClassPath.addElement(new File(systemClassPath.substring(0, i))
					.toURI().toURL());
			systemClassPath = systemClassPath.substring(i + 1);
		}
		Properties fileProps = new Properties();
		fileProps.putAll(DefaultOptions);
		while (!PathStack.isEmpty()) {
			File propFile = PathStack.pop();
			String propPath = propFile.getCanonicalPath();
			if (!PathProperties.containsKey(propPath)) {
				Properties props = new Properties();
				try {
					props.load(new FileReader(propPath));
				} catch (Exception e) {
					// Ignore all errors...
				}
				PathProperties.put(propPath, props);
			}
			fileProps.putAll(PathProperties.get(propPath));
		}
		URLClassLoader parentClassLoader = new URLClassLoader(
				ClassPath.toArray(new URL[0]));
		fileProps.putAll(options);
		String frameStyle = fileProps.getProperty("Style");
		MetaImage img = MetaImage.readImage(imgFile);
		img = AbstractFrameStyle.processImageStyle(frameStyle, img, fileProps,
				parentClassLoader);
		img.saveImage(framedFile);
	}
}
