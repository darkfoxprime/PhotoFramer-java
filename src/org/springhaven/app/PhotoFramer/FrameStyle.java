/**
 * 
 */
package org.springhaven.app.PhotoFramer;

import java.io.File;
import java.io.IOException;
import java.util.Properties;

import org.springhaven.util.MetaImage;

/**
 * @author Copyright (c) 2009 by Johnson Earls. All rights reserved. See <A
 *         HREF="http://www.springhaven.org/~darkfox/FoxJava/LICENSE">
 *         http://www.springhaven.org/~darkfox/FoxJava/LICENSE </A> for details.
 * 
 */
public interface FrameStyle {
	/**
	 * Processes an image: loads it, creates its frame, and saves the framed
	 * image.
	 * 
	 * @param sourceImage
	 *            The File pointing to the source image.
	 * @param framedImage
	 *            The File to which to write the framed image.
	 * @param options
	 *            The properties used for framing:
	 *            <table border="1">
	 *            <tr>
	 *            <th>artist</th>
	 *            <td>The artist's name to appear in the photo.</td>
	 *            </tr>
	 *            <tr>
	 *            <th>date</th>
	 *            <td>The date the photo was taken, in YYYY-MM-DD or YYYY-MM-DD
	 *            HH:MM:SS format. (If not specified in options, the date from
	 *            the photo's EXIF information, if any, or the file itself, will
	 *            be used.)</td>
	 *            </tr>
	 *            </table>
	 *            Other properties are specific to the framing style.
	 * @param resourceLoader
	 *            The ClassLoader needed to load resources used by the frame.
	 * @throws IOException
	 *             if an error occurs while reading the sourceImage or saving
	 *             the framedImage.
	 * @throws IllegalArgumentException
	 *             if an error occurs while creating the framed image.
	 */
	@Deprecated
	public void processImage(File sourceImage, File framedImage,
			Properties options, ClassLoader resourceLoader) throws IOException,
			IllegalArgumentException;

	/**
	 * Returns a framed image from a source image and a set of properties.
	 * 
	 * @param sourceImage
	 *            The MetaImage containing the source image.
	 * @param options
	 *            The properties used for framing:
	 *            <table border="1">
	 *            <tr>
	 *            <th>artist</th>
	 *            <td>The artist's name to appear in the photo.</td>
	 *            </tr>
	 *            <tr>
	 *            <th>date</th>
	 *            <td>The date the photo was taken, in YYYY-MM-DD or YYYY-MM-DD
	 *            HH:MM:SS format. (If not specified in options, the date from
	 *            the photo's EXIF information, if any, or the file itself, will
	 *            be used.)</td>
	 *            </tr>
	 *            </table>
	 *            Other properties are specific to the framing style.
	 * @param resourceLoader
	 *            The ClassLoader needed to load resources used by the frame.
	 * @throws IOException
	 *             if an error occurs while reading the sourceImage or saving
	 *             the framedImage.
	 * @throws IllegalArgumentException
	 *             if an error occurs while creating the framed image.
	 */
	public MetaImage processImage(MetaImage sourceImage, Properties options,
			ClassLoader resourceLoader) throws IllegalArgumentException;

}
