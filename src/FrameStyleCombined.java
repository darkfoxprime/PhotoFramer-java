import java.io.InvalidClassException;
import java.util.Iterator;
import java.util.Properties;

import org.springhaven.util.AbstractFrameStyle;
import org.springhaven.util.MetaImage;

/**
 * This style allows you to process an image with two or more framing styles.
 * Each style is called in order and passed the image as processed thus far. As
 * each style is called, any framing options that are prefixed by that style
 * name (or an alias, as described in the CombinedStyles option below
 * description below) are renamed to not include the prefix; this allows the
 * same framing style to be used multiple times with different options. For
 * example, if using the style "Flex", then the option "Flex_BorderLeft" would
 * be copied to "BorderLeft"; if using the style "Clean" with an alias of
 * "Outer", then the option "Outer_BorderColor" would be copied to
 * "BorderColor".
 * 
 * <h1>Options used by this framing style</h1>
 * 
 * <p>
 * The options that control this framing style are described in the following
 * table; the second table contains the descriptions of the Option Types used
 * below.
 * </p>
 * <table border="1">
 * <tr>
 * <th>Option&nbsp;Name</th>
 * <th>Option&nbsp;Type</th>
 * <th>Description</th>
 * <th>Default&nbsp;Value</th>
 * </tr>
 * <tr>
 * <th align="left">CombinedStyles</th>
 * <td>List of style names and aliases</td>
 * <td>
 * A comma-separated list of style names and aliases. The image will be
 * processed by the first style listed, then the second, and so on. Style
 * aliases are given by following the style name with an = and the alias. For
 * example, "Border, Flex = Outer" would call the Border framer first (renaming
 * all Border_* options by stripping the initial Border_ from the option name),
 * followed by the Flex framer (renaming all Outer_* options by stripping the
 * initial Outer_ from the option names).</td>
 * <td>Border</td>
 * </tr>
 * </table>
 * 
 * @author Copyright (c) 2010 by Johnson Earls. All rights reserved. See <A
 *         HREF="http://www.springhaven.org/~darkfox/FoxJava/LICENSE">
 *         http://www.springhaven.org/~darkfox/FoxJava/LICENSE </A> for details.
 * 
 */
public class FrameStyleCombined extends AbstractFrameStyle {

	private MetaImage processOneStyle(String styleName, String styleAlias,
			MetaImage sourceImage, Properties options,
			ClassLoader resourceLoader) throws IllegalArgumentException {
		styleAlias = styleAlias + "_";
		int styleAliasLength = styleAlias.length();
		Properties newOptions = new Properties();
		Iterator<String> names = options.stringPropertyNames().iterator();
		while (names.hasNext()) {
			String name = names.next();
			newOptions.setProperty(name, options.getProperty(name));
		}
		names = options.stringPropertyNames().iterator();
		while (names.hasNext()) {
			String name = names.next();
			if (name.startsWith(styleAlias)) {
				newOptions.setProperty(name.substring(styleAliasLength),
						options.getProperty(name));
			}
		}
		try {
			return AbstractFrameStyle.processImageStyle(styleName, sourceImage,
					newOptions, resourceLoader);
		} catch (InvalidClassException e) {
			throw new IllegalArgumentException(e.getMessage());
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see AbstractFrameStyle#processImage(org.springhaven.util.MetaImage,
	 * java.util.Properties, java.lang.ClassLoader)
	 */
	@Override
	public MetaImage processImage(MetaImage sourceImage, Properties options,
			ClassLoader resourceLoader) throws IllegalArgumentException {
		boolean DEBUG = parsePropertyAsBoolean(options, "CombinedDEBUG",
				"False");
		String combinedStylesProp = options.getProperty("CombinedStyles",
				"Border") + ",";
		int i = combinedStylesProp.indexOf(',');
		while (i > -1) {
			String styleName = combinedStylesProp.substring(0, i);
			combinedStylesProp = combinedStylesProp.substring(i + 1);
			i = styleName.length() - 1;
			while ((i >= 0) && (Character.isWhitespace(styleName.charAt(i)))) {
				i -= 1;
			}
			if (i < 0) {
				throw new IllegalArgumentException("CombinedStyles");
			}
			styleName = styleName.substring(0, i + 1);
			i = 0;
			while ((i < styleName.length())
					&& (Character.isWhitespace(styleName.charAt(i)))) {
				i += 1;
			}
			styleName = styleName.substring(i);
			i = 0;
			while ((i < styleName.length()) && (styleName.charAt(i) != '=')
					&& (!Character.isWhitespace(styleName.charAt(i)))) {
				i += 1;
			}
			String styleAlias = styleName;
			if (i < styleName.length()) {
				styleAlias = styleName.substring(i);
				styleName = styleName.substring(0, i);
				i = 0;
				while ((i < styleAlias.length())
						&& (styleAlias.charAt(i) != '=')
						&& (Character.isWhitespace(styleAlias.charAt(i)))) {
					i += 1;
				}
				if ((i == styleAlias.length()) || (styleAlias.charAt(i) != '=')) {
					throw new IllegalArgumentException("CombinedStyles");
				}
				i += 1;
				while ((i < styleAlias.length())
						&& (Character.isWhitespace(styleAlias.charAt(i)))) {
					i += 1;
				}
				styleAlias = styleAlias.substring(i);
				if (styleAlias.length() == 0) {
					throw new IllegalArgumentException("CombinedStyles");
				}

			}

			if (DEBUG) {
				System.err.println("FrameStyleCombined: processing style "
						+ styleName + " as alias " + styleAlias);
			}
			sourceImage = processOneStyle(styleName, styleAlias, sourceImage,
					options, resourceLoader);
			i = combinedStylesProp.indexOf(',');
		}
		return sourceImage;
	}
}
