import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.font.FontRenderContext;
import java.awt.font.TextLayout;
import java.awt.geom.AffineTransform;
import java.awt.geom.Path2D;
import java.awt.image.BufferedImage;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

import org.springhaven.util.EXIFInfo;
import org.springhaven.util.MetaImage;

/**
 * 
 */

/**
 * @author Copyright (c) 2009 by Johnson Earls. All rights reserved. See <A
 *         HREF="http://www.springhaven.org/~darkfox/FoxJava/LICENSE">
 *         http://www.springhaven.org/~darkfox/FoxJava/LICENSE </A> for details.
 * 
 */
public class FrameStyleClean extends FrameStyleBorder {
	protected EXIFInfo sourceImageEXIFInfo;
	protected String sourceImageName;

	abstract static class BorderItem {
		int position; // passed in through subclass constructor!
		int anchor; // passed in through subclass constructor!

		BorderItem(int position, int anchor) {
			this.position = position;
			this.anchor = anchor;
		}

		abstract Dimension minimumSize();
	}

	static class BorderText extends BorderItem {
		String text; // passed in through constructor
		Font font; // passed in through constructor
		Dimension maxSize; // passed in through constructor
		Color color; // passed in through constructor

		Dimension minSize; // computed based on text, font

		BorderText(int position, int anchor, String text, Font font,
				Dimension maxSize) {
			super(position, anchor);
			this.text = text;
			this.font = font;
			this.maxSize = new Dimension(maxSize);
			font = font.deriveFont(10000.0f);
			FontRenderContext frc = new FontRenderContext(null, true, true);
			Rectangle bounds;
			bounds = new TextLayout(text, font, frc).getBounds().getBounds();
			float scaleY = ((float) maxSize.height * maxSize.height)
					/ bounds.height;
			float scaleX = (bounds.width * scaleY) / maxSize.width;
			if (scaleX < 1.0f) {
				scaleX = 1.0f;
			}
			AffineTransform xform = AffineTransform.getScaleInstance(scaleX,
					scaleY);
			font = font.deriveFont(xform);
			bounds = new TextLayout(text, font, frc).getBounds().getBounds();
			minSize = new Dimension(bounds.width, bounds.height);
		}

		@Override
		Dimension minimumSize() {
			return new Dimension(minSize);
		}
	}

	static class BorderSeparator extends BorderItem {
		static enum SeparatorStyle {
			None, SimpleLine, PointedLine
		};

		SeparatorStyle style;
		Color color; // passed in through constructor
		Dimension maxSize; // passed in through constructor
		Dimension minSize; // computed based on text, font

		BorderSeparator(int position, int anchor, SeparatorStyle style,
				Color color, Dimension maxSize) {
			super(position, anchor);
			this.style = style;
			this.color = color;
			this.maxSize = new Dimension(maxSize);
			this.minSize = new Dimension(1, 1);
		}

		@Override
		Dimension minimumSize() {
			return new Dimension(minSize);
		}
	}

	int NameSize = 0;
	int CopyrightSize = 0;
	Font NameFont = null;
	Font CopyrightFont = null;
	Color NameColor = null;
	Color SeparatorColor = null;
	Color CopyrightColor = null;
	int NameSpacing = 0;
	int CopyrightSpacing = 0;
	/**
	 * 0 = None; 1 = SimpleLine; 2 = PointedLine
	 */
	int SeparatorStyle = 0;
	int SeparatorSize = 0;
	int SeparatorGap = 0;
	int SeparatorSpan = 0;
	String PhotoArtist = null;
	String CopyrightFormat = null;
	Date PhotoDate = null;

	/**
	 * <h1>Options understood by this frame style</h1>
	 * 
	 * <table border="1">
	 * <tr>
	 * <th>Parameter Name</th>
	 * <th>Description</th>
	 * <th>Default</th>
	 * </tr>
	 * <tr>
	 * <th align="left">TextOrder</th>
	 * <td>A comma-separated list of <b>Title</b>, <b>Artist</b>, <b>Date</b>,
	 * and <b>Separator</b>, representing the order in which the text fields are
	 * drawn. A field that does not appear does in TextOrder will not be used in
	 * the frame. Each field may appear only once. See <b>Text Placement</b>
	 * below for details.</td>
	 * <td>Title, Artist, Date</td>
	 * </tr>
	 * <tr>
	 * <th align="left">DefaultTextColor</th>
	 * <td>The default color used to draw the text fields, if not otherwise
	 * specified. This should either be one of the color names listed below, or
	 * a hex RGB color specified as: 0xRRGGBB (e.g. 0x00FF00 for green).<br>
	 * Known color names: black, blue, cyan, dark gray, gray, green, light gray,
	 * magenta, orange, pink, red, white, yellow.</td>
	 * <td>Black</td>
	 * </tr>
	 * <tr>
	 * <th align="left">DefaultTextFont</th>
	 * <td>The default font used to draw the text fields, if not otherwise
	 * specified. This can be a font known to Java, or the name of a TrueType
	 * (<b>.ttf</b>) or Type1 (<b>.pfb</b>) font file in the photo's directory
	 * or one of its parents.</td>
	 * <td>SansSerif</td>
	 * </tr>
	 * <tr>
	 * <th align="left">DefaultTextPosition</th>
	 * <td>The default positioning used to place the text fields, if not
	 * otherwise specified. Should be one of <b>North</b> (or <b>Top</b>),
	 * <b>South</b> (or <b>Bottom</b>), <b>East</b> (or <b>Right</b>), or
	 * <b>West</b> (or <b>Left</b>). See <b>Text Placement</b> below for
	 * details.</td>
	 * <td>North</td>
	 * </tr>
	 * <tr>
	 * <th align="left">DefaultTextAnchor</th>
	 * <td>The default position within the photo's text field boxes used to
	 * anchor the text field. See <b>Text Placement</b> below for details.
	 * Should be one of <b>North</b> (or <b>Top</b>), <b>South</b> (or
	 * <b>Bottom</b>), <b>East</b> (or <b>Right</b>), <b>West</b> (or
	 * <b>Left</b>), <b>NorthEast</b>, <b>NorthWest</b>, <b>SouthEast</b>,
	 * <b>SouthWest</b>, or <b>Center</b>.</td>
	 * <td>Center</td>
	 * </tr>
	 * <tr>
	 * <th align="left">TitleColor</th>
	 * <td>The color of the text of the photo's title.</td>
	 * <td>The <b>DefaultTextColor</b> setting.</td>
	 * </tr>
	 * <tr>
	 * <th align="left">TitlePosition</th>
	 * <td>The position used to place the photo's title. See <b>Text
	 * Placement</b> below for details.</td>
	 * <td>The <b>DefaultTextPosition</b> setting.</td>
	 * </tr>
	 * <tr>
	 * <th align="left">TitleAnchor</th>
	 * <td>The position within the photo title's box used to anchor the title.
	 * See <b>Text Placement</b> below for details.</td>
	 * <td>The <b>DefaultTextAnchor</b> setting.</td>
	 * </tr>
	 * <tr>
	 * <th align="left">TitleFormat</th>
	 * <td>The format used to draw the photo's title. See <b>Text
	 * Formatting</b>, below, for more information.</td>
	 * <td>T</td>
	 * </tr>
	 * <tr>
	 * <th align="left">TitleSize</th>
	 * <td>The maximum top-to-bottom size of the text of the photo's title. If
	 * followed by a % sign, it's a relative size - see <b>Text Placement</b>
	 * below for details.</td>
	 * <td>60%</td>
	 * </tr>
	 * <tr>
	 * <th align="left">TitleSpan</th>
	 * <td>The maximum left-to-right size of the text of the photo's title. If
	 * followed by a % sign, it's a relative size - see <b>Text Placement</b>
	 * below for details.</td>
	 * <td>66%</td>
	 * </tr>
	 * <tr>
	 * <th align="left">TitleSpacing</th>
	 * <td>How many spaces to insert between characters in the photo's title.</td>
	 * <td>0</td>
	 * </tr>
	 * <tr>
	 * <th align="left">DateColor</th>
	 * <td>The color of the text of the photo's date.</td>
	 * <td>The <b>DefaultTextColor</b> setting.</td>
	 * </tr>
	 * <tr>
	 * <th align="left">DatePosition</th>
	 * <td>The position used to place the photo's date. See <b>Text
	 * Placement</b> below for details.</td>
	 * <td>The <b>DefaultTextPosition</b> setting.</td>
	 * </tr>
	 * <tr>
	 * <th align="left">DateAnchor</th>
	 * <td>The position within the photo date's box used to anchor the date. See
	 * <b>Text Placement</b> below for details.</td>
	 * <td>The <b>DefaultTextAnchor</b> setting.</td>
	 * </tr>
	 * <tr>
	 * <th align="left">DateFormat</th>
	 * <td>The format used to draw the photo's date. See <b>Text Formatting</b>,
	 * below, for more information.</td>
	 * <td>yyyy MMMM dd</td>
	 * </tr>
	 * <tr>
	 * <th align="left">DateSize</th>
	 * <td>The maximum top-to-bottom size of the text of the photo's date. If
	 * followed by a % sign, it's a relative size - see <b>Text Placement</b>
	 * below for details.</td>
	 * <td>30%</td>
	 * </tr>
	 * <tr>
	 * <th align="left">DateSpan</th>
	 * <td>The maximum left-to-right size of the text of the photo's date. If
	 * followed by a % sign, it's a relative size - see <b>Text Placement</b>
	 * below for details.</td>
	 * <td>100%</td>
	 * </tr>
	 * <tr>
	 * <th align="left">DateSpacing</th>
	 * <td>How many spaces to insert between characters in the photo's date.</td>
	 * <td>0</td>
	 * </tr>
	 * <tr>
	 * <th align="left">ArtistColor</th>
	 * <td>The color of the text of the photo's artist.</td>
	 * <td>The <b>DefaultTextColor</b> setting.</td>
	 * </tr>
	 * <tr>
	 * <th align="left">ArtistPosition</th>
	 * <td>The position used to place the photo's artist. See <b>Text
	 * Placement</b> below for details.</td>
	 * <td>The <b>DefaultTextPosition</b> setting.</td>
	 * </tr>
	 * <tr>
	 * <th align="left">ArtistAnchor</th>
	 * <td>The position within the photo artist's box used to anchor the artist.
	 * See <b>Text Placement</b> below for details.</td>
	 * <td>The <b>DefaultTextAnchor</b> setting.</td>
	 * </tr>
	 * <tr>
	 * <th align="left">ArtistFormat</th>
	 * <td>The format used to draw the photo's artist. See <b>Text
	 * Formatting</b>, below, for more information.</td>
	 * <td>A</td>
	 * </tr>
	 * <tr>
	 * <th align="left">ArtistSize</th>
	 * <td>The maximum top-to-bottom size of the text of the photo's artist. If
	 * followed by a % sign, it's a relative size - see <b>Text Placement</b>
	 * below for details.</td>
	 * <td>30%</td>
	 * </tr>
	 * <tr>
	 * <th align="left">ArtistSpan</th>
	 * <td>The maximum left-to-right size of the text of the photo's artist. If
	 * followed by a % sign, it's a relative size - see <b>Text Placement</b>
	 * below for details.</td>
	 * <td>100%</td>
	 * </tr>
	 * <tr>
	 * <th align="left">ArtistSpacing</th>
	 * <td>How many spaces to insert between characters in the photo's artist.</td>
	 * <td>0</td>
	 * </tr>
	 * <tr>
	 * <th align="left">SeparatorColor</th>
	 * <td>The color of the separator.</td>
	 * <td>The <b>DefaultTextColor</b> setting.</td>
	 * </tr>
	 * <tr>
	 * <th align="left">SeparatorStyle</th>
	 * <td>The style of separator. Can be one of: <b>None</b>,
	 * <b>SimpleLine</b>, <b>PointedLine</b>.</td>
	 * <td>None</td>
	 * </tr>
	 * <tr>
	 * <th align="left">SeparatorSize</th>
	 * <td>The thickness of the separator. If followed by a % sign, it's
	 * relative to the bottom border size.</td>
	 * <td>5</td>
	 * </tr>
	 * <tr>
	 * <th align="left">SeparatorSpan</th>
	 * <td>How the separator spans the image. It's either a fixed width, a
	 * percentage width (relative to the width being spanned by the separator),
	 * or one of the strings "text", "name", "copyright", or <b>Above</b>,
	 * <b>Below</b>, <b>Widest</b>, or <b>Narrowest</b>, meaning to pick the
	 * size of the text field above or below the separator, or the widest or
	 * narrowest width of the text fields above or below the separator,
	 * respectively..</td>
	 * <td>100%</td>
	 * </tr>
	 * <tr>
	 * <th align="left">SeparatorGap</th>
	 * <td>The gap between text fields, or between text fields and any
	 * separators. If followed by a % sign, it's relative to the bottom border's
	 * height (for vertical gaps) or the image's width not counting borders (for
	 * horizontal gaps).</td>
	 * <td>20</td>
	 * </tr>
	 * </table>
	 * 
	 * <h1>Text Placement</h1>
	 * 
	 * <p>
	 * There are three possible text fields, plus a separator, that can be
	 * placed into the bottom border of the framed image: the photo's
	 * <b>Title</b>, <b>Artist</b>, and <b>Date</b>. Described here is the
	 * flexible control method used to determine where each text field is
	 * placed.
	 * </p>
	 * 
	 * <p>
	 * First, only the desired text fields are placed at all. The
	 * <b>TextOrder</b> option determines which fields get placed. Any text
	 * field not listed in <b>TextOrder</b> will not be placed.
	 * </p>
	 * 
	 * <p>
	 * Next, in the order listed in <b>TextOrder</b>, each text field is
	 * allocated space out of the unused area of the border. Such space is
	 * allocated either from the "North" (top) or "South" (bottom) sides of the
	 * available space (spanning the total available width), or from the "East"
	 * (right) or "West" (left) sides of the available space (spanning the total
	 * available height). How much space gets allocated is determined by either
	 * the <b><i>Field</i>Size</b> parameter (for North or South placement) or
	 * by the <b><i>Field</i>Span</b> parameter (for East or West placement).
	 * (For East and West placement, the <b><i>Field</i>Size</b> parameter is
	 * also used, to determine the height of the text).
	 * 
	 * After the text field's space is allocated, the next text field listed in
	 * <b>TextOrder</b> is processed, using the space remaining.
	 * 
	 * When all fields have been allocated, the unused space is removed,
	 * allowing other fields to expand into that space. Then all fields are
	 * reduced in size vertically until the bounds of the entire text area is as
	 * small as it can be, and the fields are drawn into the bottom border
	 * centered vertically (and always spanning the total width of the image).
	 * 
	 * Each field is placed in its allocated space according to the
	 * <b><i>Field</i>Anchor</b> parameter for that field (or
	 * <b>DefaultTextAnchor</b> if no field-specific anchor is provided).
	 * <b>Center</b> centers the field within the allocated space; <b>North</b>,
	 * <b>South</b>, <b>East</b>, or <b>West</b> (or <b>Top</b>, <b>Bottom</b>,
	 * <b>Right</b>, <b>Left</b>, respectively) place the appropriate edge of
	 * the text centered along the specified side (e.g. <b>North</b> places the
	 * center of the top edge of the text at the center of the top edge of the
	 * field's allocated space). <b>NorthEast</b>, <b>NorthWest</b>,
	 * <b>SouthEast</b>, or <b>SouthWest</b> cause the text to be placed in the
	 * specified corner of the allocated space.
	 * 
	 * <h1>Text Formatting</h1>
	 * 
	 * A modification of Java's {@link SimpleDateFormat} is used to format text
	 * fields. In addition to the format letters used by SimpleDateFormat, "A"
	 * can be used to specify the photo's artist and "T" can be used to for the
	 * photo's title.
	 * 
	 * A common example of this is to change the 'Date' field into a 'Copyright'
	 * field:
	 * 
	 * <pre>
	 * DateFormat='Copyright \u00A9' yyyy 'by' A
	 * </pre>
	 * 
	 * @param sourceImage
	 *            The image we're processing.
	 * @param options
	 *            The options from the frame.prop files found.
	 * @param resourceLoader
	 *            The ClassLoader used to load resources needed by the frame.
	 * @throws IllegalArgumentException
	 *             if one of the parameter values cannot be understood. The
	 *             parameter name will be in the exception message.
	 */
	@Override
	public void setupFrameParameters(MetaImage sourceImage, Properties options,
			ClassLoader resourceLoader) throws IllegalArgumentException {
		super.setupFrameParameters(sourceImage, options, resourceLoader);
		this.sourceImageEXIFInfo = sourceImage.getExifInfo();
		this.sourceImageName = sourceImage.getName();
		PhotoArtist = options.getProperty("Artist");
		CopyrightFormat = options.getProperty("CopyrightFormat",
				"'\u00A9' yyyy A");
		NameSize = parsePropertyAsIntegerOrPercentage(options, "NameSize",
				"35%");
		CopyrightSize = parsePropertyAsIntegerOrPercentage(options,
				"CopyrightSize", "15%");
		NameFont = CopyrightFont = parsePropertyAsFont(options, "TextFont",
				"SansSerif", resourceLoader);
		if (options.containsKey("NameFont")) {
			NameFont = parsePropertyAsFont(options, "NameFont", "",
					resourceLoader);
		}
		if (options.containsKey("CopyrightFont")) {
			CopyrightFont = parsePropertyAsFont(options, "CopyrightFont", "",
					resourceLoader);
		}
		NameColor = SeparatorColor = CopyrightColor = parsePropertyAsColor(
				options, "TextColor", "black");
		if (options.containsKey("NameColor")) {
			NameColor = parsePropertyAsColor(options, "NameColor", "");
		}
		if (options.containsKey("CopyrightColor")) {
			CopyrightColor = parsePropertyAsColor(options, "CopyrightColor", "");
		}
		if (options.containsKey("SeparatorColor")) {
			SeparatorColor = parsePropertyAsColor(options, "SeparatorColor", "");
		}
		NameSpacing = parsePropertyAsInteger(options, "NameSpacing", "0");
		if (NameSpacing < 0) {
			throw new IllegalArgumentException("NameSpacing");
		}
		CopyrightSpacing = parsePropertyAsInteger(options, "CopyrightSpacing",
				"0");
		if (CopyrightSpacing < 0) {
			throw new IllegalArgumentException("CopyrightSpacing");
		}
		{
			String tmp = options.getProperty("SeparatorStyle", "None")
					.toLowerCase();
			if (tmp.equals("none")) {
				SeparatorStyle = 0;
			} else if (tmp.equals("simpleline") || tmp.equals("simple line")) {
				SeparatorStyle = 1;
			} else if (tmp.equals("pointedline") || tmp.equals("pointed line")) {
				SeparatorStyle = 2;
			} else {
				throw new IllegalArgumentException("SeparatorStyle");
			}
		}
		SeparatorSize = parsePropertyAsIntegerOrPercentage(options,
				"SeparatorSize", "5");
		SeparatorGap = parsePropertyAsIntegerOrPercentage(options,
				"SeparatorGap", "5%");
		{
			String tmp = options.getProperty("SeparatorSpan", "text")
					.toLowerCase();
			if (tmp.equals("text")) {
				SeparatorSpan = -103;
			} else if (tmp.equals("name")) {
				SeparatorSpan = -101;
			} else if (tmp.equals("copyright")) {
				SeparatorSpan = -102;
			} else if (tmp.equals("image")) {
				SeparatorSpan = -104;
			} else {
				SeparatorSpan = parsePropertyAsIntegerOrPercentage(options,
						"SeparatorSpan", "100%");
			}
		}

		DateFormat datePropFormatter = new SimpleDateFormat("y-M-d");
		DateFormat dateTimePropFormatter = new SimpleDateFormat("y-M-d H:m:s");
		DateFormat dateTimeExifFormatter = new SimpleDateFormat("y:M:d H:m:s");
		if (sourceImageEXIFInfo.containsKey(EXIFInfo.Tag.TagDateTime)) {
			try {
				PhotoDate = dateTimeExifFormatter
						.parse((String) (sourceImageEXIFInfo
								.get(EXIFInfo.Tag.TagDateTime)[0]));
			} catch (ParseException e) {
			}
		}
		if (PhotoDate == null
				&& sourceImageEXIFInfo
						.containsKey(EXIFInfo.Tag.TagDateTimeOriginal)) {
			try {
				PhotoDate = dateTimeExifFormatter
						.parse((String) (sourceImageEXIFInfo
								.get(EXIFInfo.Tag.TagDateTimeOriginal)[0]));
			} catch (ParseException e) {
			}
		}
		if (PhotoDate == null
				&& sourceImageEXIFInfo
						.containsKey(EXIFInfo.Tag.TagDateTimeDigitized)) {
			try {
				PhotoDate = dateTimeExifFormatter
						.parse((String) (sourceImageEXIFInfo
								.get(EXIFInfo.Tag.TagDateTimeDigitized)[0]));
			} catch (ParseException e) {
			}
		}
		if (PhotoDate == null && options.containsKey("Date")) {
			try {
				PhotoDate = dateTimePropFormatter.parse(options
						.getProperty("Date"));
			} catch (ParseException e) {
			}
		}
		if (PhotoDate == null && options.containsKey("Date")) {
			try {
				PhotoDate = datePropFormatter
						.parse(options.getProperty("Date"));
			} catch (ParseException e) {
			}
		}
		if (PhotoDate == null) {
			PhotoDate = new Date();
		}
	}

	/**
	 * Resizes the image and adds the appropriate sized border to it, with photo
	 * name and copyright added to the bottom border.
	 * 
	 * @param sourceImage
	 *            The original image to border.
	 * @return The resized image with border.
	 */
	@Override
	public BufferedImage addBorderToImage(BufferedImage sourceImage) {
		BufferedImage borderedImage = super.addBorderToImage(sourceImage);
		Graphics2D borderedImageGraphics = borderedImage.createGraphics();
		String sep = "";
		for (int i = NameSpacing; i > 0; i -= 1) {
			sep += " ";
		}
		String photoName = "";
		for (int i = 0; i < sourceImageName.length(); i += 1) {
			if (i > 0) {
				photoName += sep;
			}
			photoName += sourceImageName.charAt(i);
		}
		sep = "";
		for (int i = CopyrightSpacing; i > 0; i -= 1) {
			sep += " ";
		}
		String photoCopyrightBase = CopyrightFormat;
		{
			boolean inQuote = false;
			for (int i = 0; i < photoCopyrightBase.length(); i += 1) {
				if (inQuote) {
					if (photoCopyrightBase.charAt(i) == '\'') {
						inQuote = false;
					}
				} else {
					switch (photoCopyrightBase.charAt(i)) {
					case '\'':
						inQuote = true;
						break;
					case 'A':
						photoCopyrightBase = photoCopyrightBase.substring(0, i)
								+ "'" + PhotoArtist.replaceAll("'", "''") + "'"
								+ photoCopyrightBase.substring(i + 1);
					}
				}
			}
		}
		photoCopyrightBase = new SimpleDateFormat(photoCopyrightBase)
				.format(PhotoDate);
		String photoCopyright = "";
		for (int i = 0; i < photoCopyrightBase.length(); i += 1) {
			if (i > 0) {
				photoCopyright += sep;
			}
			photoCopyright += photoCopyrightBase.charAt(i);
		}
		if (NameSize < 0) {
			NameSize = RealBorderBottom * NameSize / -100;
		}
		if (CopyrightSize < 0) {
			CopyrightSize = RealBorderBottom * CopyrightSize / -100;
		}
		if (SeparatorSize < 0) {
			SeparatorSize = RealBorderBottom * SeparatorSize / -100;
		}
		if (SeparatorGap < 0) {
			SeparatorGap = RealBorderBottom * SeparatorGap / -100;
		}

		NameFont = NameFont.deriveFont((float) NameSize);
		CopyrightFont = CopyrightFont.deriveFont((float) CopyrightSize);
		FontRenderContext frc = new FontRenderContext(null, true, true);
		Rectangle bounds;
		TextLayout layout;
		bounds = new TextLayout(photoName, NameFont, frc).getBounds()
				.getBounds();
		NameFont = NameFont.deriveFont((float) (NameSize * NameSize)
				/ (float) bounds.height);
		bounds = new TextLayout(photoCopyright, CopyrightFont, frc).getBounds()
				.getBounds();
		CopyrightFont = CopyrightFont
				.deriveFont((float) (CopyrightSize * CopyrightSize)
						/ (float) bounds.height);

		int totalSpace = NameSize + CopyrightSize + SeparatorSize + 2
				* SeparatorGap;
		int deltaY = (RealBorderBottom - totalSpace) / 2;
		if (deltaY < 0) {
			deltaY = 0;
		}
		deltaY = RealImageHeight - deltaY;
		int maxTextWidth = RealImageWidth - RealBorderLeft - RealBorderRight;
		int nameTextWidth = 0, nameTextX = 0;
		int copyrightTextWidth = 0, copyrightTextX = 0;
		int widestTextWidth = 0, widestTextX = 0;
		layout = new TextLayout(photoCopyright, CopyrightFont, frc);
		bounds = layout.getBounds().getBounds();
		if (bounds.width > maxTextWidth) {
			AffineTransform xform = AffineTransform.getScaleInstance(
					(float) maxTextWidth / (float) bounds.width, 1.0);
			CopyrightFont = CopyrightFont.deriveFont(xform);
			layout = new TextLayout(photoCopyright, CopyrightFont, frc);
			bounds = layout.getBounds().getBounds();
		}
		copyrightTextWidth = bounds.width;
		copyrightTextX = (int) ((maxTextWidth - bounds.width) * 0.5 + RealBorderLeft);
		if (copyrightTextWidth > widestTextWidth) {
			widestTextWidth = copyrightTextWidth;
			widestTextX = copyrightTextX;
		}
		borderedImageGraphics.setColor(CopyrightColor);
		layout.draw(borderedImageGraphics, copyrightTextX, deltaY
				- bounds.height - bounds.y);
		deltaY -= bounds.height;
		deltaY -= SeparatorGap;
		deltaY -= SeparatorSize;
		int separatorY = deltaY;
		deltaY -= SeparatorGap;
		layout = new TextLayout(photoName, NameFont, frc);
		bounds = layout.getBounds().getBounds();
		if (bounds.width > maxTextWidth) {
			AffineTransform xform = AffineTransform.getScaleInstance(
					(float) maxTextWidth / (float) bounds.width, 1.0);
			NameFont = NameFont.deriveFont(xform);
			layout = new TextLayout(photoName, NameFont, frc);
			bounds = layout.getBounds().getBounds();
		}
		nameTextWidth = bounds.width;
		nameTextX = (int) ((maxTextWidth - bounds.width) * 0.5 + RealBorderLeft);
		if (nameTextWidth > widestTextWidth) {
			widestTextWidth = nameTextWidth;
			widestTextX = nameTextX;
		}
		borderedImageGraphics.setColor(NameColor);
		layout.draw(borderedImageGraphics, nameTextX, deltaY - bounds.height
				- bounds.y);
		// draw separator here
		borderedImageGraphics.setColor(SeparatorColor);
		int separatorX = 0, separatorWidth = 0;
		switch (SeparatorSpan) {
		case -104:
			separatorX = RealBorderLeft;
			separatorWidth = maxTextWidth;
			break;
		case -103:
			separatorX = widestTextX;
			separatorWidth = widestTextWidth;
			break;
		case -102:
			separatorX = copyrightTextX;
			separatorWidth = copyrightTextWidth;
			break;
		case -101:
			separatorX = nameTextX;
			separatorWidth = nameTextWidth;
			break;
		default:
			if (SeparatorSpan < 0) {
				separatorWidth = maxTextWidth * SeparatorSpan / -100;
			}
			separatorX = (int) ((maxTextWidth - separatorWidth) * 0.5 + RealBorderLeft);
		}
		switch (SeparatorStyle) {
		case 1: // Simple Line
			borderedImageGraphics.fillRect(separatorX, separatorY,
					separatorWidth, SeparatorSize);
			break;
		case 2: // Pointed Line
			double topY = separatorY;
			double bottomY = (separatorY + SeparatorSize - 1);
			double midY = (topY + bottomY) * 0.5;
			double leftX = separatorX;
			double rightX = separatorX + separatorWidth;
			double midleftX = leftX * 0.75 + rightX * 0.25;
			double midrightX = leftX * 0.25 + rightX * 0.75;
			Path2D.Double pointedLinePath = new Path2D.Double();
			pointedLinePath.moveTo(leftX, midY);
			pointedLinePath.curveTo(midleftX, topY - 1, midrightX, topY - 1,
					rightX, midY);
			pointedLinePath.curveTo(midrightX, bottomY + 1, midleftX,
					bottomY + 1, leftX, midY);
			borderedImageGraphics.fill(pointedLinePath);
			break;
		// anything else gets ignored
		}
		return borderedImage;
	}

}
